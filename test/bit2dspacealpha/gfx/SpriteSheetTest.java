/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspacealpha.gfx;

import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.spriteutil.SpriteSheet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Bo
 */
public class SpriteSheetTest {

    public SpriteSheetTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of init method, of class SpriteSheet.
     */
    @Test
    public void testInit() {
        System.out.println("init");
        String sheetPath = Gfx.DEF_SPRITE_SHEET_PATH;
        SpriteSheet instance = new SpriteSheet();
        boolean result = instance.init(sheetPath);
        boolean expResult = true;
        assertEquals(expResult, result);
    }

    /**
     * Test of getLeadingBytes method, of class SpriteSheet.
     */
    @Test
    public void testGetLeadingBytes() {
        System.out.println("getLeadingBytes");
        int n = Gfx.NUM_SPRITE_SHEET_COLORS;
        SpriteSheet instance = new SpriteSheet();
        String sheetPath = Gfx.DEF_SPRITE_SHEET_PATH;
        boolean init = instance.init(sheetPath);
        boolean expected = true;
        assertEquals(expected, init);
        int[] expResult = new int[n];
        for (int i = 0; i < n; i++) {
            expResult[i] = i;
        }
        int[] result = instance.getLeadingBytes(n);
        assertArrayEquals(expResult, result);
    }

}