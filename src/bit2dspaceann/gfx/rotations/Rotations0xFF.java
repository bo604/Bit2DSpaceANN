/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.gfx.rotations;

/**
 *
 *
 * DOES NOT WORK QUITE YET... REVISIT LATER!!!
 *
 * i like the idea... in short, again:
 * 360° =: 256 "bytees"
 * then use bit fun for stuff...
 *
 * see TestMain for some tests!
 *
 * @author Bo
 */
public class Rotations0xFF {

    /*
     * DEFINE:
     * 256 BYTEE = 360 DEGREE = 2*PI RADIANS
     *
     * ROTATION MATRICES TRIGONOMETRICS STORED IN INT BYTES:
     *
     * [0xFF * a_11, 0xFF * a_12, 0xFF * a_21, 0xFF * a_22]
     *
     * with
     * a_ij = cos,-sin,sin,cos of the angle rotation destribed by the usual rotation matrix
     *
     * 256 ROTATION INTEGERS stored in array after BYTEE angle
     *
     */


    private final int[] rotationMatrices;

    public Rotations0xFF() {
        rotationMatrices = new int[0xFF];
        initRotationMatrices();
    }


    private void initRotationMatrices() {
        for (int bytee = 0; bytee < 0xFF; bytee++) {

            double rad = toRadians(bytee);

            double sin = Math.sin(rad);
            double cos = Math.cos(rad);

            int rotationMatrix =  ( (((int)( cos * 0xFF ))&0xFF) << 24 )
                                | ( (((int)(-sin * 0xFF ))&0xFF) << 16 )
                                | ( (((int)( sin * 0xFF ))&0xFF) << 8 )
                                |   (((int)( cos * 0xFF ))&0xFF);

            rotationMatrices[bytee] = rotationMatrix;
        }

        for (int bytee = 0; bytee < 10; bytee++) {
            System.out.println(rotationMatrices[bytee]>>24);
        }
    }


    public int getRotationMatrix(int bytee) {
        /*
         * TEST FOR BOUDNS?????????
         */
        return rotationMatrices[bytee];
    }



    /**
     * USE ONLY FOR ROTATION MATRIX ARRAY INITIALIZATION!!!!!!!!
     * Converts "bytee" into radians
     * @param bytee
     * @return
     */
    private static double toRadians(int bytee) {
        return bytee * 128.0 / Math.PI; // = bytee / 2Pi * 256...
    }

//    /**
//     * DO NOT USE LIVE!!!!!!!
//     * FOR ROTATION MATRIX ARRAY INITIALIZATION!!!!!!!!
//     * Converts radians into "bytee"
//     * @param rad
//     * @return
//     */
//    private static byte toBytee(double rad) {
//        --------
//    }


    private static final int log128 = 7;
    private static final int log256 = 8;

    /**
     *
     * @param rotationMatrix integer bytes: [AABBCCDD]
     *
     * @return
     */
    public static void rotate(int rotationMatrix, int[] vectorToRotate ) {

        //shorthand:
        int rm = rotationMatrix;
        int vx = vectorToRotate[0];
        int vy = vectorToRotate[1];

        int rx =  ((((rm<<24)&0xFF) * vx ) >> log256 )   // = vx * cos(theta)
        //           a_11 <= 255      a_11 >> log256 = cos(theta)

                + ((((rm<<16)&0xFF) * vx ) >> log256 );    // = vx * -sin(theta)
        //           a_12 <= 255      a_12 >> log256 = -sin(theta)

        int ry =  ((((rm<<8)&0xFF) * vy ) >> log256 )    // = vy * sin(theta)
        //           a_21 <= 255      a_21 >> log256 = sin(theta)

                + ((( rm    &0xFF) * vy ) >> log256 );    // = vy * cos(theta)
        //           a_22 <= 255      a_22 >> log256 = cos(theta)

        vectorToRotate[0] = rx;
        vectorToRotate[1] = ry;
    }


}
