/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.gfx.rotations;

import bit2dspaceann.gfx.Gfx;

/**
 *
 * @author Bo
 */
public class TileRotationLookup256 {


    //rotated shit

    /*
     * get with tile pixel index [0..7][0..7][angle]
     */
    public final float[][][] cosdx_nsindy;
    public final float[][][] sindx_cosdy;

    public TileRotationLookup256() {

        cosdx_nsindy = new float[Gfx.TILE_SIZE][Gfx.TILE_SIZE][Gfx.NUM_ANGLES];
        sindx_cosdy = new float[Gfx.TILE_SIZE][Gfx.TILE_SIZE][Gfx.NUM_ANGLES];

        initRotations();

    }

    private void initRotations() {

        int dxCenter = Gfx.TILE_SIZE >> 1;
        int dyCenter = Gfx.TILE_SIZE >> 1;

        for (int i = 0; i < Gfx.TILE_SIZE; i++) {
            for (int j = 0; j < Gfx.TILE_SIZE; j++) {
                for (int k = 0; k < Gfx.NUM_ANGLES; k++) {

                        int dxToRotate = i - dxCenter;
                        int dyToRotate = j - dyCenter;

                        // rotate
//                        float dxRotated = cos[angle] * dxToRotate - sin[angle] * dyToRotate;
//                        float dyRotated = sin[angle] * dxToRotate + cos[angle] * dyToRotate;
                        double dxRotated = Math.cos(k) * dxToRotate - Math.sin(k) * dyToRotate;
                        double dyRotated = Math.sin(k) * dxToRotate + Math.cos(k) * dyToRotate;

                        cosdx_nsindy[i][j][k] = (float)dxRotated;
                        sindx_cosdy[i][j][k] = (float)dyRotated;

                }
            }
        }

    }





}
