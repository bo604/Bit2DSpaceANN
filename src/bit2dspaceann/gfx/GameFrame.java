/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.gfx;

import java.awt.BorderLayout;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import javax.swing.JFrame;

/**
 *
 * @author Bo
 */
public class GameFrame extends JFrame {

    public static final String FRAME_TITLE = "IN SPAAAACE!";

    // For full screen only:
    public GraphicsDevice gd = null;
    
    public GameFrame() {
        super(FRAME_TITLE);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setLayout(new BorderLayout());
    }

    public boolean init(GameCanvas canvas) {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setLayout(new BorderLayout());
        this.add(canvas , BorderLayout.CENTER);
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        if(Gfx.FULL_SCREEN) {
            gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()[0];
            gd.setFullScreenWindow(this);
        }
        return true;
    }

    public void addCanvas(GameCanvas canvas, String layoutPosition) {
        this.add(canvas,layoutPosition);
    }

}


//    public boolean init(GameCanvas canvas, GameCanvas canvas2) {
//        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        this.setResizable(false);
//        this.setLayout(new BorderLayout());
//        this.add( canvas , BorderLayout.CENTER);
//        this.add( canvas2 , BorderLayout.SOUTH);
//        this.pack();
//        this.setLocationRelativeTo(null);
//        this.setVisible(true);
//        return true;
//    }


// <editor-fold defaultstate="collapsed" desc="old">


//        if(isGameOver) {
//            ggScreen.render();
//            // Draw gg screen to image pixels
//            for (int y = 0; y < ggScreen.height; y++) {
//                for (int x = 0; x < ggScreen.width; x++) {
//                    int colorCode = ggScreen.pixels[x + y * ggScreen.width];
//                    if(colorCode < 255) pixels[x + y * width] = colors[colorCode];
//                }
//            }
//        }
//        else {
//            // View screen rendering.
//            // "Camera position" to player position
//            int xOffset = player.xWorld - (screen.width/2);
//            int yOffset = player.yWorld - (screen.height/2);
//            // Render stuff into view screen
//            level.renderTiles(screen, xOffset, yOffset);
//            level.renderEntities(screen);
//            splashScreen.render(screen);
//            // Draw view screen to image pixels
//            for (int y = 0; y < screen.height; y++) {
//                for (int x = 0; x < screen.width; x++) {
//                    int colorCode = screen.pixels[x + y * screen.width];
//                    if(colorCode < 255) pixels[x + y * width] = colors[colorCode];
//                }
//            }
//        }


//    private void fontTest() {
//        // Render some font
//        String msg = "RUN!";
//        int textX = (screen.width - msg.length() * Screen.TILE_WIDTH) / 2;
//        int textY = (screen.height - Screen.TILE_WIDTH) / 2;
////        int textCol = Colors.get(000, -1, -1, 555);   // White on Black
//        int textCol = Colors.get(-1, -1, -1, 000);      // Black on Trans
//        int testScale = 1;
//        Font.render(msg, screen, textX, textY, textCol, testScale);
//    }


//        // Number of tiles in x and y of the sprite sheet (=width/tile_width)
//        int nTiles = 32;
//        // Log base 2 of tile width (for bit-shifting div/mults)
//        int tileWidthLog2 = Screen.TILE_WIDTH_LOG2;
//        // Render screen internally
//        for (int y = 0; y < nTiles; y++) {
//            for (int x = 0; x < nTiles; x++) {
//                /* Define the 4 colors used for the render */
//                int col = Colors.get(555, 500, 050, 005);
//                boolean mirrorX = x%2 == 1;
//                boolean mirrorY = y%2 == 1;
//                /* Render screen pixel internally */
//                screen.render( x<<tileWidthLog2, y<<tileWidthLog2, 0, col, mirrorX, mirrorY);
//            }
//        }
//        /* strange moving bars */
//        if(!true) {
//            for (int i = 0; i < pixels.length; i++) {
//                pixels[i] = i + tickCount;
//            }
//        }
//        /* let it run */
//        if(!true) {
//            screen.xOffset++;
//            screen.yOffset++;
//        }


// </editor-fold>

