/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.gfx;

/**
 * 555-Color conversion
 * @author Bo
 */
public class ColorsLong {

    // Test colors: black, white, red, green, blue, yellow, purple, teal
    public static long TEST_COLOR = ColorsLong.get(-1, 555, 500, 050, 005, 550, 505, 055);

    // Test colors gray gradient: trans, black, grays..., white, red
    public static long TEST_GRAY = ColorsLong.get(005, 111, 222, 333, 444, 555, 500, 050);

    // Test colors gray gradient: trans, black, grays..., white, red
    public static long GRAY_RED = ColorsLong.get(-1, 000, 111, 222, 333, 444, 555, 500);

//    public static long DEF_SHIP = ColorsLong.get(-1, 444, 222, 530, 055, 500, 050, 005);
    public static long DEF_SHIP = ColorsLong.get(-1, 444, 222, 055, 530, 500, 050, 005);

    
    public static long DEF_FOOD = ColorsLong.get(-1, 444, 222, 053, 440, 050, 252, 025);
    
    public static long DEF_FONT = ColorsLong.get(-1, -1, -1, -1, -1, -1, -1, 555);
    
    
    /**
     * 4 color shortcut
     * @param c1
     * @param c2
     * @param c3
     * @param c4
     * @return
     */
    public static long get(int c1, int c2, int c3, int c4) {
        return get(c1,c2,c3,c4,-1,-1,-1,-1);
    }

    /**
     * Specify 8 colors
     * Put each color in its own byte inside a long
     * c_i in [0...6*6*6], which is less than 1 byte (i.e. max 255 colors each)
     * the values of the bytes of the returned log represent indices in the color table
     *
     * Best to use only once to init the color scheme for a sprite!
     * 
     * @param c_i The ith color to use for rendering of sprite tiles
     * @return combined long color code carrying one color per byte, i.e. color = 0xC1C2C3C4....C8
     */
    public static long get(int c1, int c2, int c3, int c4, int c5, int c6, int c7, int c8) {
        long color = get(c1);
        color += get(c2) << 8;
        color += get(c3) << 16;
        color += get(c4) << 24;
        color += get(c5) << 32;
        color += get(c6) << 40;
        color += get(c7) << 48;
        color += get(c8) << 56;
        return color;
    }

    public static long get(int c) {
        if(c < 0) return 255;
        // Split into rgb by decimal position
        int r = c / 100 % 10;
        int g = c / 10 % 10;
        int b = c % 10;
        // Return long rgb in [0..6*6*6]
        return r*36 + g*6 + b;
    }

    /**
     * get a specific byte (given by colorIndex) from the long
     * @param color
     * @param colorIndex index(byte) of the 256color to get from the long
     * @return
     */
    public static int getColorTableIndex(long color, int colorIndex) {
        // multiply colorIndex by 8 (bits per byte) (by shifting << 3)
        // shift to byte corresponding to colorIndex
        // cutoff higher bytes ( & 0xFF )
        return (int)((color >> ( colorIndex << 3 )) & 0xFF);
    }


    /**
     * ONLY CALL THIS FOR CONSTRUCTION!!!
     * Use the preconstructed tables for rendering!
     * @return
     */
    public static int[] constructColorTable() {
        /*
         * Initialize fixed array of colors that will be used throughout the game
         * with NUM_SHADES per color (per color per color...)
         */
        int nShades = Gfx.NUM_RENDER_COLOR_SHADES;
        int colorRangeDivider = nShades - 1;
        int[] colors = new int[nShades * nShades * nShades];  // nShades^3 colors
        int index = 0;
        for (int r = 0; r < nShades; r++) {
            for (int g = 0; g < nShades; g++) {
                for (int b = 0; b < nShades; b++) {
                    int rr = r * 255 / colorRangeDivider;
                    int gg = g * 255 / colorRangeDivider;
                    int bb = b * 255 / colorRangeDivider;
                    colors[index++] = 0xFF000000 | rr << 16 | gg << 8 | bb;
                }
            }
        }
        return colors;
    }


}
