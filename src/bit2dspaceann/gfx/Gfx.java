/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.gfx;

/**
 *
 * @author Bo
 */
public class Gfx {

    /*
     * Sprite sheets used by the game.
     */
    public static final String DEF_SPRITE_SHEET_PATH = "/spritesheets/def_sprite_sheet_8.png";
    public static final String FONT_SPRITE_SHEET_PATH = "/spritesheets/font_sprite_sheet_8.png";
//    public static final String ENTITY_SPRITE_SHEET_PATH = "/spritesheets/entity_sprite_sheet_16.png";

    public static final String[] LEVEL_PATHS = new String[] {
        null,
//        "/levels/test_32x32.png",
        "/levels/test_64x64_grass.png",
        "/levels/test_64x64_space.png",
        "/levels/test_128x128_space.png",
    };

    public static final String[] BG_PATHS = new String[] {
        null,
        "/bg/stars_01.png",
        "/bg/stars_02.jpg",
        "/bg/stars_03.png",
    };

    /**
     * How many tiles are in each row of the spread sheet?
     */
    public static final int NUM_TILES_IN_SHEET_ROWS = 32;
    public static final int NUM_TILES_IN_SHEET_ROWS_LOG2 = 5;
    // Shorthand
    public static final int NTIS = NUM_TILES_IN_SHEET_ROWS;
    public static final int NTIS_L2 = NUM_TILES_IN_SHEET_ROWS_LOG2;


    /**
     * The image of the game
     */
//    private static final double aspect = 3d / 4d;
    private static final double aspect = 9d / 16d;
    public static final int IMAGE_PIXEL_WIDTH = 1600;
    public static final int IMAGE_PIXEL_HEIGHT = (int)(IMAGE_PIXEL_WIDTH * aspect);
//    public static final int IMAGE_PIXEL_WIDTH = 1920;
//    public static final int IMAGE_PIXEL_HEIGHT = 1080;
    public static final int IMAGE_VIEW_SCALE = 1;
    public static final boolean FULL_SCREEN = !true;

//    /**
//     * nColors is 255(rgb) / 15(range) = 17.
//     * The color will have the format 1,2,3...17, with 17 == transparent
//     */
    public static final int NUM_SPRITE_SHEET_COLORS = 8; // = number of bytes in a long (used to store the tile color schemes)
    public static final int RBG_RANGE_PER_COLOR = 36;    // = (int)(255/7), the "distance" of (grayscale) colors in the sprite sheets

    /**
     * base number of shades for each color channel in new format, i.e. colors 000 to 555
     * with r, g, and b from 0 to 5. Enough of that already!
     */
    public static final int NUM_RENDER_COLOR_SHADES = 6;
    public static final int NUM_RENDER_COLORS = NUM_RENDER_COLOR_SHADES * NUM_RENDER_COLOR_SHADES * NUM_RENDER_COLOR_SHADES;

    /**
     * size (and log2) of each tile of the spread sheet
     * TODO: increase to 16 (and 4) when everything runs smoothly!
     */
    public static final int TILE_SIZE = 8;
    public static final int TILE_SIZE_LOG2 = 3;
    // Shorthand
    public static final int TS = TILE_SIZE;
    public static final int TS_L2 = TILE_SIZE_LOG2;


    /**
     * number of angles used for the rotations.
     * change this in the near future?
     * also: use this everywhere consistently! some 360's are still floating around
     */
    public static final int NUM_ANGLES = 256;
    
    public static boolean RENDER_VISION_RAYS = !true;
    public static boolean RENDER_FISH_VISION = !true;
    public static boolean RENDER_CHARTS = !true;
    public static boolean RENDER_GENEPOOL = !true;
    public static boolean RENDER_ENTITY_INFO = !true;
//    public static boolean RENDER_ENTITY_FUEL = !true;

    /**
     * number of ticks per generation
     */
    public static final int TICKS_PER_GENERATION = 6000;
    
    
    /**
     * Mirroring bit constants
     */
    public static final int BIT_MIRROR_NONE = 0x00;
    public static final int BIT_MIRROR_X = 0x01;
    public static final int BIT_MIRROR_Y = 0x02;


}
