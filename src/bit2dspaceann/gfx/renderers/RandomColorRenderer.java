/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.gfx.renderers;

import bit2dspaceann.gfx.Screen;
import java.util.Random;

/**
 *
 * @author Bo
 */
public class RandomColorRenderer {

    private static final Random rnd = new Random();
    private static final int numPixelsPerRender = 1000;

    public static void render(Screen screen) {

//        for (int i = 0; i < screen.pWidth; i++) {
//            for (int j = 0; j < screen.pHeight; j++) {
//                int colorIndex = rnd.nextInt(6*6*6);
//                screen.renderPixelWithColorIndex(i, j, colorIndex);
//            }
//        }

        for (int i = 0; i < numPixelsPerRender; i++) {
            int x = rnd.nextInt(screen.pWidth);
            int y = rnd.nextInt(screen.pHeight);
            int c = rnd.nextInt(6*6*6);
            screen.renderPixelWithColorIndex(x, y, c);
        }

    }


}
