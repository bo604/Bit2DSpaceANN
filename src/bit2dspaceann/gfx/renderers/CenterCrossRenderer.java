/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.gfx.renderers;

import bit2dspaceann.gfx.Screen;

/**
 *
 * @author Bo
 */
public class CenterCrossRenderer {


    public static void render(Screen screen) {

        int x = (screen.pWidth >> 1);
        int y = (screen.pHeight >> 1);

        for (int i = 0; i < screen.pWidth; i++) {
            for (int j = 0; j < screen.pHeight; j++) {

                screen.renderPixelWithColorIndex(i, y, 0);
                screen.renderPixelWithColorIndex(x, j, 0);

            }

        }

    }

}
