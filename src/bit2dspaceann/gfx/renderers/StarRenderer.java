/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.gfx.renderers;

import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

/**
 *
 * @author Bo
 */
public class StarRenderer {


    private int[] pixels;



    public StarRenderer() {
        init();
    }

    private boolean isInit = false;

    private void init() {

        int width = Gfx.IMAGE_PIXEL_WIDTH;
        int height = Gfx.IMAGE_PIXEL_HEIGHT;
        String imagePath = Gfx.BG_PATHS[3];
        
        BufferedImage image = null;
        try {
            image = ImageIO.read(StarRenderer.class.getResource(imagePath));
        } catch (Exception e) {
            System.out.println("Failed loading star image: " + imagePath);
            e.printStackTrace();
        }
        if(image==null) {
            return;
        }

        pixels = image.getRGB(0, 0, width, height, null, 0, width);
        isInit = true;
    }



    public void render(Screen screen) {
        if(isInit) {
            System.arraycopy(pixels, 0, screen.pixels, 0, pixels.length);
        }
    }


}
