/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.gfx.renderers;

import bit2dspaceann.gfx.Screen;
import bit2dspaceann.util.Rect2Di;
import java.awt.Point;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author Bo
 */
public class GeoRenderer {
    
    
    public static void fillRect(Screen screen, int color, int x, int y, int w, int h) {
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                screen.renderPixelWithRGB(x+i, y+j, color);
            }
        }
    }
    
    private static final float twoPi = (float)(2*Math.PI);
    
    public static void drawCircle(Screen screen, int color, int x, int y, float r, int n) {
        float dangle = twoPi / n;
        for (int i = 0; i < n; i++) {
            float angle = dangle * i;
            float dx = r * (float)Math.cos(angle);
            float dy = r * (float)Math.sin(angle);
            screen.renderPixelWithRGB((int)(x+dx), (int)(y+dy), color);
        }
    }

    public static void drawCircleThick(Screen screen, int color, int x, int y, float r, int n) {
        float dangle = twoPi / n;
        for (int i = 0; i < n; i++) {
            float angle = dangle * i;
            float dx = r * (float)Math.cos(angle);
            float dy = r * (float)Math.sin(angle);
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < 2; k++) {
                    screen.renderPixelWithRGB((int)(x+dx+j), (int)(y+dy+k), color);
                }
            }
        }
    }

    public static void drawRect(Screen screen, int color, Rectangle2D rect) {
        int x = (int)Math.round(rect.getX());
        int y = (int)Math.round(rect.getY());
        int w = (int)Math.round(rect.getWidth());
        int h = (int)Math.round(rect.getHeight());
        drawLine(screen, color, x, y, x+w, y);
        drawLine(screen, color, x+w, y, x+w, y+h);
        drawLine(screen, color, x+w, y+h, x, y+h);
        drawLine(screen, color, x, y+h, x, y);
    }
    
    public static void drawRect(Screen screen, int color, Rect2Di r) {
        drawLine(screen, color, r.x, r.y, r.x+r.w, r.y);
        drawLine(screen, color, r.x+r.w, r.y, r.x+r.w, r.y+r.h);
        drawLine(screen, color, r.x+r.w, r.y+r.h, r.x, r.y+r.h);
        drawLine(screen, color, r.x, r.y+r.h, r.x, r.y);
    }
    
    public static void drawLine(Screen screen, int color, Point from, Point to) {
        drawLine(screen, color, from.x, from.y, to.x, to.y);
    }
    
    public static void drawLine(Screen screen, int color, int x1, int y1, int x2, int y2) {
        
        float dx = x2-x1;
        float dy = y2-y1;

        float nx = dx > 0 ? dx : -dx;
        float ny = dy > 0 ? dy : -dy;
        
        if(nx >= ny) {
//            renderLineX(screen, color, x1, x2, y1, y2);
            drawLineX(screen, color, x1, y1, x2, y2);
        }
        else {
            drawLineY(screen, color, x1, y1, x2, y2);
        }
        
    }
    
    private static void drawLineX(Screen screen, int color, int x1, int y1, int x2, int y2) {
//    private static void renderLineX(Screen screen, int color, int x1, int x2, int y1, int y2) {
        if(x1==x2) {
            screen.renderPixelWithRGB(x1, y1, color);
            return;
        }
        if(x2<x1) {
            int tmp = x1; x1 = x2; x2 = tmp;
                tmp = y1; y1 = y2; y2 = tmp;
        }
        int nx = x2-x1;
        float dy = (float)(y2-y1) / nx;
        for (int i = 0; i < nx; i++) {
            int y = y1 + (int)Math.round(i*dy);
            screen.renderPixelWithRGB(x1+i, y, color);
        }
    }
    
    private static void drawLineY(Screen screen, int color, int x1, int y1, int x2, int y2) {
//    private static void renderLineY(Screen screen, int color, int x1, int x2, int y1, int y2) {
        if(y1==y2) {
            screen.renderPixelWithRGB(x1, y1, color);
            return;
        }
        if(y2<y1) {
            int tmp = x1; x1 = x2; x2 = tmp;
                tmp = y1; y1 = y2; y2 = tmp;
        }
        int ny = y2-y1;
        float dx = (float)(x2-x1) / ny;
        for (int i = 0; i < ny; i++) {
//            int y = y1 + (int)Math.round(i * dy);
            int x = x1 + (int)Math.round(i * dx);
            screen.renderPixelWithRGB(x, y1+i, color);
        }
    }
    
    
}
