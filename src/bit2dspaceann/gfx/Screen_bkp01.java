/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.gfx;

import bit2dspaceann.gfx.spriteutil.SpriteSheet;

/**
 *
 *
 * LONG COLOR (8 color scheme) RENDERING:
 * this takes a long code color, which contains 8 colors, one in each byte in range [0..6*6*6]
 * in sheet.pixels are entries in range [0..7] (the number of the color to display (one of the eight available in the long color))
 * to get the correct rgb color as int, which will be drawn on the screen,
 * take the sheet.pixel entry, multiply by 8
 * ( = number of bits to get to the corresponding color in the long)
 * shift the long color a number of bits corresponding to that
 * cap the shifted color to 255(=0xFF) to get rid of the higher bytes.
 * and TADA! an int rgb value to draw into the actual screen canvas image
 * REQUIRES TESTING!s
 *
 * @author Bo
 */
public class Screen_bkp01 {

    // Temp. array for rendering. MUST BE COMMITTED! (to GameCanvas)
    public int[] pixels = null;

    // "cam" position
    public int xOffset = 0;
    public int yOffset = 0;

    // BufferedImage size in pixels of the GameCanvas
    public int pWidth = -1;
    public int pHeight = -1;

//    // The spite sheet to use for rendering
//    public SpriteSheet sheet = null;


//    private static final int COLOR_STEPS = 6;
    private int[] colorTable = null;        // = new int[COLOR_STEPS * COLOR_STEPS * COLOR_STEPS];  // 6^3 colors


    // The spite sheets available for rendering on this screen
    private SpriteSheet[] sheets = null;


    public float[] sin = new float[] {
        0,0.71f,1,0.71f,0,-0.71f,-1,-0.71f
    };
    public float[] cos = new float[] {
        1,0.71f,0,-0.71f,-1,-0.71f,0,0.71f,
    };


//    public float sin(int angle) {
//        return sin[angle & 32]  ... make this happen? some subdivision of 360 degrees with 2^something entries...
//    }

    public boolean init() {
        return init(Gfx.IMAGE_PIXEL_WIDTH, Gfx.IMAGE_PIXEL_HEIGHT);
    }

    public boolean init(int pWidth, int pHeight) {
        this.pWidth = pWidth;
        this.pHeight = pHeight;
        this.pixels = new int[pWidth * pHeight];
        this.sheets = new SpriteSheet[0xFF];
        /*
         * Initialize fixed array of colors that will be used throughout the game
         * with a total of 6*6*6 colors
         */
        colorTable = ColorsLong.constructColorTable();
        /*
         * build table array for trigonometric functions
         */
        buildTrigonometricArrays();

        return true;
    }


    private void buildTrigonometricArrays() {
        int nAngles = Gfx.NUM_ANGLES;
        sin = new float[nAngles];
        cos = new float[nAngles];
        for (int i = 0; i < nAngles; i++) {
            double rad = Math.toRadians( i );
            sin[i] = (float)Math.sin(rad);
            cos[i] = (float)Math.cos(rad);
        }
    }

    public void setOffset(int xOff, int yOff) {
        xOffset = xOff;
        yOffset = yOff;
    }

    public void setSpriteSheet(int idx, SpriteSheet spriteSheet) {
        sheets[idx] = spriteSheet;
    }


//    /**
//     * super cheap post-processing example?
//     * @param rgb
//     */
//    public void and(int[] rgb) {
//        System.out.println("rgb length: " + rgb.length);
//        System.out.println("pix length: " + pixels.length);
//        for (int i = 0; i < rgb.length; i++) {
//            if(i >= pixels.length) return;
//            pixels[i] &= rgb[i];
//        }
//    }


    /**
     * @param xPixel
     * @param yPixel
     * @param colorIndex
     * @param scale
     */
    public void renderPixelWithColorIndex(int xPixel, int yPixel, int colorIndex) {
        int color = colorTable[colorIndex];
        // Out of bounds?
        if( yPixel < 0 || yPixel >= pHeight ) return;
        // Out of bounds?
        if( xPixel < 0 || xPixel >= pWidth ) return;
        // Finally fill the pixel
        pixels[ xPixel + yPixel * pWidth ] = color;
    }


    /**
     * Convenience method for rendering with scale = 1 and no mirroring.
     * @param xPos Pixel x position to be rendered to.
     * @param yPos Pixel y position to be rendered to.
     * @param tileIndex The 1-d index of the tile of the sprite sheet to be rendered.
     * @param color The color scheme (i.e. C555 or C999)
     */
    public void render(int xPos, int yPos, byte sheetIndex, int tileIndex, long color) {
        render(xPos, yPos, sheetIndex, tileIndex, color, 1, Gfx.BIT_MIRROR_NONE);
    }


    /**
     * Renders a pixel at canvas position xPos/yPos according to the sprite sheet tile on tileIndex,
     * @param xPos Pixel x position to be rendered to.
     * @param yPos Pixel y position to be rendered to.
     * @param tileIndex The 1-d index of the tile of the sprite sheet to be rendered.
     * @param color The color scheme (i.e. C555 or C999)
     * @param scale The scaling of the tile during the render
     * @param mirrorX Mirror in x?
     * @param mirrorY Mirror in y?
     */
    public void render(int xPos, int yPos, byte sheetIndex, int tileIndex, long color, int scale, int mirrorDir) {
        // Apply offsets
        xPos -= xOffset;
        yPos -= yOffset;
        // Eval mirrors
        boolean mirrorX = ( mirrorDir & Gfx.BIT_MIRROR_X ) > 0;
        boolean mirrorY = ( mirrorDir & Gfx.BIT_MIRROR_Y ) > 0;
        // Find spritesheet tile in 2D
        int xTile = tileIndex & Gfx.NTIS_L2;
        int yTile = tileIndex >> Gfx.NTIS_L2;
        // Calc tile offset... d'oh
        int tileOffset = (xTile << Gfx.TILE_SIZE_LOG2) + (yTile << Gfx.TILE_SIZE_LOG2) * sheets[0].pWidth;
        // draw the tile
        drawFullTileAtPosition(xPos, yPos, sheetIndex, tileOffset, color, scale, mirrorX, mirrorY);
    }

    /**
     * @param xPos the x position in pixels on the actual screen to renter to (from left)
     * @param yPos the y position in pixels on the actual screen to renter to (from top)
     * @param sheetIndex the sprite sheet to draw from
     * @param tileIndex the index of the tile to draw from
     * @param color
     * @param angle
     */
    public void renderWithAngle(int xPos, int yPos, byte sheetIndex, int tileIndex, long color, int angle) {
        // Apply offsets
        xPos -= xOffset;
        yPos -= yOffset;
        // Find spritesheet tile in 2D
        int xTile = tileIndex & Gfx.NUM_TILES_IN_SHEET_ROWS_LOG2;
        int yTile = tileIndex >> Gfx.NUM_TILES_IN_SHEET_ROWS_LOG2;

        // Calc tile pixel offset... d'oh
//        int tilePixelOffset = (xTile << Gfx.TILE_SIZE_LOG2) + (yTile << Gfx.TILE_SIZE_LOG2) * sheets[0].pWidth;
        int tilePixelOffset = (xTile * Gfx.TILE_SIZE) + (yTile * Gfx.TILE_SIZE) * sheets[sheetIndex].pWidth;

        // draw the tile with angle, no mirroring, scale 1
//        drawFullTileAtPositionAndAngleScaleOneNoMirror( xPos, yPos, sheetIndex, tilePixelOffset, color, angle );


        drawFullTileAtPositionAndAngleScaleOneNoMirrorTwoPixels( xPos, yPos, sheetIndex, tilePixelOffset, color, angle );
    }


    /**
     * @param xPos the x position in pixels on the actual screen to renter to (from left)
     * @param yPos the y position in pixels on the actual screen to renter to (from top)
     * @param sheetIndex the sprite sheet to draw from
     * @param tileIndex the index of the tile to draw from
     * @param color
     * @param angle
     */
    public void renderWithAngle2DTileIndices(int xPos, int yPos, byte sheetIndex, int xTile, int yTile, long color, int angle) {
        // Apply offsets
        xPos -= xOffset;
        yPos -= yOffset;

//        // Find spritesheet tile in 2D
//        int xTile = tileIndex & Gfx.NUM_TILES_IN_SHEET_ROWS_LOG2;
//        int yTile = tileIndex >> Gfx.NUM_TILES_IN_SHEET_ROWS_LOG2;

        // Calc tile pixel offset... d'oh
//        int tilePixelOffset = (xTile << Gfx.TILE_SIZE_LOG2) + (yTile << Gfx.TILE_SIZE_LOG2) * sheets[0].pWidth;
        int tilePixelOffset = (xTile * Gfx.TILE_SIZE) + (yTile * Gfx.TILE_SIZE) * sheets[sheetIndex].pWidth;

//         //draw the tile with angle, no mirroring, scale 1 --> faster
//        drawFullTileAtPositionAndAngleScaleOneNoMirror( xPos, yPos, sheetIndex, tilePixelOffset, color, angle );

        //draw four pixels for each     --> one extra each might be enough
        drawFullTileAtPositionAndAngleScaleOneNoMirrorTwoPixels( xPos, yPos, sheetIndex, tilePixelOffset, color, angle );
    }


    private void drawFullTileAtPosition(int xPos, int yPos,
            byte sheetIndex, int tilePixelOffset, long color, int scale, boolean mirrorX, boolean mirrorY) {
        
        int scaleMask = scale-1;
        // Draw full tile
        for (int y = 0; y < Gfx.TILE_SIZE; y++) {
            // Out of bounds?
            int ySheet = y;
            if(mirrorY) ySheet = Gfx.TILE_SIZE - 1 - y;

            int yPixel = y + yPos + ( y * scaleMask ) - ((scaleMask << 3) / 2);

            // iterate over x pixels.. or something T.T
            for (int x = 0; x < Gfx.TILE_SIZE; x++) {
                int xSheet = x;
                if(mirrorX) xSheet = Gfx.TILE_SIZE - 1 - x;

                int xPixel = x + xPos + ( x * scaleMask ) - ((scaleMask << 3) / 2);

                // Calculate index of the SpriteSheet pixel which has to be rendered
                int sheetPixelIndex = xSheet + ySheet*sheets[sheetIndex].pWidth + tilePixelOffset;

                    // get color index from the given byte of the long color
                    int colorIndex = (int)((color >> ( sheets[sheetIndex].pixels[sheetPixelIndex] << 3 )) & 0xFF);

                    // If not transparent, write to pixels
                    if(colorIndex<255) {
                        // Translate colorIndex[0..6*6*6] to col (int RGB) using the precalulated colors array
                        int col = colorTable[colorIndex];
                        renderScaledRGBPixelAtPosition(xPixel, yPixel, col, scale);
                    }
            }
        }

    }


    public void drawFullTileAtPositionAndAngleScaleOneNoMirror(int xPos, int yPos,
            byte sheetIndex, int tileOffset, long color, int angle) {


        // Draw full tile
        for (int y = 0; y < Gfx.TILE_SIZE; y++) {
            // Out of bounds?
            int ySheet = y;
//            if(mirrorY) ySheet = Gfx.TILE_SIZE - 1 - y;

            int yPixel = y + yPos;

            // iterate over x pixels.. or something T.T
            for (int x = 0; x < Gfx.TILE_SIZE; x++) {
                int xSheet = x;
//                if(mirrorX) xSheet = Gfx.TILE_SIZE - 1 - x;

                int xPixel = x + xPos;

                // Calculate index of the SpriteSheet pixel which has to be rendered
                int sheetPixelIndex = xSheet + ySheet*sheets[sheetIndex].pWidth + tileOffset;

                    // get the color index from the given long color byte
                    int colorIndex = (int)((color >> ( sheets[sheetIndex].pixels[sheetPixelIndex] << 3 )) & 0xFF);

                    // If not transparent, write to pixels
                    if(colorIndex<255) {

                        // rotation position (center of tile)
                        int xRot = xPos + 3;
                        int yRot = yPos + 3;

                        // difference vector from rot pos to currently drawn pixel
                        int dx = xPixel - xRot;
                        int dy = yPixel - yRot;

                        // rotate
                        float dxRotated = cos[angle] * dx - sin[angle] * dy;
                        float dyRotated = sin[angle] * dx + cos[angle] * dy;

                        // add to center tile pos for new draw position
                        int xDraw = Math.round(xRot + dxRotated);
                        int yDraw = Math.round(yRot + dyRotated);


//                        // Translate colorIndex[0..6*6*6] to col (int RGB) using the precalulated colors array
//                        int col = colorTable[colorIndex];
//                        renderScaledPixelAtPosition(xDraw, yDraw, col, 1);
                        renderPixelWithColorIndex(xDraw, yDraw, colorIndex);
                    }
            }
        }

    }

    public void drawFullTileAtPositionAndAngleScaleOneNoMirrorTwoPixels(int xPos, int yPos,
            byte sheetIndex, int tileOffset, long color, int angle) {


        // Draw full tile
        for (int y = 0; y < Gfx.TILE_SIZE; y++) {
            // Out of bounds?
            int ySheet = y;
//            if(mirrorY) ySheet = Gfx.TILE_SIZE - 1 - y;

            int yPixel = y + yPos;

            // iterate over x pixels.. or something T.T
            for (int x = 0; x < Gfx.TILE_SIZE; x++) {
                int xSheet = x;
//                if(mirrorX) xSheet = Gfx.TILE_SIZE - 1 - x;

                int xPixel = x + xPos;

                // Calculate index of the SpriteSheet pixel which has to be rendered
                int sheetPixelIndex = xSheet + ySheet*sheets[sheetIndex].pWidth + tileOffset;

                    // get the color index from the given long color byte
                    int colorIndex = (int)((color >> ( sheets[sheetIndex].pixels[sheetPixelIndex] << 3 )) & 0xFF);

                    // If not transparent, write to pixels
                    if(colorIndex<255) {

                        // rotation position (center of tile)
                        int xRot = xPos + 3;
                        int yRot = yPos + 3;

                        // difference vector from rot pos to currently drawn pixel
                        int dx = xPixel - xRot;
                        int dy = yPixel - yRot;

                        // rotate
                        float dxRotated = cos[angle] * dx - sin[angle] * dy;
                        float dyRotated = sin[angle] * dx + cos[angle] * dy;

                        // add to center tile pos for new draw position
                        int xDraw = Math.round(xRot + dxRotated);
                        int yDraw = Math.round(yRot + dyRotated);


//                        // Translate colorIndex[0..6*6*6] to col (int RGB) using the precalulated colors array
                        int col = colorTable[colorIndex];
                        renderScaledRGBPixelAtPosition(xDraw, yDraw, col, 2);
//                        renderPixelWithColorIndex(xDraw, yDraw, colorIndex);
                    }
            }
        }

    }

    
    
    private void renderPixelWithRGB(int xPixel, int yPixel, int rgb) {
        // Out of bounds?
        if( yPixel < 0 || yPixel >= pHeight ) return;
        // Out of bounds?
        if( xPixel < 0 || xPixel >= pWidth ) return;
        // Finally fill the pixel
        pixels[ xPixel + yPixel * pWidth ] = rgb;
    }

    private void renderScaledRGBPixelAtPosition(int xPixel, int yPixel, int rgb, int scale) {
        // yScaling
        for (int yScale = 0; yScale < scale; yScale++) {
            // Out of bounds?
            if( yPixel + yScale < 0 || yPixel + yScale >= pHeight ) continue;
            // xScaling
            for (int xScale = 0; xScale < scale; xScale++) {
                // Out of bounds?
                if( xPixel + xScale < 0 || xPixel + xScale >= pWidth ) continue;
                // Finally fill the pixel
                pixels[ (xPixel + xScale) + ((yPixel + yScale) * pWidth) ] = rgb;
            }
        }
    }




}



//    private void drawFullTileAtPositionAndAngle(int xPos, int yPos,
//            byte sheetIndex, int tileOffset, long color,
//            int scale, boolean mirrorX, boolean mirrorY, int angle) {
//
//        if(!mirrorX && ! mirrorY && scale==1) {
//            drawFullTileAtPositionAndAngleScaleOneNoMirror(xPos, yPos,
//            sheetIndex, tileOffset, color, angle);
//        }
//
//        throw new UnsupportedOperationException("rotation render with mirror or scale not supported, dude! ...yet!");
//    }




// <editor-fold defaultstate="collapsed" desc="old light stuff">

//    /**
//     * Renders a pixel at canvas position xPos/yPos according to the sprite sheet tile on tileIndex,
//     * @param xPos Pixel x position to be rendered to.
//     * @param yPos Pixel y position to be rendered to.
//     * @param tileIndex The 1-d index of the tile of the sprite sheet to be rendered.
//     * @param color The color scheme (i.e. C555 or C999)
//     * @param scale The scaling of the tile during the render
//     * @param mirrorX Mirror in x?
//     * @param mirrorY Mirror in y?
//     */
//    public void renderWithLight(int xPos, int yPos, byte sheetIndex, int tileIndex, long color, float alpha, int scale, int mirrorDir) {
//        // Apply offsets
//        xPos -= xOffset;
//        yPos -= yOffset;
//
//        boolean mirrorX = ( mirrorDir & Gfx.BIT_MIRROR_X ) > 0;
//        boolean mirrorY = ( mirrorDir & Gfx.BIT_MIRROR_Y ) > 0;
//
//        // Find spritesheet tile in 2D
//        int xTile = tileIndex % Gfx.NUM_TILES_IN_SHEET_ROWS;
//        int yTile = tileIndex / Gfx.NUM_TILES_IN_SHEET_ROWS;
//        int scaleMask = scale-1;
//
////        int tileOffset = (xTile << Gfx.TILE_SIZE_LOG2) + (yTile << Gfx.TILE_SIZE_LOG2) * sheet.pWidth;
//        int tileOffset = (xTile << Gfx.TILE_SIZE_LOG2) + (yTile << Gfx.TILE_SIZE_LOG2) * sheets[0].pWidth;
//
//        // iterate over tile y pixels
//        for (int y = 0; y < Gfx.TILE_SIZE; y++) {
//            // Out of bounds?
//            int ySheet = y;
//            if(mirrorY) ySheet = Gfx.TILE_SIZE - 1 - y;
//
//            int yPixel = y + yPos + ( y * scaleMask ) - ((scaleMask << 3) / 2);
//
//            // iterate over tile x pixels
//            for (int x = 0; x < Gfx.TILE_SIZE; x++) {
//                int xSheet = x;
//                if(mirrorX) xSheet = Gfx.TILE_SIZE - 1 - x;
//
//                int xPixel = x + xPos + ( x * scaleMask ) - ((scaleMask << 3) / 2);
//
//                // Calculate index of the SpiteSheet pixel which has to be rendered
////                int sheetPixelIndex = xSheet + ySheet*sheet.pWidth + tileOffset;
//                int sheetPixelIndex = xSheet + ySheet*sheets[sheetIndex].pWidth + tileOffset;
//
//                if(sheets[sheetIndex].is8Color) {
//                    /*
//                     * LONG COLOR (8 color scheme) RENDERING:
//                     * this takes a long code color, which contains 8 colors, one in each byte in range [0..6*6*6]
//                     * in sheet.pixels are entries in range [0..7] (the number of the color to display (one of the eight available in the long color))
//                     * to get the correct rgb color as int, which will be drawn on the screen,
//                     * take the sheet.pixel entry, multiply by 8
//                     * ( = number of bits to get to the corresponding color in the long)
//                     * shift the long color a number of bits corresponding to that
//                     * cap the shifted color to 255(=0xFF) to get rid of the higher bytes.
//                     * and TADA! an int rgb value to draw into the actual screen canvas image
//                     * REQUIRES TESTING!s
//                     */
//    //                int colorIndex = (int)((color >> ( sheet.pixels[sheetPixelIndex] << 3 )) & 0xFF);
//                    int colorIndex = (int)((color >> ( sheets[sheetIndex].pixels[sheetPixelIndex] << 3 )) & 0xFF);
//
//                    // If not transparent, write to pixels
//                    if(colorIndex<255) {
//                        // Translate colorIndex[0..6*6*6] to col (int RGB) using the precalulated colors array
//                        int col = colorTable[colorIndex];
//
//                        // Apply alpha as color factor
////                        int r = (int)(((col >> 16) & 0xFF) * srcA);
////                        int g = (int)(((col >> 8) & 0xFF) * srcA);
////                        int b = (int)((col & 0xFF) * srcA);
//
//
//                        // Apply Light Map
////                        int lightColor = lightMap.getLight(xPixel >> Gfx.TILE_SIZE_LOG2, yPixel >> Gfx.TILE_SIZE_LOG2);
//                        int lightColor = lightMap.getLight((xPos+xOffset) >> Gfx.TILE_SIZE_LOG2, (yPos+yOffset) >> Gfx.TILE_SIZE_LOG2);
//                        // FOR NOW: float factors
//                        float mapAlpha = ((lightColor >> 24) & 0xFF) / 255.0f;
////                        float mapR = ((lightColor >> 16) & 0xFF) / 255.0f;
////                        float mapG = ((lightColor >> 8) & 0xFF) / 255.0f;
////                        float mapB = (lightColor & 0xFF) / 255.0f;
////                        // map int color channels
////                        int mr = (lightColor >> 16) & 0xFF;
////                        int mg = (lightColor >> 8) & 0xFF;
////                        int mb = lightColor & 0xFF;
//
//
//                        // total alpha
//                        float a = Math.max(alpha, mapAlpha);
//
//                        if(a<0.1) a = 0.1f;
//
//                        // tile colors
//                        int tr = (col >> 16) & 0xFF;
//                        int tg = (col >> 8) & 0xFF;
//                        int tb = col & 0xFF;
//
////                        float ac = alpha + mapA * (1-alpha);
//
//                        int r = (int)(tr * a);
//                        int g = (int)(tg * a);
//                        int b = (int)(tb * a);
////                        int r = (int)((tr * alpha + mR * mapA * (1-alpha))/ac);
////                        int g = (int)((tg * alpha + mG * mapA * (1-alpha))/ac);
////                        int b = (int)((tb * alpha + mB * mapA * (1-alpha))/ac);
////                        int r = (int)(tr * alpha) + (int)(mR * (1-a));
////                        int g = (int)(tg * alpha) + (int)(mG * (1-a));
////                        int b = (int)(tb * alpha) + (int)(mB * (1-a));
////                        int r = (int)(((col >> 16) & 0xFF) * a) + (int)(mR * (1-a));
////                        int g = (int)(((col >> 8) & 0xFF) * a) + (int)(mG * (1-a));
////                        int b = (int)((col & 0xFF) * a) + (int)(mB * (1-a));
////                        int r = (int)(((col >> 16) & 0xFF) * a * mapR);
////                        int g = (int)(((col >> 8) & 0xFF) * a * mapG);
////                        int b = (int)((col & 0xFF) * a * mapB);
////                        int r = (int)(((col >> 16) & 0xFF) * a);
////                        int g = (int)(((col >> 8) & 0xFF) * a);
////                        int b = (int)((col & 0xFF) * a);
//
//
//
//                        col = 0xFF000000 | (r<<16) | (g<<8) | b;
//
//                        renderScaledPixelAtPosition(col, xPixel, yPixel, scale);
//                    }
//                }
//                else {
//                    /*
//                     * Direct color rendering
//                     */
//                    int col = sheets[sheetIndex].pixels[sheetPixelIndex];
//                    if(col != sheets[sheetIndex].directColorTransparent) {
//                        renderScaledPixelAtPosition(col, xPixel, yPixel, scale);
//                    }
//                }
//            }
//        }
//    }
//    /**
//     * Renders a pixel at canvas position xPos/yPos according to the sprite sheet tile on tileIndex,
//     * @param xPos Pixel x position to be rendered to.
//     * @param yPos Pixel y position to be rendered to.
//     * @param tileIndex The 1-d index of the tile of the sprite sheet to be rendered.
//     * @param color The color scheme (i.e. C555 or C999)
//     * @param scale The scaling of the tile during the render
//     * @param mirrorX Mirror in x?
//     * @param mirrorY Mirror in y?
//     */
//    public void renderVisible(int xPos, int yPos, byte sheetIndex, int tileIndex,
//            long color, int scale, int mirrorDir, PixelVisibilityChecker visChecker) {
//
//        // Apply offsets
//        xPos -= xOffset;
//        yPos -= yOffset;
//
//        boolean mirrorX = ( mirrorDir & Gfx.BIT_MIRROR_X ) > 0;
//        boolean mirrorY = ( mirrorDir & Gfx.BIT_MIRROR_Y ) > 0;
//
//        // Find spritesheet tile in 2D
//        int xTile = tileIndex % Gfx.NUM_TILES_IN_SHEET_ROWS;
//        int yTile = tileIndex / Gfx.NUM_TILES_IN_SHEET_ROWS;
//        int scaleMask = scale-1;
//
////        int tileOffset = (xTile << Gfx.TILE_SIZE_LOG2) + (yTile << Gfx.TILE_SIZE_LOG2) * sheet.pWidth;
//        int tileOffset = (xTile << Gfx.TILE_SIZE_LOG2) + (yTile << Gfx.TILE_SIZE_LOG2) * sheets[0].pWidth;
//
//        // Draw full tile? Not sure about the loops at the moment...
//        for (int y = 0; y < Gfx.TILE_SIZE; y++) {
//            // Out of bounds?
//            int ySheet = y;
//            if(mirrorY) ySheet = Gfx.TILE_SIZE - 1 - y;
//
//            int yPixel = y + yPos + ( y * scaleMask ) - ((scaleMask << 3) / 2);
//
//            // iterate over x pixels.. or something T.T
//            for (int x = 0; x < Gfx.TILE_SIZE; x++) {
//                int xSheet = x;
//                if(mirrorX) xSheet = Gfx.TILE_SIZE - 1 - x;
//
//                int xPixel = x + xPos + ( x * scaleMask ) - ((scaleMask << 3) / 2);
//
//                // VISIBLE?
//                if(visChecker.isPixelVisible(xPixel, yPixel)) {
//
//                    // Calculate index of the SpiteSheet pixel which has to be rendered
//    //                int sheetPixelIndex = xSheet + ySheet*sheet.pWidth + tileOffset;
//                    int sheetPixelIndex = xSheet + ySheet*sheets[sheetIndex].pWidth + tileOffset;
//
//                    if(sheets[sheetIndex].is8Color) {
//                        /*
//                         * LONG COLOR (8 color scheme) RENDERING:
//                         * this takes a long code color, which contains 8 colors, one in each byte in range [0..6*6*6]
//                         * in sheet.pixels are entries in range [0..7] (the number of the color to display (one of the eight available in the long color))
//                         * to get the correct rgb color as int, which will be drawn on the screen,
//                         * take the sheet.pixel entry, multiply by 8
//                         * ( = number of bits to get to the corresponding color in the long)
//                         * shift the long color a number of bits corresponding to that
//                         * cap the shifted color to 255(=0xFF) to get rid of the higher bytes.
//                         * and TADA! an int rgb value to draw into the actual screen canvas image
//                         * REQUIRES TESTING!
//                         */
//        //                int colorIndex = (int)((color >> ( sheet.pixels[sheetPixelIndex] << 3 )) & 0xFF);
//                        int colorIndex = (int)((color >> ( sheets[sheetIndex].pixels[sheetPixelIndex] << 3 )) & 0xFF);
//
//                        // If not transparent, write to pixels
//                        if(colorIndex<255) {
//                            // Translate colorIndex[0..6*6*6] to col (int RGB) using the precalulated colors array
//                            int col = colorTable[colorIndex];
//                            renderScaledPixelToScaledPosition(col, xPixel, yPixel, scale);
//                        }
//                    }
//                    else {
//                        /*
//                         * Direct color rendering
//                         */
//                        int col = sheets[sheetIndex].pixels[sheetPixelIndex];
//                        if(col != sheets[sheetIndex].directColorTransparent) {
//                            renderScaledPixelToScaledPosition(col, xPixel, yPixel, scale);
//                        }
//                    }
//                }
//
//                // NOT VISIBLE
//                else {
//                    renderScaledPixelToScaledPosition(0x000000, xPixel, yPixel, scale);
//                }
//
//
//            }
//        }
//    }

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="old">

//    private void render(int xPos, int yPos, int tileIndex, int color, int mirrorDir) {
//        boolean mirrorX = ( mirrorDir & BIT_MIRROR_X ) > 0;
//        boolean mirrorY = ( mirrorDir & BIT_MIRROR_Y ) > 0;
//        render(xPos, yPos, tileIndex, color, mirrorX, mirrorY);
//    }
//
//
//    private void render(int xPos, int yPos, int tileIndex, int color, boolean mirrorX, boolean mirrorY) {
        // Apply offsets
//        xPos -= xOffset;
//        yPos -= yOffset;
//        // Find tile in 2D
//        int xTile = tileIndex % 32;
//        int yTile = tileIndex / 32;
//
//        int tileOffset = (xTile << TILE_WIDTH_LOG2) + (yTile << TILE_WIDTH_LOG2) * sheet.width;
//
//        // Draw full tile? Not sure about the loops at the moment...
//        for (int y = 0; y < TILE_WIDTH; y++) {
//            // Out of bounds?
//            if( y+yPos<0 || y+yPos>=height ) continue;
//            int ySheet = y;
//            if(mirrorY) ySheet = TILE_WIDTH - 1 - y;
//            for (int x = 0; x < TILE_WIDTH; x++) {
//                // Out of bounds?
//                if( x+xPos<0 || x+xPos>=width ) continue;
//                int xSheet = x;
//                if(mirrorX) xSheet = TILE_WIDTH - 1 - x;
//
//                int sheetPixelIndex = xSheet + ySheet*sheet.width + tileOffset;
//                // Calculate the color for the pixel
//                int col = (color >> ( sheet.pixels[sheetPixelIndex] * TILE_WIDTH )) & 255;
//                // If not transparent, write to pixels
//                if(col<255) pixels[ (x+xPos) + ((y+yPos)*width) ] = col;
//            }
//        }
//    }



//    public void render(int[] pixels, int offset, int row) {
//        int yTileMin = yOffset >> TILE_WIDTH_LOG2;            // divide by 8 (npixels per sheet tile?
//        int yTileMax = (yOffset + height) >> TILE_WIDTH_LOG2; // divide by 8 (npixels per sheet tile?
//        for (int yTile = yTileMin ; yTile <= yTileMax; yTile++) {
//            int yMin = yTile * TILE_WIDTH - yOffset;
//            int yMax = yMin + TILE_WIDTH;
//            if(yMin<0) yMin = 0;
//            if(yMax>height) yMax = height;
//
//            int xTileMin = xOffset >> TILE_WIDTH_LOG2;
//            int xTileMax = (xOffset + width) >> TILE_WIDTH_LOG2;
//            for (int xTile = xTileMin; xTile <= xTileMax; xTile++) {
//                int xMin = xTile * TILE_WIDTH - xOffset;
//                int xMax = xMin + TILE_WIDTH;
//                if(xMin<0) xMin = 0;
//                if(xMax>width) xMax = width;
//
//                int tileIndex = (xTile & MAP_WIDTH_MASK) + (yTile & MAP_WIDTH_MASK) * MAP_WIDTH;
//
//                for (int y = yMin; y < yMax; y++) {
//                    int sheetPixel = ((xMin+xOffset)&TILE_WIDTH_MASK) + (((y+yOffset)&TILE_WIDTH_MASK) * sheet.width);
//                    int tilePixel = offset + xMin + y * row;
//
//                    for (int x = xMin; x < xMax; x++) {
//                        int colorIndex = tileIndex * nColors + sheet.pixels[sheetPixel++];
//                        pixels[tilePixel++] = colors[colorIndex];
//                    }
//
//                }
//            }
//        }
//    }
// </editor-fold>