/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.gfx.spriteutil;

import bit2dspaceann.gfx.ColorsLong;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;

/**
 *
 * @author Bo
 */
public class SpriteMask {

    // sprite sheet?
    public byte sheetIndex = 0;

    // base tile?
    public int xTileBase = 0;
    public int yTileBase = 0;

    // how many tiles in x and y?
    public int xTileSize = 1;       // remove for this mask? already have the multi tile rotationmask
    public int yTileSize = 1;       // remove for this mask? already have the multi tile rotationmask

    public long color = ColorsLong.TEST_COLOR;


    public SpriteMask(int xTileBase, int yTileBase) {
        this((byte)0x00,xTileBase,yTileBase,1,1,ColorsLong.TEST_COLOR);
    }
    public SpriteMask(int xTileBase, int yTileBase, long color) {
        this((byte)0x00,xTileBase,yTileBase,1,2,color);
    }

    public SpriteMask(byte sheetIndex, int xTileBase, int yTileBase, int xTileSize, int yTileSize, long color) {
        this.sheetIndex = sheetIndex;
        this.xTileBase = xTileBase;
        this.yTileBase = yTileBase;
        this.xTileSize = xTileSize;
        this.yTileSize = yTileSize;
        this.color = color;
    }

    public void render(Screen screen, int xp, int yp) {
        for (int i = 0; i < xTileSize; i++) {
            for (int j = 0; j < yTileSize; j++) {

                int xpi = xp + (i << Gfx.TILE_SIZE_LOG2);
                int ypj = yp + (j << Gfx.TILE_SIZE_LOG2);
                int tileIndex = xTileBase + i + ((yTileBase + j) << Gfx.NUM_TILES_IN_SHEET_ROWS_LOG2);

                screen.render(xpi, ypj, sheetIndex, tileIndex, color);
//                screen.renderWithAngle2DTileIndices(xpi, ypj, sheetIndex, xTileBase+i, yTileBase+j, color, angle....);

            }
        }
    }

    /**
     * 
     * @param screen
     * @param xp the pixel position the thing should be rendered to: NOT YET CORRECTED FOR TILE NUM
     * 
     * 
     * @param yp
     * @param angle
     */
    public void render(Screen screen, int xp, int yp, int scale, int angle) {
        for (int i = 0; i < xTileSize; i++) {
            for (int j = 0; j < yTileSize; j++) {

                int xpi = xp + (i << Gfx.TILE_SIZE_LOG2)*scale;
                int ypj = yp + (j << Gfx.TILE_SIZE_LOG2)*scale;
                int tileIndex = xTileBase + i + ((yTileBase + j) << Gfx.NUM_TILES_IN_SHEET_ROWS_LOG2);

//                screen.renderWithAngle(xpi, ypj, sheetIndex, tileIndex, color, angle);

                screen.renderTileAtPosRot(xpi, ypj, sheetIndex, xTileBase+i, yTileBase+j, color, scale, angle);

            }
        }
    }


}

