/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.gfx.spriteutil;

import bit2dspaceann.gfx.Gfx;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import javax.imageio.ImageIO;

/**
 *
 * @author Bo
 */
public class SpriteSheet {

    public String sheetPath = null;

    public int pWidth = -1;
    public int pHeight = -1;
    public int[] pixels = null;

    
    public SpriteSheet() { }

    public boolean init(String sheetPath) {
        
        BufferedImage image = load(sheetPath);
        if(image==null) return false;

        this.sheetPath = sheetPath;
        pWidth = image.getWidth();
        pHeight = image.getHeight();
        // No zero dimension?
        if(pWidth<=0 || pHeight<=0) {
            System.out.println("SpriteSheet has zero dimension(s): " + sheetPath);
            return false;
        }

        pixels = image.getRGB(0, 0, pWidth, pHeight, null, 0, pWidth);
        convertPixelColors();

        // basic output of first 12 bytes
        for (int i = 0; i < 12; i++) {
            System.out.println(pixels[i]);
        }

        // Init successfull
        return true;
    }


    private BufferedImage load(String sheetPath) {
        BufferedImage image = null;
        try {
            image = ImageIO.read( SpriteSheet.class.getResourceAsStream(sheetPath) );
            System.out.println("Sprite sheet image type: " + image.getType());
        } catch (Exception e) {
            System.out.println("Sheet load fuck up: " + sheetPath);
            e.printStackTrace();
        }
        return image;
    }


    private void convertPixelColors() {
        /*
         * CONVERSION TO COLORS = 0..7
         * Current pixel color format: 0xAARRGGBB
         * where RR=GG=BB the gray scales (in 36er steps) from 0 to 255, representing abstract colors 1 to 8..
         * --> convert to color in [0..Gfx.NUM_COLORS] = [0..255/Gfx.RBG_RANGE_PER_COLOR]
         * -----> first step: remove alpha
         */
        for (int i = 0; i < pixels.length; i++) {
            pixels[i] = (pixels[i] & 0xff) / Gfx.RBG_RANGE_PER_COLOR;
        }
    }


    /**
     * for testing
     * @param n
     * @return
     */
    public int[] getLeadingBytes(int n) {
        int[] leadingBytes = Arrays.copyOfRange(pixels, 0, n);
        return leadingBytes;
    }
    



}

// <editor-fold defaultstate="collapsed" desc="old">

//        BufferedImage image = null;
//        try {
//            image = ImageIO.read( SpriteSheet.class.getResourceAsStream(path) );
//        } catch (Exception e) {
//            System.out.println("Sheet load fuck up:");
//            e.printStackTrace();
//        }
//        if(image==null) return;
//        this.sheetPath = path;
//        pWidth = image.getWidth();
//        pHeight = image.getHeight();
//
//        pixels = image.getRGB(0, 0, pWidth, pHeight, null, 0, pWidth);
//
//        // pixels format at this point: 0xAARRGGBB
//        // wanted: pixels in [0..nColors]
//
//
////        int rgbRangePerColor = 64; //(int)(255D/4D);
//
//
//        for (int i = 0; i < pixels.length; i++) {
//
////            pixels[i] = pixels[i] & 0xff;   // removes alpha
////            pixels[i] = pixels[i] / rgbRangePerColor;   // old rgb range per new color, e.g. range of 64 rgb values for 4 new colors
//            pixels[i] = (pixels[i] & 0xff) / Gfx.RBG_RANGE_PER_COLOR;
//        }
//
//        // basic output of first 8 bytes
//        for (int i = 0; i < 24; i++) {
//            System.out.println(pixels[i]);
//        }

// </editor-fold>

