/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.gfx.spriteutil;

import bit2dspaceann.gfx.ColorsLong;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;

/**
 *
 * @author Bo
 */
public class RotationSpriteMask {


    public byte sheetIndex = 0;
    
    public int xTileBase = 0;
    public int yTileBase = 24;

    public int xTileSize = 4;
    public int yTileSize = 4;

    public int renderScale = 1;

    public long color = ColorsLong.DEF_SHIP;

    int dxToRotPoint, dyToRotPoint;


    public RotationSpriteMask(int xTileBase, int yTileBase, int xTileSize, int yTileSize) {
        this((byte)0x00, xTileBase, yTileBase, xTileSize, yTileSize, ColorsLong.TEST_COLOR);
    }

    public RotationSpriteMask(int xTileBase, int yTileBase, int xTileSize, int yTileSize, long color) {
        this((byte)0x00, xTileBase, yTileBase, xTileSize, yTileSize, color);
    }

    public RotationSpriteMask(byte sheetIndex, int xTileBase, int yTileBase, int xTileSize, int yTileSize, long color) {
        this.sheetIndex = sheetIndex;
        this.xTileBase = xTileBase;
        this.yTileBase = yTileBase;
        this.xTileSize = xTileSize;
        this.yTileSize = yTileSize;
        this.color = color;
        // vector from sprite origin (topleft) to rotation point (center)
        dxToRotPoint = (xTileSize << (Gfx.TILE_SIZE_LOG2-1)) - 4;
        dyToRotPoint = (yTileSize << (Gfx.TILE_SIZE_LOG2-1)) - 4;
    }


//    /**
//     *
//     * @param screen
//     * @param xp x position in pixels on the actual screen to render this thing
//     * @param yp x position in pixels on the actual screen to render this thing
//     * @param angle
//     */
//    public void render(Screen screen, int xp, int yp, int angle) {
//
//        float cos = screen.cos[angle];
//        float sin = screen.sin[angle];
//
//        // translate xp,yp to tile center
//        xp -= ((xTileSize << Gfx.TILE_SIZE_LOG2) >> 1);
//        yp -= ((yTileSize << Gfx.TILE_SIZE_LOG2) >> 1);
//
//        // for all tiles in the given set (base to base+size)
//        for (int i = 0; i < xTileSize; i++) {
//            for (int j = 0; j < yTileSize; j++) {
//
//                int dxToRotate = ((i << Gfx.TILE_SIZE_LOG2) - dxToRotPoint);
//                int dyToRotate = ((j << Gfx.TILE_SIZE_LOG2) - dyToRotPoint);
//
//                float dxRotated = cos * dxToRotate - sin * dyToRotate;
//                float dyRotated = sin * dxToRotate + cos * dyToRotate;
//
//                int xNewTileOrigin = Math.round(dxRotated + xp + dxToRotPoint);
//                int yNewTileOrigin = Math.round(dyRotated + yp + dyToRotPoint);
//
//                screen.renderWithAngle2DTileIndices(xNewTileOrigin, yNewTileOrigin, sheetIndex, xTileBase+i, yTileBase+j, color, angle);
//
//            }
//
//        }
//
//    }


    public void render(Screen screen, int xp, int yp, int angle) {

        float cos = screen.cos[angle];
        float sin = screen.sin[angle];

        // translate xp,yp to tile center
        xp -= ((xTileSize << Gfx.TILE_SIZE_LOG2) >> 1);
        yp -= ((yTileSize << Gfx.TILE_SIZE_LOG2) >> 1);

        // for all tiles in the given set (base to base+size)
        for (int i = 0; i < xTileSize; i++) {
            for (int j = 0; j < yTileSize; j++) {

                int dxToRotate = ((i << Gfx.TILE_SIZE_LOG2) - dxToRotPoint) * renderScale;
                int dyToRotate = ((j << Gfx.TILE_SIZE_LOG2) - dyToRotPoint) * renderScale;

                float dxRotated = cos * dxToRotate - sin * dyToRotate;
                float dyRotated = sin * dxToRotate + cos * dyToRotate;

                int xNewTileOrigin = Math.round(dxRotated + xp + dxToRotPoint);
                int yNewTileOrigin = Math.round(dyRotated + yp + dyToRotPoint);

                screen.renderTileAtPosRotPixels(xNewTileOrigin, yNewTileOrigin, sheetIndex, xTileBase+i, yTileBase+j, color, renderScale, angle);

            }

        }

    }




}


// <editor-fold defaultstate="collapsed" desc="old">
//    public void render(Screen screen, int xp, int yp) {
//        for (int i = 0; i < xTileSize; i++) {
//            for (int j = 0; j < yTileSize; j++) {
//
//                int xpi = xp + (i << Gfx.TILE_SIZE_LOG2);
//                int ypj = yp + (j << Gfx.TILE_SIZE_LOG2);
//                int tileIndex = xTileBase + i + ((yTileBase + j) << Gfx.NUM_TILES_IN_SHEET_ROWS_LOG2);
//                screen.render(xpi, ypj, sheetIndex, tileIndex, color);
//
//            }
//        }
//    }
//    public void render(Screen screen, int xp, int yp, int angle) {
//        for (int i = 0; i < xTileSize; i++) {
//            for (int j = 0; j < yTileSize; j++) {
//
//                int xpi = xp + (i << Gfx.TILE_SIZE_LOG2);
//                int ypj = yp + (j << Gfx.TILE_SIZE_LOG2);
//                int tileIndex = xTileBase + i + ((yTileBase + j) << Gfx.NUM_TILES_IN_SHEET_ROWS_LOG2);
//
//
////                screen.render(xpi, ypj, sheetIndex, tileIndex, color);
//                screen.renderWithAngle(xpi, ypj, sheetIndex, tileIndex, color, angle);
//
//            }
//        }
//    }
// </editor-fold>

