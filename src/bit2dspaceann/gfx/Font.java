/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.gfx;

/**
 *
 * @author Bo
 */
public class Font {

    private static byte sheetIndex = 1;
    public static long DEF_COLOR = ColorsLong.get(-1, -1, -1, -1, -1, -1, -1, 555);

//    private static final int SHEET_TILE_WIDTH = 8;
//    private static final int SHEET_TILE_WIDTH_LOG2 = 3;

    private static String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ      "
                                + "1234567890.,:;'\"!?$%()-=+/      ";

////    private static final int bubbleBaseOffsetY = -10;
////    private static final int bubbleColor = Colors.get(000, -1, -1, 440);
//    private static final int bubbleSpriteSheetTileIndex = 0 + 17 * 32;

    public static void render(String msg, Screen screen, int x, int y, long color, int scale) {
        msg = msg.toUpperCase();
        for (int i = 0; i < msg.length(); i++) {
            int charIndex = chars.indexOf(msg.charAt(i));
//            if(charIndex>=0) screen.render(x+i*sizeX, y, charIndex+30 * 32, color);
            int sx = x + i * Gfx.TILE_SIZE * scale;
//            if(charIndex>=0) screen.render(sx, y, sheetIndex, charIndex + 30 * 32, color, scale, Gfx.BIT_MIRROR_NONE);
//            if(charIndex>=0) screen.render(sx, y, sheetIndex, charIndex, color, scale, Gfx.BIT_MIRROR_NONE);
            if(charIndex>=0) screen.render(sx, y, sheetIndex, charIndex, color, scale, Gfx.BIT_MIRROR_NONE);
        }
    }
    
    
    public static int sizeOf(String msg, int scale) {
        return msg.length() * Gfx.TILE_SIZE * scale;
    }


//    public static void renderBubble(String msg, Screen screen, int xCenter, int yCenter, long color) {
//        msg = msg.toUpperCase();
//        int xOff = ((msg.length()-2) << (SHEET_TILE_WIDTH_LOG2-1));
////        int yOff = bubbleBaseOffsetY - SHEET_TILE_WIDTH;
//        int yOff = - 2 * SHEET_TILE_WIDTH;
//        screen.render(xCenter, yCenter - SHEET_TILE_WIDTH, sheetIndex, bubbleSpriteSheetTileIndex, color, 1, Gfx.BIT_MIRROR_X);
//        render(msg, screen, xCenter - xOff, yCenter + yOff, color, 1);
//    }

    public static void setSheetIndex(byte sheetIndex) {
        Font.sheetIndex = sheetIndex;
    }


}
