/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.gfx.graphs;

import bit2dspaceann.gfx.Font;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.gfx.renderers.GeoRenderer;
import java.awt.Color;
import java.awt.Point;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 *
 * @author Bo
 */
public class Chart2f {
    
    public String title = "Awesome Chart";
    public String labelX = "X-Axis";
    private ArrayList<DataPoint2f> points = new ArrayList<DataPoint2f>();
    
    private int x0 = 200;
    private int y0 = 400;
    
    private int w = 400;
    private int h = 200;
    
    float min = Float.MAX_VALUE, max = -Float.MAX_VALUE;
    
    private int axisColor = Color.white.getRGB();
    
//    private int lineColor = Color.green.getRGB();
//    private int avrgColor = Color.pink.getRGB();
    
//    private int lineColorDark = Color.green.darker().darker().darker().getRGB();
//    private int lineColorBright = Color.green.getRGB();
    
    private int lineColorDark = Color.cyan.darker().darker().darker().getRGB();
    private int lineColorBright = Color.cyan.getRGB();
    
    private DecimalFormat dec2 = new DecimalFormat("0.##");
    
    public boolean renderAverage = !true;
    private float windowSum = 0;
    public int runAvrgLength = 15 ;
    

    public Chart2f() {
    }

    public Chart2f(int x, int y) {
        x0 = x;
        y0 = y;
    }
          
    public Chart2f(String title, int x, int y) {
        this(x,y);
        this.title = title;
    }
          
    
    
    public void render(Screen screen) {
        
        
        screen.mixRect(new Color(.12f,.10f,.15f).getRGB(), x0, y0-h, w, h, 0.95f);
        
        
        renderAxis(screen);
        
//        System.out.println("RENDER POINTS!");
        if(!points.isEmpty())
            renderPoints(screen);
        
//        System.out.println("RENDER LINE SEGMENTS!");
        if(points.size()>1)
            renderLineSegments(screen);
    }
    
    
    
    private void renderPoints(Screen screen) {
        for (DataPoint2f point : points) {
            renderPoint(screen, toPixel(point));
        }
    }
    
    
    private void renderLineSegments(Screen screen) {
        windowSum = points.get(0).y;
        float fromAvrg = windowSum;
        
        for (int i = 0; i < points.size()-1; i++) {
            
            int fromIdx = i;
            int toIdx = i+1;
            
            Point pFrom = toPixel(points.get(i));
            Point pTo = toPixel(points.get(i+1));
            int lineSegmentColor = renderAverage ? lineColorDark : lineColorBright;
            
            if(pFrom.equals(pTo)) {
                screen.renderPixelWithRGB(pFrom.x, pFrom.y, lineSegmentColor);
            }
            else {
                GeoRenderer.drawLine(screen, lineSegmentColor, pFrom, pTo);
            }
            
            if(renderAverage) {
                windowSum += points.get(toIdx).y;

                float toAvrg;
                if(toIdx < runAvrgLength) {
                    toAvrg = windowSum / (toIdx+1);
                }
                else {
                    windowSum -= points.get(toIdx-runAvrgLength).y;
                    toAvrg = windowSum / runAvrgLength;
                }

                pFrom = convertToPixel(fromIdx, fromAvrg);
                pTo = convertToPixel(toIdx, toAvrg);
                GeoRenderer.drawLine(screen, lineColorBright, pFrom, pTo);

                fromAvrg = toAvrg;
            }
        }
    }
    
    private void renderAxis(Screen screen) {
        GeoRenderer.drawLine(screen, axisColor, x0,y0, x0+w, y0);
        GeoRenderer.drawLine(screen, axisColor, x0,y0, x0, y0-h);
        
        Font.render(title, screen, x0 + (w>>1) - (Font.sizeOf(title, 1)>>1) , y0-h+2, Font.DEF_COLOR, 1);
        
        if(points.isEmpty()) return;
        
        Font.render(dec2.format(max), screen, x0+4, y0-h+2, Font.DEF_COLOR, 1);
        
        int xSizeAxisLabel = Font.sizeOf(Integer.toString(points.size()), 1);
        Font.render(Integer.toString(points.size()), screen, x0+w-xSizeAxisLabel-4, y0-12, Font.DEF_COLOR, 1);
        
        // x-axis label
//        Font.render(labelX, screen, x0+w-xSizeAxisLabel-Font.sizeOf(labelX, 1)-32, y0-12, Font.DEF_COLOR, 1);
        Font.render(labelX, screen, x0 + (w>>1) - (Font.sizeOf(labelX, 1)>>1), y0-12, Font.DEF_COLOR, 1);
        
    }
    
    private Point convertToPixel(float x, float y) {
        if(points.size()<1) {
            return new Point(x0, y0);
        }
        float xStepSize = (float)w / (float)(points.size()-1);
        float yScale = h / (max-min);
        int px = x0 + Math.round(xStepSize * x);
        int py = y0 - Math.round(yScale * (y - min));
        return new Point(px,py);
    }
    
    private Point toPixel(DataPoint2f p) {
        return convertToPixel(p.x, p.y);
    }
    
    
    public void renderPoint(Screen screen, Point p) {
        renderPoint(screen, p.x, p.y);
    }
    public void renderPoint(Screen screen, int x, int y) {
        int px = (int)Math.round(x);
        int py = (int)Math.round(y);
        int color = renderAverage ? lineColorDark : lineColorBright;
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
//                System.out.println("RENDER POINT PIXEL!");
                screen.renderPixelWithRGB(px+i, py+j, color);
            }
        }
    }
    
    public void addDataPoint(float value) {
        DataPoint2f point = new DataPoint2f(points.size(), value);
        points.add(point);
        if(point.y > max) max = point.y;
        if(point.y < min) min = point.y;
    }
    
    public void setData(float[] value) {
        points.clear();
        min = Float.MAX_VALUE;
        max = -Float.MAX_VALUE;
        for (int i = 0; i < value.length; i++) {
            addDataPoint(value[i]);
        }
    }
    
}


//            if(renderAverage && i+1 >= nAverage) {
//                float avrg = 0;
//                for (int j = 0; j < nAverage; j++) {
//                    avrg += points.get(i+1-j).y;
//                }
//                avrg /= nAverage;
//                Point2f newAverage = new Point2f(i+1, avrg);
//                if(lastAverage!=null) {
//                    pFrom = toPixel(lastAverage);
//                    pTo = toPixel(newAverage);
//                    GeoRenderer.renderLine(screen, avrgColor, pFrom, pTo);
//                }
//                lastAverage = newAverage;
//            }
            
//            float lastRunAvrg = runAvrg;
//            runAvrg = (runAvrg * i + points.get(i+1).y) / (i+1);
//            Point2f aFrom = new Point2f(i, lastRunAvrg);
//            Point2f aTo = new Point2f(i+1, runAvrg);
//            GeoRenderer.renderLine(screen, avrgColor, toPixel(aFrom), toPixel(aTo));
//
//            
//            if(i >= runAvrgLength) {
//                runAvrg -= points.get(i-runAvrgLength).y / runAvrgLength;
//                runAvrg += points.get(i+1).y / runAvrgLength;
//            }
