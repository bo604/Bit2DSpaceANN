/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.gfx.graphs;

import java.awt.Color;

/**
 *
 * @author Bo
 */
public class DataPoint2f implements Comparable<DataPoint2f> {
    
    public float x=0,y=0;
    public int color = Color.green.getRGB();

    public DataPoint2f() {
    }

    public DataPoint2f(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public int compareTo(DataPoint2f other) {
        if(this.x == other.x) return 0;
        if(this.x < other.x) return -1;
        return 1;
    }
    
    
}
