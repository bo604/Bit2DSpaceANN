/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.gfx;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

/**
 *
 * @author Bo
 */
public class GameCanvas extends Canvas {

    private int viewScale = Gfx.IMAGE_VIEW_SCALE;
    private int pWidth = Gfx.IMAGE_PIXEL_WIDTH;
    private int pHeight = Gfx.IMAGE_PIXEL_HEIGHT;

    /**
     * Central rendering image for the game. Everything goes in there
     * via commit(screen), and will show up in the GameFrame...
     */
    private BufferedImage image = null;
    private int[] pixels = null;

    public boolean init() {
        /*
         * Init with SCALED SIZE
         *  ( only matters for the size of the frame on the monitor,
         *    and has no influence on pixel- or tile-coordinates )
         */
        int scaledWidth = pWidth * viewScale;
        int scaledHeight = pHeight * viewScale;
        this.setMinimumSize(new Dimension(scaledWidth, scaledHeight));
        this.setMaximumSize(new Dimension(scaledWidth, scaledHeight));
        this.setPreferredSize(new Dimension(scaledWidth, scaledHeight));

        // Create the buffer image with the actual size
        this.image = new BufferedImage(pWidth, pHeight, BufferedImage.TYPE_INT_RGB);
        // Get reference to the pixels of the image
        this.pixels = ((DataBufferInt)image.getRaster().getDataBuffer()).getData();

        // Init successful
        return true;
    }



    /**
     * Convenience method for combined commit and render. Use only if there is only
     * one screen used. Otherwise commit each screen first, then this.render() once!
     * @param screen
     * @param offset
     */
    public void commitAndRender(Screen screen, int offset) {
        commit(screen, offset);
        render();
    }

    /**
     * Commit screen to canvas
     * @param screen
     * @param offset if the screen needs to draw from somewhere other than pixels[0]
     */
    public void commit(Screen screen, int offset) {
        for (int y = 0; y < screen.pHeight; y++) {
            for (int x = 0; x < screen.pWidth; x++) {
                pixels[offset + x + y * screen.pWidth] = screen.pixels[x + y * screen.pWidth];
            }
        }
    }

    private boolean doClearScreenToBlack = true;

    /**
     * Draw buffered image to canvas
     */
    public void render() {
        // Get/make BufferStrategy
        BufferStrategy bs = this.getBufferStrategy();
        if(bs==null) {
            this.createBufferStrategy(3);
            return;
        }
        // Draw image to canvas
        Graphics g = bs.getDrawGraphics();
        if(doClearScreenToBlack) {
            g.setColor(Color.BLACK);
            g.fillRect(0, 0, pWidth, pHeight);
        }
        g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
        g.dispose();
        bs.show();
    }

}


// <editor-fold defaultstate="collapsed" desc="old 2">
/*
 * commit stuff to buffer image here!
 * or before? i.e. call canvas render last?
 *
 * before feels better somehow? dont what to control
 * screens or switch any states here... just render
 * dat imadge!
 */
//        switch(gameState) {
//            case Running:
//                renderLevelViewScreen();
//                break;
//            case GameOver:
//                renderGGScreen();
//                break;
//            case Menu:
//                renderMenuScreen();
//                break;
//        }
//        renderHUD();
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="old">


//        if(isGameOver) {
//            ggScreen.render();
//            // Draw gg screen to image pixels
//            for (int y = 0; y < ggScreen.height; y++) {
//                for (int x = 0; x < ggScreen.width; x++) {
//                    int colorCode = ggScreen.pixels[x + y * ggScreen.width];
//                    if(colorCode < 255) pixels[x + y * width] = colors[colorCode];
//                }
//            }
//        }
//        else {
//            // View screen rendering.
//            // "Camera position" to player position
//            int xOffset = player.xWorld - (screen.width/2);
//            int yOffset = player.yWorld - (screen.height/2);
//            // Render stuff into view screen
//            level.renderTiles(screen, xOffset, yOffset);
//            level.renderEntities(screen);
//            splashScreen.render(screen);
//            // Draw view screen to image pixels
//            for (int y = 0; y < screen.height; y++) {
//                for (int x = 0; x < screen.width; x++) {
//                    int colorCode = screen.pixels[x + y * screen.width];
//                    if(colorCode < 255) pixels[x + y * width] = colors[colorCode];
//                }
//            }
//        }


//    private void fontTest() {
//        // Render some font
//        String msg = "RUN!";
//        int textX = (screen.width - msg.length() * Screen.TILE_WIDTH) / 2;
//        int textY = (screen.height - Screen.TILE_WIDTH) / 2;
////        int textCol = Colors.get(000, -1, -1, 555);   // White on Black
//        int textCol = Colors.get(-1, -1, -1, 000);      // Black on Trans
//        int testScale = 1;
//        Font.render(msg, screen, textX, textY, textCol, testScale);
//    }


//        // Number of tiles in x and y of the sprite sheet (=width/tile_width)
//        int nTiles = 32;
//        // Log base 2 of tile width (for bit-shifting div/mults)
//        int tileWidthLog2 = Screen.TILE_WIDTH_LOG2;
//        // Render screen internally
//        for (int y = 0; y < nTiles; y++) {
//            for (int x = 0; x < nTiles; x++) {
//                /* Define the 4 colors used for the render */
//                int col = Colors.get(555, 500, 050, 005);
//                boolean mirrorX = x%2 == 1;
//                boolean mirrorY = y%2 == 1;
//                /* Render screen pixel internally */
//                screen.render( x<<tileWidthLog2, y<<tileWidthLog2, 0, col, mirrorX, mirrorY);
//            }
//        }
//        /* strange moving bars */
//        if(!true) {
//            for (int i = 0; i < pixels.length; i++) {
//                pixels[i] = i + tickCount;
//            }
//        }
//        /* let it run */
//        if(!true) {
//            screen.xOffset++;
//            screen.yOffset++;
//        }


// </editor-fold>

