/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.evo;

import bit2dspaceann.util.MiscMath;

/**
 *
 * @author Bo
 */
public class ANNLayer {
    
    public final int nIn;
    public final int nOut;
    
    private float[] input;
    private float[] output;
    
    private float[][] weights;

    public ANNLayer(int nIn, int nOut) {
        this.nIn = nIn;
        this.nOut = nOut;
        weights = new float[nIn][nOut];
        output = new float[nOut];
    }
    
    public void setInput(float[] input) {
        this.input = input;
    }
    
    public float[] getOutput() {
        return output;
    }
    
    public void calculate() {
        for (int j = 0; j < nOut; j++) {
            float in = 0;
            for (int i = 0; i < nIn; i++) {
                in += weights[i][j] * input[i];
            }
            output[j] = MiscMath.sigmoid(in);
        }
    }
    
//    private float sigmoid(float x) {
//        return (float)(1.0/( 1.0+Math.exp(-x)));
//    }
    
    public void randomize() {
        for (int i = 0; i < nIn; i++) {
            for (int j = 0; j < nOut; j++) {
                weights[i][j] = (float)(2*Math.random()-1);
            }
        }
    }
    
    public float[] getGeneSequence() {
        float[] gene = new float[nIn*nOut];
        int idx = 0;
        for (int i = 0; i < nIn; i++) {
            for (int j = 0; j < nOut; j++) {
                gene[idx] = weights[i][j];
                idx++;
            }
        }
        return gene;
    }
    
    public void setGene(float[] gene) {
        int idx = 0;
        for (int i = 0; i < nIn; i++) {
            for (int j = 0; j < nOut; j++) {
                weights[i][j] = gene[idx];
                idx++;
            }
        }
    }
    
}
