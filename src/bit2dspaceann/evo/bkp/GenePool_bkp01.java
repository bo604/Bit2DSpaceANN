/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.evo.bkp;

import bit2dspaceann.evo.Gene;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author Bo
 */
public class GenePool_bkp01 extends ArrayList<Gene> {

    public GenePool_bkp01() {
        super();
    }
    
    public GenePool_bkp01(Collection<? extends Gene> clctn) {
        super(clctn);
    }
    
    private static final int geneRenderScale = 2;
    
    public void render(Screen screen, int x, int y) {
        if(!Gfx.RENDER_GENEPOOL) return;
        int idx = 0;
        for (Gene gene : this) {
            gene.render(screen, x, y+idx*(1+geneRenderScale), geneRenderScale);
            idx++;
        }
    }
    
    
}
