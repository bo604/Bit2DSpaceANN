/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.evo.bkp;

import bit2dspaceann.gfx.ColorsLong;
import bit2dspaceann.gfx.Font;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;
import java.awt.Color;

/**
 *
 * @author Bo
 */
public class Gene_bkp01 implements Comparable<Gene_bkp01> {
    
    public float[] gene;
    public float fitness;
    public int age;

    public Gene_bkp01(float[] gene, float fitness, int age) {
        this.gene = gene;
        this.fitness = fitness;
        this.age = age;
    }

    public int compareTo(Gene_bkp01 other) {
        if(this.fitness < other.fitness) {
            return 1;
        }
        else if(this.fitness > other.fitness) {
            return -1;
        }
        else return 0;
    }
    
    private long fontColor = ColorsLong.get(-1, -1, -1, -1, -1, -1, -1, 555);
    
    private int[][] colors = null;
    
    private void initColors() {
        nx = (int)Math.ceil(Math.sqrt(gene.length));
        ny = nx + 1;
        colors = new int[nx][ny];
        for (int j = 0; j < ny; j++) {
            for (int i = 0; i < nx; i++) {
                int idx = i + j * ny;
                if(idx < gene.length) {
                    colors[i][j] = calcColor(gene[idx]);
                }
                else {
//                    colors[i][j] = Color.blue.getRGB();
                    colors[i][j] = Integer.MAX_VALUE;
                }
            }
        }
    }
    
    int nx = 0;
    int ny = 0;
    int scale = 5;
    
    private int calcColor(float weight) {
        if(weight<0) {
            weight = -weight;
            float r = 1;
            float g = 1-weight;
            return new Color(r, g, 0).getRGB();
        }
        else {
            float r = 1-weight;
            float g = 1;
            return new Color(r, g, 0).getRGB();
        }
    }
    
    public void render(Screen screen) {
        Font.render("Top Gene (Age " + age + ")", screen, 0, 24, fontColor, 1);
        if(colors==null) initColors();
        for (int j = 0; j < ny; j++) {
            for (int i = 0; i < nx; i++) {
                if(colors[i][j]!=Integer.MAX_VALUE) {
                    int x = i * scale;
                    int y = j * scale + 36;
                    for (int xx = 0; xx < scale; xx++) {
                        for (int yy = 0; yy < scale; yy++) {
                            screen.renderPixelWithRGB(x+xx, y+yy, colors[i][j]);
                        }
                    }
                }
            }
        }
    }
    
    public void renderUpwards(Screen screen) {
        Font.render("Top Gene (Age " + age + ")", screen, 4, Gfx.IMAGE_PIXEL_HEIGHT-10, fontColor, 1);
        if(colors==null) initColors();
        for (int j = 0; j < ny; j++) {
            for (int i = 0; i < nx; i++) {
                if(colors[i][j]!=Integer.MAX_VALUE) {
                    int x = 4 + i * scale;
                    int y = Gfx.IMAGE_PIXEL_HEIGHT - j * scale - 12-scale;
                    for (int xx = 0; xx < scale; xx++) {
                        for (int yy = 0; yy < scale; yy++) {
                            screen.renderPixelWithRGB(x+xx, y+yy, colors[i][j]);
                        }
                    }
                }
            }
        }
    }
    
}
