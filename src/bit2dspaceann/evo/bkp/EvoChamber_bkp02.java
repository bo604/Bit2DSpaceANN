package bit2dspaceann.evo.bkp;

///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package bit2dspaceann.evo;
//
//import bit2dspaceann.entities.Entity;
//import bit2dspaceann.entities.Fish;
//import bit2dspaceann.entities.Plant;
//import bit2dspaceann.entities.controls.ANNControl;
//import bit2dspaceann.entities.controls.Control;
//import bit2dspaceann.gfx.Font;
//import bit2dspaceann.gfx.Screen;
//import bit2dspaceann.level.Level;
//import java.text.DecimalFormat;
//import java.util.HashMap;
//import java.util.Random;
//
///**
// *
// * @author Bo
// */
//public class EvoChamber_bkp02 {
//    
//    private static final Random rnd = new Random();
//
////    private static final EasyChart fitDistChart = new EasyChart();
////    private static final EasyChart fitHistChart = new EasyChart();
//    
//    
////    private float variationRateStart = .02f; //0.15f;
////    private float variationRateMin = 0.0001f; //0.04f;
////    private float variationRate = variationRateStart;
////    private float variationRateDecay = 0.01f; //0.99f;
////    private float variationDistance = 0.1f;
////    
////    private float mutationRateStart = .02f; //0.15f;
////    private float mutationRateMin = 0.0001f; //0.02f;
////    private float mutationRate = mutationRateStart;
////    private float mutationRateDecay = 0.02f; //0.98f;
////
////    private float superMutantRateStart = 0.5f;
////    private float superMutantRate = superMutantRateStart;
////    private float superMutantRateDecay = 0.02f;
//    
//    public static final int genLength = 6000;
//    
//    private DecimalFormat dec2 = new DecimalFormat("0.##");
//    private StringBuilder info;
//    
//    private Level level;
//    
//    int generation = 1;
//    Gene bestGene = null;
////    GenePool lastGenePool = null;
//    
//    HashMap<String, GenePool> genePoolMap = new HashMap<String, GenePool>();
//    
//    String[] evolvers = new String[] {"Fish", "Plant"};
//    String geneRenderSelect = "Fish";
//
//    public EvoChamber_bkp02(Level level) {
//        this.level = level;
//        for (String key : evolvers) {
//            genePoolMap.put(key, new GenePool());
//        }
//    }
//    
//    
//    private static final int yInfoOffset = 12;
//    
//    public void render(Screen screen) {
//        // render best gene
//        if(bestGene!=null) {
////            bestGene.renderUpwards(screen);
//            bestGene.render2D(screen, 4, 20, Gene.UP);
//        }
//
//        // render last gene pool
//        GenePool renderPool = genePoolMap.get(geneRenderSelect);
//        if(renderPool!=null && !renderPool.isEmpty()) {
//            renderPool.render(screen, 200, 1);
//        }
//        
//        // render infos
//        if(info!=null) {
//            String[] lines = info.toString().split("\n");
//            int iOffset = 0;
//            for (int i = 0; i < lines.length; i++) {
//                String line = lines[i];
//                Font.render(line, screen, 0, yInfoOffset+iOffset, Font.DEF_COLOR, 1);
//                iOffset += line.isEmpty() ? 6 : 12;
//            }
//        }
//        
////        // charts
////        if(Gfx.RENDER_CHARTS) {
////            fitnessHistoryChart.render(screen);
////            fitnessDistributionChart.render(screen);
////        }
//    }
//    
//    
//    
//    
//    public void makeNewGeneration() {
//        // build info
//        info = new StringBuilder();
//        info.append("Last Generation: ").append(generation).append("\n");
////        info.append("\n");
////        info.append("Mutation Rate: ").append(dec2.format(100f*mutationRate)).append("%\n");
////        info.append("Variation Rate: ").append(dec2.format(100f*variationRate)).append("%\n");
////        info.append("Supermutant Rate: ").append(dec2.format(100f*superMutantRate)).append("%\n");
//        info.append("\n");
//        
//        // evolve stuff
////        GenePool genes = collectFishGenes();
////        bestGene = genes.get(0);
////        lastGenePool = new GenePool(genes);
////        printStats(genes);
////        evolveAllFish(genes);
//        evolveStuff();
//        
//        // print info
//        System.out.println("========================");
//        System.out.println(info.toString());
////        System.out.println("=========================");
//        System.out.println("============");
//        
//        // update stuff
//        generation++;
////        updateRates();
//        level.resetFood(Level.nFood);
//        level.resetControlledMobPositions();
//        level.clearTrajectories();
//    }
//    
//    
//    private void evolveStuff() {
//        GenePool genes = genePoolMap.get("Fish");
//        
////        collectFishGenes(genes);
////        bestGene = genes.get(0);
//        
//        genes.collectGenes(level.entities, "Fish");
//        bestGene = genes.get(0);
//        
////        printStats(genes);
//        genes.printStats(info);
//        
//        evolveAllFish(genes);
//        
//        genes.updateGenerationRates();
//    }
//    
//    
////    private void updateRates() {
//////        mutationRate = Math.max(mutationRate*mutationRateDecay,0.02f);
//////        variationRate = Math.max(variationRate*variationRateDecay,0.04f);
////        mutationRate = expDecay(generation, mutationRateDecay, mutationRateMin, mutationRateStart);
////        variationRate = expDecay(generation, variationRateDecay, variationRateMin, variationRateStart);
////        superMutantRate = expDecay(generation, superMutantRateDecay, .01f, superMutantRateStart);
////    }
//    
//    
//    
//    
//    /**
//     * 
//     * @param genePool 
//     */
//    private void evolveAllFish(GenePool genePool) {
//        
//        // calc stats
//        float totFit = 0;
//        float topFit = 0;
//        for (int i = 0; i < genePool.size(); i++) {
//            float fit = genePool.get(i).fitness;
//            totFit += fit;
//            if(fit > topFit) {
//                topFit = fit;
//            }
//        }
//        float avrgFit = totFit / genePool.size();
//        
//        // thresholds
//        float starveThreshold = 0.05f * avrgFit;
//        float surviveThreshold = 0.85f * topFit;
//        
////        // starve
////        int nStarved = 0;
////        for (Iterator<ANNGene> it = genes.iterator(); it.hasNext();) {
////            ANNGene gene = it.next();
////            if(gene.fitness < starveThreshold) {
////                it.remove();
////                nStarved++;
////            }
////        }
////        renderInfo += ("Starved (Gene lost): " + nStarved + "\n");
//        
//        int nSurvive = 0;
//        int nFishRemaining = 0;
//        for (Entity entity : level.entities) {
//            if(entity.getEntityTypeName().equals("Fish")) {
//                nFishRemaining++;
//                
//                Fish fish = (Fish)entity;
//                Control c = fish.getControl(ANNControl.class);
//                if(c==null) continue;
//                ANNControl annc = (ANNControl)c;
//                
//                if(fish.fitness() > surviveThreshold) {
//                    nSurvive++;
//                    fish.age++;
//                }
//                else {
////                    Gene g1 = genePool.selectGeneWeighted();
////                    Gene g2 = genePool.selectGeneWeighted();
////                    float[] newGene = genePool.combineGenes(g1, g2);
//                    float[] newGene = genePool.createOffspring();
//                    
//                    annc.setGene(newGene);
//                    fish.age = 1;
//                }
//                
////                annc.resetMob();
//                fish.reset();
//            }
//            // dirty workaround for plant food collected:
//            else if(entity.getEntityTypeName().equals("Plant")) {
//                Plant plant = (Plant)entity;
//                plant.foodCollected = 0;
//                plant.ticksNoSnack = 0;
//            }
//        }
////        renderInfo += ("Survived (Gene aged): " + nSurvive + "\n");
//        info.append("Survived (Aged): ").append(nSurvive).append("\n");
//        
//        // refill fish population
//        for (int i = 0; i < Level.nFish - nFishRemaining; i++) {
//            if(rnd.nextFloat()<genePool.superMutantRate) {
//                new Fish(level);
//            }
//            else {
////                Gene g1 = genePool.selectGeneWeighted();
////                Gene g2 = genePool.selectGeneWeighted();
////                new Fish(level, genePool.combineGenes(g1, g2));
//                new Fish(level, genePool.createOffspring());
//            }
//        }
////        renderInfo += ("Fish Repopulated: " + (Level.nFish-nFishRemaining));
//        info.append("Repopulated: ").append(Level.nFish-nFishRemaining);
//        
//    }
//    
//    
//}
//    
//
//
//
//    //    private float[] combineGenesWithFitness(ANNGene g1, ANNGene g2) {
////        int nGenes = g1.gene.length;
////        float[] gene = new float[nGenes];
////        
////        // selection probability based on fitness:
////        float fitTot = g1.fitness + g2.fitness;
////        float p1 = g1.fitness / fitTot;
////        float p2 = g2.fitness / fitTot;
////        
//////        System.out.println("g1f=" + g1.fitness + "  p1="+p1);
//////        System.out.println("g2f=" + g2.fitness + "  p2="+p2);
////        
////        // include mutation rate:
////        float pNoMutation = 1 - mutationRate;
////        p1 *= pNoMutation;
////        p2 *= pNoMutation;
////        
//////        System.out.println("p1="+p1);
//////        System.out.println("p2="+p2);
////        
////        for (int i = 0; i < nGenes; i++) {
////            float r = rnd.nextFloat();
////            if(r<p1) {
////                gene[i] = g1.gene[i];
////            }
////            else if(r<p1+p2) {
////                gene[i] = g2.gene[i];
////            }
////            else {
////                gene[i] = 2 * rnd.nextFloat() - 1;
////            }
////            if(rnd.nextFloat()<variationRate) {
////                gene[i] += variationDistance * (2*rnd.nextFloat()-1);
////                if(gene[i]>1) gene[i]=1;
////                if(gene[i]<-1) gene[i]=-1;
////            }
////        }
////        return gene;
////    }
//
//    
////    private float[] combineGenesPrimitive(float[] g1, float[] g2) {
////        int nGenes = g1.length;
////        float[] gene = new float[nGenes];
////        for (int i = 0; i < nGenes; i++) {
////            float r = rnd.nextFloat();
////            if(r<.45f) {
////                gene[i] = g1[i];
////            }
////            else if(r<.85f) {
////                gene[i] = g2[i];
////            }
////            else {
////                gene[i] = 2 * rnd.nextFloat() - 1;
////            }
////        }
////        return gene;
////    }
