/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.evo;

import bit2dspaceann.gfx.ColorsLong;
import bit2dspaceann.gfx.Font;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.gfx.renderers.GeoRenderer;
import java.awt.Color;

/**
 *
 * @author Bo
 */
public class Gene implements Comparable<Gene> {
    
    public float[] gene;
    public float fitness;
    public int age;
    
    public static final int renderScale = 5;
    private static final long fontColor = ColorsLong.get(-1, -1, -1, -1, -1, -1, -1, 555);
    private int[] color = null;
    
    public static final int UP = 0;
    public static final int DOWN = 1;
    public static final int LEFT = 2;
    public static final int RIGHT = 3;
            
//    int x0 = 4;
//    int y0 = 20;
    
    int nx = -1;

    public Gene(float[] gene, float fitness, int age) {
        this.gene = gene;
        this.fitness = fitness;
        this.age = age;
    }

    public int compareTo(Gene other) {
        if(this.fitness < other.fitness) {
            return 1;
        }
        else if(this.fitness > other.fitness) {
            return -1;
        }
        else return 0;
    }
    
    private void initColors() {
        color = new int[gene.length];
        for (int i = 0; i < gene.length; i++) {
            color[i] = calcColor(gene[i]);
        }
        nx = (int)Math.sqrt(color.length)+1;
    }

    
    private int calcColor(float weight) {
        if(weight<0) {
            weight = -weight;
            float r = 1;
            float g = 1-weight;
            return new Color(r, g, 0).getRGB();
        }
        else {
            float r = 1-weight;
            float g = 1;
            return new Color(r, g, 0).getRGB();
        }
    }
    
//    private boolean renderUp = true;
    
//    public void render2D(Screen screen) {
//        Font.render("Top Gene (Age " + age + ")", screen, 4, Gfx.IMAGE_PIXEL_HEIGHT-10, fontColor, 1);
//        if(color==null) initColors();
//        int nx = (int)Math.sqrt(color.length)+1;
//        for (int i = 0; i < color.length; i++) {
//            int x = x0 + (i%nx)*renderScale;
////            int y = y0 + (i/nx)*renderScale;
//            int y = renderUp ?
//                    screen.pHeight - (y0 + (i/nx)*renderScale)
//                  : y0 + (i/nx)*renderScale;
//            
//            GeoRenderer.renderRect(screen, color[i], x, y, renderScale, renderScale);
//        }
//    }
    
    
    public void render(Screen screen, int x0, int y0, int renderScale) {
        if(color==null) initColors();
        for (int i = 0; i < color.length; i++) {
            int x = x0 + i * renderScale;
            int y = y0;
            GeoRenderer.fillRect(screen, color[i], x, y, renderScale, renderScale);
        }
    }
    
    
    public void render2D(Screen screen, int x0, int y0, int renderDir) {
        Font.render("Top Gene (Age " + age + ")", screen, 4, Gfx.IMAGE_PIXEL_HEIGHT-10, fontColor, 1);
        if(color==null) initColors();
        if(nx<1) System.out.println("GENE.RENDER NX FUCKUP!");
        for (int i = 0; i < color.length; i++) {
            int x = x0 + (i%nx)*renderScale;
            int y = renderDir == Gene.UP ?
                    screen.pHeight - (y0 + (i/nx)*renderScale)
                  : y0 + (i/nx)*renderScale;
            GeoRenderer.fillRect(screen, color[i], x, y, renderScale, renderScale);
        }
    }
    
}
    
//        nx = (int)Math.ceil(Math.sqrt(gene.length));
//        ny = nx + 1;
//        colors = new int[nx][ny];
//        for (int j = 0; j < ny; j++) {
//            for (int i = 0; i < nx; i++) {
//                int idx = i + j * ny;
//                if(idx < gene.length) {
//                    colors[i][j] = calcColor(gene[idx]);
//                }
//                else {
////                    colors[i][j] = Color.blue.getRGB();
//                    colors[i][j] = Integer.MAX_VALUE;
//                }
//            }
//        }
    
//    public void render(Screen screen) {
//        Font.render("Top Gene (Age " + age + ")", screen, 0, 24, fontColor, 1);
//        if(color==null) initColors();
//        for (int j = 0; j < ny; j++) {
//            for (int i = 0; i < nx; i++) {
//                if(color[i][j]!=Integer.MAX_VALUE) {
//                    int x = i * scale;
//                    int y = j * scale + 36;
//                    for (int xx = 0; xx < scale; xx++) {
//                        for (int yy = 0; yy < scale; yy++) {
//                            screen.renderPixelWithRGB(x+xx, y+yy, color[i][j]);
//                        }
//                    }
//                }
//            }
//        }
//    }
    
//    public void renderUpwards(Screen screen) {
//        Font.render("Top Gene (Age " + age + ")", screen, 4, Gfx.IMAGE_PIXEL_HEIGHT-10, fontColor, 1);
//        if(color==null) initColors();
//        for (int j = 0; j < ny; j++) {
//            for (int i = 0; i < nx; i++) {
//                if(color[i][j]!=Integer.MAX_VALUE) {
//                    int x = 4 + i * scale;
//                    int y = Gfx.IMAGE_PIXEL_HEIGHT - j * scale - 12-scale;
//                    for (int xx = 0; xx < scale; xx++) {
//                        for (int yy = 0; yy < scale; yy++) {
//                            screen.renderPixelWithRGB(x+xx, y+yy, color[i][j]);
//                        }
//                    }
//                }
//            }
//        }
//    }
