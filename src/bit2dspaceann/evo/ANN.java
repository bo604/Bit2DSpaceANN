/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.evo;

import java.util.Arrays;

/**
 *
 * @author Bo
 */
public class ANN {
    
    int[] nNodes;
    ANNLayer[] layers;
    
    float[] input;
    float[] output;

    public ANN(int... nNodes) {
        this.nNodes = nNodes;
        construct();
    }
    
    private void construct() {
        int nLayers = nNodes.length-1;
        layers = new ANNLayer[nLayers];
        for (int i = 0; i < nLayers; i++) {
            layers[i] = new ANNLayer(nNodes[i], nNodes[i+1]);
            layers[i].randomize();
        }
    }

    public void setInput(float[] input) {
        this.input = input;
    }
 
    public float[] getOutput() {
        return output;
    }
    
    public int getNumberInputs() {
        return nNodes[0];
    }
    
    public float[] calculate(float[] input) { 
        for (int i = 0; i < layers.length; i++) {
            layers[i].setInput(input);
            layers[i].calculate();
            input = layers[i].getOutput();
        }
        return input;
    }
    
    public void randomize() {
        for (ANNLayer layer : layers) {
            layer.randomize();
        }
    }
    
    public float[] getGene() {
        int nGenes = 0;
        for (ANNLayer layer : layers) {
            nGenes += layer.nIn * layer.nOut;
        }
        
        int idx = 0;
        float[] gene = new float[nGenes];
        for (ANNLayer layer : layers) {
            float[] layerGene = layer.getGeneSequence();
            for (float g : layerGene) {
                gene[idx] = g;
                idx++;
            }
        }
        
        return gene;
    }
    
    public void setGene(float[] gene) {
        int nGenes = 0;
        for (ANNLayer layer : layers) {
            nGenes += layer.nIn * layer.nOut;
        }
        if(gene.length!=nGenes) {
            throw new RuntimeException("Wrong number of genes while setting ANN gene!");
        }
        int idx = 0;
        for (ANNLayer layer : layers) {
            nGenes = layer.nIn*layer.nOut;
            float[] layerGene = Arrays.copyOfRange(gene, idx, idx+nGenes);
//            float[] layerGene = Arrays.copyOfRange(gene, idx, nGenes);
            layer.setGene(layerGene);
            idx += nGenes;
        }
    }
    
    
}
