/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.evo;

import bit2dspaceann.entities.Entity;
import bit2dspaceann.entities.Fish;
import bit2dspaceann.entities.Plant;
import bit2dspaceann.gfx.Font;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.level.Level;
import java.util.HashMap;
import java.util.Random;

/**
 *
 * @author Bo
 */
public class EvoChamber {
    
    private static final Random rnd = new Random();
    
//    public static final int genLength = 6000;
//    int generation = 1;
    
    private StringBuilder info;
    
    private Level level;
    
//    Gene bestGene = null;
//    GenePool lastGenePool = null;
    
    HashMap<String, GenePool> genePoolMap = new HashMap<String, GenePool>();
    
    String[] evolvers = new String[] {"Fish", "Plant"};
    String geneRenderSelect = "Fish";

    public EvoChamber(Level level) {
        this.level = level;
        for (String key : evolvers) {
            genePoolMap.put(key, new GenePool());
        }
    }
    
    
    private static final int yInfoOffset = 12;
    
    public void render(Screen screen) {
//        // render best gene
//        if(bestGene!=null) {
////            bestGene.renderUpwards(screen);
//            bestGene.render2D(screen, 4, 20, Gene.UP);
//        }

        // render last gene pool
        GenePool renderPool = genePoolMap.get(geneRenderSelect);
        if(renderPool!=null) {
            renderPool.render(screen, 200, 1);
        }
        
        // render infos
        if(info!=null) {
            String[] lines = info.toString().split("\n");
            int iOffset = 0;
            for (int i = 0; i < lines.length; i++) {
                String line = lines[i];
                Font.render(line, screen, 0, yInfoOffset+iOffset, Font.DEF_COLOR, 1);
                iOffset += line.isEmpty() ? 6 : 12;
            }
        }
        
    }
    
    
    
    
    public void makeNewGeneration() {
        // build info
        info = new StringBuilder();

        // evolve stuff
        evolveStuff();
        
        // print info
        System.out.println("========================");
        System.out.println(info.toString());
        System.out.println("============");
        
        // update stuff
        level.resetFood(Level.nFood);
        level.resetControlledMobPositions();
        level.clearTrajectories();
    }
    
    
    private void evolveStuff() {
        GenePool genes = genePoolMap.get("Fish");
        
        genes.appendInfo(info);
        genes.collectGenes(level.entities, "Fish");
        genes.appendStats(info);
        
        evolveAllFish(genes);
        
        genes.updateGenerationRates();
    }
    
    
    
    
    /**
     * 
     * @param genePool 
     */
    private void evolveAllFish(GenePool genePool) {
        
        // calc stats
        float totFit = genePool.totalFitness();
        float maxFit = genePool.maxFitness();
        float avrgFit = totFit / genePool.size();
        
        // thresholds
        float starveThreshold = 0.05f * avrgFit;
        float surviveThreshold = 0.85f * maxFit;
        
//        // starve
//        int nStarved = 0;
//        for (Iterator<ANNGene> it = genes.iterator(); it.hasNext();) {
//            ANNGene gene = it.next();
//            if(gene.fitness < starveThreshold) {
//                it.remove();
//                nStarved++;
//            }
//        }
//        renderInfo += ("Starved (Gene lost): " + nStarved + "\n");
        
        int nSurvive = 0;
        int nFishRemaining = 0;
        
        for (Entity entity : level.entities) {
            if(entity.getEntityTypeName().equals("Fish")) {
                nFishRemaining++;
                
                Fish fish = (Fish)entity;
//                Control c = fish.getControl(ANNControl.class);
//                if(c==null) continue;
//                ANNControl annc = (ANNControl)c;
                
                if(fish.fitness() > surviveThreshold) {
                    nSurvive++;
                    fish.age++;
                }
                else {
                    fish.setGene(genePool.createOffspring());
                }
                
                fish.reset();
            }
            
            // dirty workaround for resetting plant food collected:
            else if(entity.getEntityTypeName().equals("Plant")) {
                Plant plant = (Plant)entity;
                plant.reset();
            }
            
        }
        info.append("\n");
        info.append("Survived (Aged): ").append(nSurvive).append("\n");
        
        // refill fish population
        for (int i = 0; i < Level.nFish - nFishRemaining; i++) {
            if(genePool.isEmpty() || rnd.nextFloat() < genePool.superMutantRate) {
                new Fish(level);
            }
            else {
                new Fish(level, genePool.createOffspring());
            }
        }
        info.append("Repopulated: ").append(Level.nFish-nFishRemaining);
        
    }
    
    
}