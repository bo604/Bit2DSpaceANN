/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.evo;

import bit2dspaceann.entities.Entity;
import bit2dspaceann.entities.EvoMob;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.gfx.graphs.Chart2f;
import bit2dspaceann.util.MiscMath;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Bo
 */
public class GenePool extends ArrayList<Gene> {
    
    private static final Random rnd = new Random();
    private static final DecimalFormat dec2 = new DecimalFormat("0.##");
    
    int generation = 1;
    
    private final Chart2f fitnessHistoryChart;
    private final Chart2f fitnessDistributionChart;
    
    private float variationRateStart = .04f; //0.15f;
    private float variationRateMin = 0.0001f; //0.04f;
    public float variationRate = variationRateStart;
    private float variationRateDecay = 0.005f; //0.99f;
    private float variationDistance = 0.1f;
    
    private float mutationRateStart = .04f; //0.15f;
    private float mutationRateMin = 0.0001f; //0.02f;
    public float mutationRate = mutationRateStart;
    private float mutationRateDecay = 0.015f; //0.98f;

    private float superMutantRateStart = 0.5f;
    public float superMutantRate = superMutantRateStart;
    private float superMutantRateDecay = 0.02f;
    
    public Gene bestGene = null;

    public GenePool() {
        super();
        // fitness history chart
        fitnessHistoryChart = new Chart2f("Avrg. Fit. History",
                Gfx.IMAGE_PIXEL_WIDTH-840,Gfx.IMAGE_PIXEL_HEIGHT-16);
        fitnessHistoryChart.labelX = "Generation";
        fitnessHistoryChart.renderAverage = true;
        // fitness distribution chart
        fitnessDistributionChart = new Chart2f("Fitness Distribution",
                Gfx.IMAGE_PIXEL_WIDTH-420,Gfx.IMAGE_PIXEL_HEIGHT-16);
        fitnessDistributionChart.labelX = "Population";
    }
    
    public GenePool(Collection<? extends Gene> clctn) {
        this();
        this.addAll(clctn);
    }
    
    public void collectGenes(List<Entity> entities, String entityType) {
        this.clear();
        for (Entity entity : entities) {
            if(entity.getEntityTypeName().equals(entityType)) {
                EvoMob mob = (EvoMob)entity;
                this.add(mob.getGene());
//                System.out.println("Gene collected from " + mob.getEntityTypeName()+ " " + mob);
            }
        }
        Collections.sort(this);
        if(this.isEmpty())
            bestGene = null;
        else
            bestGene = this.get(0);
    }
    
    private static final int geneRenderScale = 2;
    
    public void render(Screen screen, int x, int y) {
        if(this.isEmpty()) return;
        
        // render best gene
        if(bestGene!=null) {
            bestGene.render2D(screen, 4, 20, Gene.UP);
        }
        
        // charts
        if(Gfx.RENDER_CHARTS) {
            fitnessHistoryChart.render(screen);
            fitnessDistributionChart.render(screen);
        }
        
        if(!Gfx.RENDER_GENEPOOL) return;
        int idx = 0;
        for (Gene gene : this) {
            gene.render(screen, x, y+idx*(1+geneRenderScale), geneRenderScale);
            idx++;
        }
    }
    
    public Gene selectGeneWeighted() {
        float totFit = 0;
        for (Gene gene : this) {
            totFit += gene.fitness;
        }
        float r = rnd.nextFloat();
        float relFit = 0;
        for (Gene gene : this) {
            relFit += gene.fitness / totFit;
            if(r<relFit) {
//                System.out.println("Selected gene fitness: " + gene.fitness);
                return gene;
            }
        }
        throw new RuntimeException("FUCK UP: NO GENE SELECTED!");
    }
    
    public float[] combineGenes(Gene g1, Gene g2) {
        int nGenes = g1.gene.length;
        float[] newGene = new float[nGenes];
        
        // selection probability based on fitness
        float p1 = g1.fitness / (g1.fitness + g2.fitness);
//        float p2 = g2.fitness / (g1.fitness + g2.fitness);
//        System.out.println("g1f=" + g1.fitness + "  p1="+p1);
//        System.out.println("g2f=" + g2.fitness + "  p2="+p2);
        
        // combine
        for (int i = 0; i < nGenes; i++) {
            Gene dominantGene = rnd.nextFloat() < p1 ? g1 : g2;
            // mutate, vary, or select dominant gene
            float r = rnd.nextFloat();
            // mutate?
            if(r<mutationRate) {
                newGene[i] = 2 * rnd.nextFloat() - 1;
            }
            // vary dominant?
            else if(r<mutationRate+variationRate) {
                newGene[i] = dominantGene.gene[i];
                newGene[i] += variationDistance * (2*rnd.nextFloat()-1);
                if(newGene[i]>1) newGene[i]=1;
                if(newGene[i]<-1) newGene[i]=-1;
            }
            // select dominant
            else {
                newGene[i] = dominantGene.gene[i];
            }
        }
        return newGene;
    }
    
    public float[] createOffspring() {
        return combineGenes(selectGeneWeighted(), selectGeneWeighted());
    }

    public void updateGenerationRates() {
        generation++;
        mutationRate = MiscMath.expDecay(generation, mutationRateDecay, mutationRateMin, mutationRateStart);
        variationRate = MiscMath.expDecay(generation, variationRateDecay, variationRateMin, variationRateStart);
        superMutantRate = MiscMath.expDecay(generation, superMutantRateDecay, .01f, superMutantRateStart);
    }

    public void appendInfo(StringBuilder info) {
        info.append("Last Generation: ").append(generation).append("\n");
        info.append("\n");
        info.append(" Mutation Rate: ").append(dec2.format(100f*mutationRate)).append("%\n");
        info.append("Variation Rate: ").append(dec2.format(100f*variationRate)).append("%\n");
        info.append("Supermut. Rate: ").append(dec2.format(100f*superMutantRate)).append("%\n");
    }
    
    public float totalFitness() {
        float totFit = 0;
        for (int i = 0; i < size(); i++) {
            totFit += get(i).fitness;
        }
        return totFit;
    }
    
    public float maxFitness() {
        float topFit = 0;
        for (int i = 0; i < this.size(); i++) {
            float fit = this.get(i).fitness;
            if(fit > topFit) {
                topFit = fit;
            }
        }
        return topFit;
    }
    
    public void appendStats(StringBuilder info) {
        // total fitness
        float totFit = totalFitness();
        float avrgFit = totFit/size();
        float maxFit = maxFitness();
        
        // fitness distribution
        float[] fitDist = new float[size()];
        for (int i = 0; i < size(); i++) {
            fitDist[i] = get(i).fitness;
        }
        
        fitnessDistributionChart.setData(fitDist);
        fitnessHistoryChart.addDataPoint(avrgFit);
        
        info.append("\n");
        info.append("Total Fitness: ").append(totFit).append("\n");
        info.append("Avrg. Fitness: ").append(dec2.format(avrgFit)).append("\n");
        info.append(" Max. Fitness: ").append(maxFit).append("\n");
//        System.out.println("Best Gene: " + Arrays.toString(genes.get(0).gene));
    }
    
}
