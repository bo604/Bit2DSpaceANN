/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.test;

import bit2dspaceann.gfx.rotations.Rotations0xFF;

/**
 *
 * @author Bo
 */
public class TestMain {

    public static void main(String[] args) {

        TestMain main = new TestMain();

//        main.intDivsVsBinOps();

//        main.testRotations0xFF();

        main.testBitModulo();

        
    }


    private void testBitModulo() {

        int n = 37;
        for (int i = -n; i < n; i++) {
            System.out.println("i="+i+" -> &32=" + ( i & 0x1f ) );
        }

    }


    private void testRotations0xFF() {

        int dx = 1;
        int dy = 0;
        int[] vectorToRotate = new int[] {dx,dy};

        Rotations0xFF rot = new Rotations0xFF();
        int rotationMatrix = rot.getRotationMatrix(128);
        Rotations0xFF.rotate(rotationMatrix, vectorToRotate);

//        Arrays.toString(vectorToRotate);

    }


    /*
     * THE ULTIMATE BATTLE! ....or maybe not.
     * Standard integer division vs. binary operations.
     */
    private void intDivsVsBinOps() {
        int n = 4000;
        int m = 10;
        long start = 0;
        long stop = 0;
        int r = 0;

        long duraDiv = 0;
        long duraBin = 0;

        // Loop for averaging
        for (int k = 0; k < m; k++) {

            // Test standard integer operations, division and modulo (costly)
            start = System.nanoTime();
            for (int i = 1; i < n; i++) {
                for (int j = 1; j < n; j++) {
                    r = i / j;
                    r = i % j;
                    r = j / i;
                    r = j % i;
                }
            }
            stop = System.nanoTime();
            duraDiv += (stop - start) / 1000000;

            // Test binary operations   (teh shiznit!)
            start = System.nanoTime();
            for (int i = 1; i < n; i++) {
                for (int j = 1; j < n; j++) {
                    r = i >> j;     // if j is equal to 2^anything, this is similar to i / j
                    r = i & j;      // if j is equal to 2^anything, this is similar to i % j
                    r = j >> i;
                    r = j & i;
                }
            }
            stop = System.nanoTime();
            duraBin += (stop - start) / 1000000;

        }

        System.out.println("/%: " + duraDiv/10 + "ms");
        System.out.println(">>&: " + duraBin/10 + "ms");
    }


}
