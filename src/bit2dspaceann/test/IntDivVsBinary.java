
package bit2dspaceann.test;

/**
 * THE ULTIMATE BATTLE! ....or maybe not.
 * Standard integer division vs. binary operations.
 *
 * @author Bo
 */
public class IntDivVsBinary {

    public static void main(String[] args) {
        int n = 4000;
        int m = 10;
        long start = 0;
        long stop = 0;
        int r = 0;

        long duraDiv = 0;
        long duraBin = 0;

        // Loop for averaging
        for (int k = 0; k < m; k++) {

            // Test standard integer operations, division and modulo (costly)
            start = System.nanoTime();
            for (int i = 1; i < n; i++) {
                for (int j = 1; j < n; j++) {
                    r = i / j;
                    r = i % j;
                    r = j / i;
                    r = j % i;
                }
            }
            stop = System.nanoTime();
            duraDiv += (stop - start) / 1000000;

            // Test binary operations   (teh shiznit!)
            start = System.nanoTime();
            for (int i = 1; i < n; i++) {
                for (int j = 1; j < n; j++) {
                    r = i >> j;     // if j is equal to 2^anything, this is similar to i / j
                    r = i & j;      // if j is equal to 2^anything, this is similar to i % j
                    r = j >> i;
                    r = j & i;
                }
            }
            stop = System.nanoTime();
            duraBin += (stop - start) / 1000000;

        }

        // Show awesomeness
        System.out.println("Time for " + n * n * 2 + " runs of / and %: " + duraDiv/m + "ms");
        System.out.println("Time for " + n * n * 2 + " runs of >> and &: " + duraBin/m + "ms");
    }


}
