package bit2dspaceann.level;

import bit2dspaceann.entities.Entity;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Bo
 */
public class Level_bkp02 {

    public List<Entity> entities = new ArrayList<Entity>();

    public boolean contains(float x, float y) {
        if(x<0 || x>=Gfx.IMAGE_PIXEL_WIDTH) return false;
        if(y<0 || y>=Gfx.IMAGE_PIXEL_HEIGHT) return false;
        return true;
    }

    public void addEntity(Entity entity) {
        entities.add(entity);
    }

    public void tick() {
        for (Iterator<Entity> it = entities.iterator(); it.hasNext();) {
            Entity entity = it.next();
            if(entity.flaggedForRemoval) {
                it.remove();
            }
            else {
                entity.tick();
            }
        }
        for (Tile tile : Tile.tiles) {
            if(tile==null) break;
            tile.tick();
        }
    }

    public void renderEntities(Screen screen) {
        for (Entity entity : entities) {
                entity.render(screen);
        }
    }
    
}