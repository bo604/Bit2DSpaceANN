//package bit2dspaceann.level;
//
//import bit2dspaceann.entities.ControlledMob;
//import bit2dspaceann.entities.Entity;
//import bit2dspaceann.entities.Food;
//import bit2dspaceann.entities.controls.Control;
//import bit2dspaceann.entities.controls.TrajectoryControl;
//import bit2dspaceann.entities.controls.VelocityControl;
//import bit2dspaceann.gfx.ColorsLong;
//import bit2dspaceann.gfx.Gfx;
//import bit2dspaceann.gfx.Screen;
//import bit2dspaceann.util.Rect2Di;
//import java.awt.geom.Rectangle2D;
//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.LinkedList;
//import java.util.List;
//import java.util.Random;
//import java.util.concurrent.Callable;
//import java.util.concurrent.Future;
//import java.util.concurrent.ScheduledThreadPoolExecutor;
//
///**
// *
// * @author Bo
// */
//public class Level_bkp04 {
//    
//    public static final int nFish = 50; //40;
//    public static final int nSharks = 0;
//    public static final int nFood = 100; //180;
//    public static final int nPlantsTox = 3; //180;
//    public static final int nPlantsFood = 3; //180;
//
//    private static final Random rnd = new Random();
//    public List<Entity> entities = new ArrayList<Entity>();
//    
//
//    public boolean contains(float x, float y) {
//        if(x<0 || x>=Gfx.IMAGE_PIXEL_WIDTH) return false;
//        if(y<0 || y>=Gfx.IMAGE_PIXEL_HEIGHT) return false;
//        return true;
//    }
//
//    public void addEntity(Entity entity) {
//        entities.add(entity);
//    }
//    
//    private final int nCores = 7;
//    private final ScheduledThreadPoolExecutor exec = new ScheduledThreadPoolExecutor(nCores);
//    private final LinkedList<Future<Object>> futures = new LinkedList<Future<Object>>();
//
//    public void tick() {
//        
//        for (Iterator<Entity> it = entities.iterator(); it.hasNext();) {
//            Entity entity = it.next();
//            if(entity.flaggedForRemoval) {
//                it.remove();
//            }
//            else {
////                entity.tick();
//                futures.add(exec.submit(new EntityTicker(entity)));
//            }
//        }
//        boolean isDone = false;
//        while(!isDone) {
//            for (Iterator<Future<Object>> it = futures.iterator(); it.hasNext();) {
//                Future<Object> future = it.next();
//                if(future.isDone()) it.remove();
//            }
//            if(futures.isEmpty()) isDone = true;
//        }
//        
//        launchFoods();
//    }
//    
//    public void tickParallelStream() {
//        for (Iterator<Entity> it = entities.iterator(); it.hasNext();) {
//            Entity entity = it.next();
//            if(entity.flaggedForRemoval)
//                it.remove();
//        }
//        entities.parallelStream().forEach(Entity::tick);
//        launchFoods();
//    }
//    
//    
//    private class EntityTicker implements Callable<Object> {
//        final Entity entity;
//
//        public EntityTicker(Entity entity) {
//            this.entity = entity;
//        }
//        
//        public Object call() throws Exception {
//            entity.tick();
//            return null;
//        }
//        
//    }
//
//    public void renderEntities(Screen screen) {
//        for (Entity entity : entities) {
//                entity.render(screen);
//        }
//    }
//    
//    /***
//     * DOES NOT WORK! WHY????
//     * 
//     * @param x
//     * @param y
//     * @param w
//     * @param h
//     * @param dest 
//     */
//    public void getEntitiesInside(int x, int y, int w, int h, List<Entity> dest) {
//        dest.clear();
//        for (Entity e : entities) {
//            if( e.xWorld > x   && e.yWorld > y
//             && e.xWorld < x+w && e.yWorld < y+h)
//                dest.add(e);
//        }
//    }
//    
//    public void getEntitiesInside(Rect2Di r, List<Entity> dest) {
////        this.getEntitiesInside(r.x, r.y, r.w, r.h, dest);
//        dest.clear();
//        for (Entity entity : entities) {
//            if(r.contains(entity.xWorld, entity.yWorld)) {
//                dest.add(entity);
//            }
//        }
//    }
//    
//    public void getEntitiesInside(Rectangle2D rect, List<Entity> dest) {
//        dest.clear();
//        for (Entity entity : entities) {
//            if(rect.contains(entity.xWorld, entity.yWorld)) {
//                dest.add(entity);
//            }
//        }
//    }
//    
//    
//
////    public void getEntitiesInside(Rectangle2D rect, List<Entity> dest, String filter) {
////        dest.clear();
////        for (Entity entity : entities) {
////            if( ! entity.flaggedForRemoval
////               && entity.getEntityTypeName().equals(filter)
////               && rect.contains(entity.xWorld, entity.yWorld)) {
////                    dest.add(entity);
////            }
////        }
////    }
//
//
//    public void resetControlledMobPositions() {
//        for (Entity entity : entities) {
////            if(entity.getEntityTypeName().equals("Fish")) {
//            if(entity instanceof ControlledMob) {
//                entity.xWorld = rnd.nextFloat() * Gfx.IMAGE_PIXEL_WIDTH;
//                entity.yWorld = rnd.nextFloat() * Gfx.IMAGE_PIXEL_HEIGHT;
//            }
//        }
//    }
//    
//    public void resetFood(int nFood) {
//        for (Entity entity : entities) {
//            if(entity.getEntityTypeName().equals("Food")) {
//                entity.flaggedForRemoval = true;
//            }
//        }
//        for (int i = 0; i < nFood; i++) {
//            new Food(this);
//        }
//    }
//    
//    public void fillFood() {
//        int nFoodLeft = 0;
//        for (Entity entity : entities) {
//            if(entity.getEntityTypeName().equals("Food")) {
//                nFoodLeft++;
//            }
//        }
//        for (int i = 0; i < Level_bkp04.nFood-nFoodLeft; i++) {
////            makeFood(true);
//            new Food(this);
//        }
//    }
//    
//    public void fillFoodFromTop() {
//        int nFoodLeft = 0;
//        for (Entity entity : entities) {
//            if(entity.getEntityTypeName().equals("Food")) {
//                nFoodLeft++;
//            }
//        }
//        for (int i = 0; i < Level_bkp04.nFood-nFoodLeft; i++) {
//            float x = rnd.nextFloat() * Gfx.IMAGE_PIXEL_WIDTH;
//            float y = rnd.nextFloat() * 0.1f * Gfx.IMAGE_PIXEL_HEIGHT;
//            new Food(this,x,y);
//        }
//    }
//    
//    public static long launchedFoodColor = ColorsLong.get(-1, 444, 222, 443, 440, 550, 552, 331);
//    
//    private final List<Entity> foodLauncher = new ArrayList<Entity>();
//    
//    public void prepareLaunchFood(Entity launcher) {
//        foodLauncher.add(launcher);
//    }
//    
//    public void launchFoods() {
//        for (Iterator<Entity> it = foodLauncher.iterator(); it.hasNext();) {
//            Entity launcher = it.next();
//            launchFood(launcher);
//            it.remove();
//        }
//    }
//    
//    private void launchFood(Entity launcher) {
//        Food food = new Food(this, launcher.xWorld, launcher.yWorld);
////        food.color = Arrays.copyOf(launcher.color, 3);
////        food.color = new float[] {.8f, .7f, .2f};
//        food.color = new float[] {1, 1, 0};
//        food.setSpriteColor(launchedFoodColor);
//
//        VelocityControl vc = new VelocityControl(food);
//        vc.v.set(2*rnd.nextFloat()-1, 2*rnd.nextFloat()-1);
////        ColorControl cc = new ColorControl(food);
//    }
//    
//    public void clearTrajectories() {
//        for (Entity entity : entities) {
//            if(entity instanceof ControlledMob) {
//                ControlledMob mob = (ControlledMob)entity;
//                Control c = mob.getControl(TrajectoryControl.class);
//                if(c!=null) {
//                    TrajectoryControl tc = (TrajectoryControl)c;
//                    tc.clear();
//                }
//            }
//        }
//    }
//    
//}