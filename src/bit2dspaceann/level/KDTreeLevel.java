///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package bit2dspaceann.level;
//
//import bit2dspaceann.entities.Entity;
//import java.awt.geom.Rectangle2D;
//import java.util.List;
//import net.sf.javaml.core.kdtree.KDTree;
//
///**
// *
// * @author Bo
// */
//public class KDTreeLevel extends Level {
//    
//    private KDTree kdTree;
//
//    private void rebuildKdTree() {
//        kdTree = new KDTree(2);
//        for (Entity entity : entities) {
//            double[] key = new double[] {entity.xWorld, entity.yWorld};
//            kdTree.insert(key, entity);
//        }
//    }
//
//    @Override
//    public void tick() {
//        rebuildKdTree();
//        super.tickParallelStream();
//    }
//    
//    @Override
//    public void getEntitiesInside(Rectangle2D rect, List<Entity> dest) {
//        if(!dest.isEmpty()) dest.clear();
//        double[] lowk = new double[] { rect.getX(), rect.getY() };
//        double[] uppk = new double[] { rect.getX()+rect.getWidth(), rect.getY()+rect.getHeight() };
//        Object[] ents = kdTree.range(lowk, uppk);
//        for (int i = 0; i < ents.length; i++) {
//            dest.add((Entity)ents[i]);
//        }
//    }
//    
//}
