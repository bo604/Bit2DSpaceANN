///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//
//package bit2dspaceann.level;
//
//import bit2dspaceann.entities.Entity;
//import bit2dspaceann.gfx.Gfx;
//import bit2dspaceann.gfx.Screen;
//import java.awt.image.BufferedImage;
//import java.io.File;
//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Random;
//import javax.imageio.ImageIO;
//
///**
// *
// * @author Bo
// */
//public class Level_bkp01 {
//
//    // Width and height in number of tiles (atm. 64x64)
//    public int levelWidth = 128;
//    public int levelHeight = 128;
//
//    private byte[] tiles;
//
//    public List<Entity> entities = new ArrayList<Entity>();
////    private AnimMaskPlayer player = null;
//
//    private String imagePath;
//    private BufferedImage image;
//
//    public Level_bkp01(String imagePath) {
//
////        /* DONT LOAD LEVEL FOR NOW!!! */
//        
//        if(imagePath!=null) {
//            this.imagePath = imagePath;
//            loadLevelFromFile();
//        }
//        else {
//            this.levelWidth = 256;
//            this.levelHeight = 256;
//            tiles = new byte[levelWidth*levelHeight];
//            generateLevel();
//        }
//
//    }
//
//
//    private void loadLevelFromFile() {
//        try {
//            image = ImageIO.read( Level_bkp01.class.getResource( imagePath ) );
//            levelWidth = image.getWidth();
//            levelHeight = image.getHeight();
//            tiles = new byte[levelWidth*levelHeight];
//            loadTiles();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    
//    private void loadTiles() {
//        int[] tileMapColors = image.getRGB(0, 0, levelWidth, levelHeight, null, 0, levelWidth);
//        for (int y = 0; y < levelHeight; y++) {
//            for (int x = 0; x < levelWidth; x++) {
//                for (Tile tile : Tile.tiles) {
//                    if(tile==null) break;
//                    if(tile.getMapColor() == tileMapColors[x+y*levelWidth]) {
//                        tiles[x + y * levelWidth] = tile.getID();
//                    }
//                }
//            }
//        }
//    }
//
//
//    private void saveLevelToFile() {
//        try {
//            ImageIO.write(image, "png", new File( Level_bkp01.class.getResource(imagePath).getFile()) );
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void alterTile(int x, int y, Tile newTile) {
//        tiles[x + y * levelWidth] = newTile.getID();
//        image.setRGB(x, y, newTile.getMapColor());
//    }
//
//
//    public final void generateLevel() {
//        for (int y = 0; y < levelHeight; y++) {
//            for (int x = 0; x < levelWidth; x++) {
////                if(x*y%10<7) {
////                    tiles[x+y*levelWidth] = Tile.GRASS.getID();
////                }
////                else {
////                    tiles[x+y*levelWidth] = Tile.STONE.getID();
////                }
//                tiles[x+y*levelWidth] = Tile.SPACE.getID();
//            }
//        }
//    }
//
//    public boolean contains(float x, float y) {
//        if(x<0 || x>=Gfx.IMAGE_PIXEL_WIDTH) return false;
//        if(y<0 || y>=Gfx.IMAGE_PIXEL_HEIGHT) return false;
//        return true;
//    }
//
//    public void addEntity(Entity entity) {
//        entities.add(entity);
//    }
//
//    Random rnd = new Random();
//    public final int rndXWorld() { return rnd.nextInt( (levelWidth-1) << Gfx.TILE_SIZE_LOG2 ); }
//    public final int rndYWorld() { return rnd.nextInt( (levelHeight-1) << Gfx.TILE_SIZE_LOG2 ); }
//
//    public void tick() {
//        for (Iterator<Entity> it = entities.iterator(); it.hasNext();) {
//            Entity entity = it.next();
//            if(entity.flaggedForRemoval) {
//                it.remove();
//            }
//            else {
//                entity.tick();
//            }
//        }
//        for (Tile tile : Tile.tiles) {
//            if(tile==null) break;
//            tile.tick();
//        }
//    }
//
//    public void renderEntities(Screen screen) {
//        for (Entity entity : entities) {
//                entity.render(screen);
//        }
//    }
////            if(isTileVisibleFromTile(entity.xWorld>>Gfx.TILE_SIZE_LOG2, entity.yWorld>>Gfx.TILE_SIZE_LOG2,
////                                    player.xWorld>>Gfx.TILE_SIZE_LOG2, player.yWorld>>Gfx.TILE_SIZE_LOG2))
//
//
//    public void renderTiles(Screen screen, int playerWorldX, int playerWorldY) {
//
//        // Screen position (center on player)
//        int xOffset = playerWorldX - (screen.pWidth>>1);
//        int yOffset = playerWorldY - (screen.pHeight>>1);
//        // Bounds
//        xOffset = bindX(xOffset, screen.pWidth);
//        yOffset = bindY(yOffset, screen.pHeight);
//        // Bind screen offset ("cam position") to level boundaries
//        screen.setOffset(xOffset, yOffset);
//        // Culling x
//        int xTileMin = xOffset >> Gfx.TILE_SIZE_LOG2;
//        int xTileMax = ((xOffset + screen.pWidth) >> Gfx.TILE_SIZE_LOG2) + 1;
//        // Culling y
//        int yTileMin = yOffset >> Gfx.TILE_SIZE_LOG2;
//        int yTileMax = ((yOffset + screen.pHeight) >> Gfx.TILE_SIZE_LOG2) + 2;
//        // Render visible tiles
//        for (int yTile = yTileMin; yTile < yTileMax; yTile++) {
//            for (int xTile = xTileMin; xTile < xTileMax; xTile++) {
//                /*
//                 * LOW RES TILE VISIBILITY:
//                 */
////                if(!true || isTileVisibleFromTile(xTile, yTile, playerWorldX >> Gfx.TILE_SIZE_LOG2, playerWorldY >> Gfx.TILE_SIZE_LOG2)) {
////                    getTile(xTile,yTile).render(screen, this, xTile << Gfx.TILE_SIZE_LOG2, yTile << Gfx.TILE_SIZE_LOG2 );
////                }
////                else {
////                    Tile.VOID.render(screen, this, xTile << Gfx.TILE_SIZE_LOG2, yTile << Gfx.TILE_SIZE_LOG2);
////                }
//
//
//                Tile tile = getTile(xTile, yTile);
//                if(tile.getID() != Tile.VOID.getID() && tile.getID() != Tile.SPACE.getID()) {
//                    tile.render(screen, this, xTile << Gfx.TILE_SIZE_LOG2, yTile << Gfx.TILE_SIZE_LOG2 );
//                }
//            }
//        }
//
//    }
//
//
//    protected int bindX(int xOffset, int screenWidth) {
//        if(xOffset < 0) xOffset = 0;
//        int xOffMax = (levelWidth << Gfx.TILE_SIZE_LOG2) - screenWidth;
//        if(xOffset > xOffMax) xOffset = xOffMax;
//        return xOffset;
//    }
//
//    protected int bindY(int yOffset, int screenHeight) {
//        if(yOffset < 0) yOffset = 0;
//        int yOffMax = (levelHeight << Gfx.TILE_SIZE_LOG2) - screenHeight;
//        if(yOffset > yOffMax) yOffset = yOffMax;
//        return yOffset;
//    }
//
//
//    public Tile getTile(int x, int y) {
//        if(x<0 || y<0 || x>=levelWidth || y>=levelHeight) return Tile.VOID;
//        return Tile.tiles[ tiles[x+y*levelWidth] ];
//    }
//
//
//    /**
//     * @param ttx target tile index
//     * @param tty
//     * @param ptx player tile index
//     * @param pty
//     * @return
//     */
//    public boolean isTileVisibleFromTile(int ttx, int tty, int ptx, int pty) {
//        // Delta of tile indices
//        int dtx = ttx - ptx;
//        int dty = tty - pty;
//        int max = Math.max( Math.abs(dtx), Math.abs(dty) );
//        // Check direct path between tiles for vision block:
//        for (int i = 1; i < max; i++) {
//            int tx = ptx + i * dtx / max;
//            int ty = pty + i * dty / max;
//            if(getTile(tx, ty).isSolid()) {
//                // View blocked! Not Visible.
//                return false;
//            }
//        }
//        // Is Visible
//        return true;
//    }
//
//    /**
//     * @param tpx target tile index
//     * @param tpy
//     * @param ppx player tile index
//     * @param ppy
//     * @return
//     */
//    public boolean isPixelVisibleFromPixel(int tpx, int tpy, int ppx, int ppy) {
//        // Delta of pixels
//        int dpx = tpx - ppx;
//        int dpy = tpy - ppy;
//        int max = Math.max( Math.abs(dpx), Math.abs(dpy) );
//        // Check direct path between pixels for tiles blocking vision:
//        for (int i = 1; i < max; i++) {
//            int px = ppx + i * dpx / max;
//            int py = ppy + i * dpy / max;
//            if(getTile(px >> Gfx.TILE_SIZE_LOG2, py >> Gfx.TILE_SIZE_LOG2).isSolid()) {
//                // View blocked! Not Visible.
//                return false;
//            }
//        }
//        // Is Visible
//        return true;
//    }
//}
//
//// <editor-fold defaultstate="collapsed" desc="old 5">
////    public abstract void renderTiles(Screen screen, int playerWorldX, int playerWorldY);
////
//////    public void renderTiles(Screen screen, int playerWorldX, int playerWorldY) {
//////        // Screen position (center on player)
//////        int xOffset = playerWorldX - screen.pWidth/2;
//////        int yOffset = playerWorldY - screen.pHeight/2;
//////        // Bounds
//////        xOffset = bindX(xOffset, screen.pWidth);
//////        yOffset = bindY(yOffset, screen.pHeight);
//////        // Bind screen offset ("cam position") to level boundaries
//////        screen.setOffset(xOffset, yOffset);
//////        // Culling x
//////        int xTileMin = xOffset >> Gfx.TILE_SIZE_LOG2;
//////        int xTileMax = ((xOffset + screen.pWidth) >> Gfx.TILE_SIZE_LOG2) + 1;
//////        // Culling y
//////        int yTileMin = yOffset >> Gfx.TILE_SIZE_LOG2;
//////        int yTileMax = ((yOffset + screen.pHeight) >> Gfx.TILE_SIZE_LOG2) + 2;
//////        // Render visible tiles
//////        for (int yTile = yTileMin; yTile < yTileMax; yTile++) {
//////            for (int xTile = xTileMin; xTile < xTileMax; xTile++) {
//////                /*
//////                 * LOW RES TILE VISIBILITY:
//////                 */
//////                if(!true || isTileVisibleFromTile(xTile, yTile, playerWorldX >> Gfx.TILE_SIZE_LOG2, playerWorldY >> Gfx.TILE_SIZE_LOG2)) {
//////                    getTile(xTile,yTile).render(screen, this, xTile << Gfx.TILE_SIZE_LOG2, yTile << Gfx.TILE_SIZE_LOG2 );
//////                }
//////                else {
//////                    Tile.VOID.render(screen, this, xTile << Gfx.TILE_SIZE_LOG2, yTile << Gfx.TILE_SIZE_LOG2);
//////                }
//////            }
//////        }
//////    }
////    private final PixelVisibilityChecker visChecker = new PixelVisibilityChecker(this);
////    public class PixelVisibilityChecker {
////        public Level level;
////        public int playerWorldX, playerWorldY;
////        public PixelVisibilityChecker(Level level) {
////            this.level = level;
////        }
////        public boolean isPixelVisible(int px, int py) {
////            return level.isPixelVisibleFromPixel(px, py, playerWorldX, playerWorldY);
////        }
////    }
////    /*
////     * when redo: use one pixel per tile?
////     */
////    public int[] renderSolidMap(int xOffset, int yOffset) {
////
////        int pWidth = Gfx.IMAGE_PIXEL_WIDTH;
////        int pHeight = Gfx.IMAGE_PIXEL_HEIGHT;
////
////        int[] solids = new int[pWidth * pHeight];
////        Arrays.fill(solids, 0xFFFFFF);
////
////        // Bounds x
////        if(xOffset < 0) xOffset = 0;
////        int xOffMax = (levelWidth << Gfx.TILE_SIZE_LOG2) - pWidth;
////        if(xOffset > xOffMax) xOffset = xOffMax;
////        // Bounds y
////        if(yOffset < 0) yOffset = 0;
////        int yOffMax = (levelHeight << Gfx.TILE_SIZE_LOG2) - pHeight;
////        if(yOffset > yOffMax) yOffset = yOffMax;
////
//////        // Bind screen offset ("cam position") to level boundaries
//////        screen.setOffset(xOffset, yOffset);
////
////        // Culling x
////        int xTileMin = xOffset >> Gfx.TILE_SIZE_LOG2;
////        int xTileMax = ((xOffset + pWidth) >> Gfx.TILE_SIZE_LOG2) + 1;
////        // Culling y
////        int yTileMin = yOffset >> Gfx.TILE_SIZE_LOG2;
////        int yTileMax = ((yOffset + pHeight) >> Gfx.TILE_SIZE_LOG2) + 2;
////
////        System.out.println("xmax="+xTileMax);
////        System.out.println("ymax="+yTileMax);
////
////        // Render visible tiles
////        for (int yTile = yTileMin; yTile < yTileMax; yTile++) {
////            for (int xTile = xTileMin; xTile < xTileMax; xTile++) {
//////                getTile(xTile,yTile).render(screen, this, xTile << Gfx.TILE_SIZE_LOG2, yTile << Gfx.TILE_SIZE_LOG2 );
////
////                Tile tile = getTile(xTile, yTile);
////                if(tile.isSolid()) {
////                    for (int i = 0; i < Gfx.TILE_SIZE; i++) {
////                        for (int j = 0; j < Gfx.TILE_SIZE; j++) {
////                            int x = - xOffset + (xTile << Gfx.TILE_SIZE_LOG2) + i;
////                            int y = - yOffset + (yTile << Gfx.TILE_SIZE_LOG2) + j;
////                            if(x<0 || x >= pWidth) continue;
////                            if(y<0 || y >= pHeight) continue;
////                            solids[x + y * pWidth] = 0x000000;
////                        }
////                    }
////                }
////            }
////        }
////
////        return solids;
////    }
//// </editor-fold>
//
//// <editor-fold defaultstate="collapsed" desc="old 4">
////    /**
////     * senseless ray casting... VERY SLOW... (10fps)
////     */
////    public void renderTilesHighShadow(Screen screen, int playerWorldX, int playerWorldY) {
////        visChecker.playerWorldX = playerWorldX;
////        visChecker.playerWorldY = playerWorldY;
////        // Screen position (center on player)
////        int xOffset = playerWorldX - screen.pWidth/2;
////        int yOffset = playerWorldY - screen.pHeight/2;
////        // Bounds
////        xOffset = bindX(xOffset, screen.pWidth);
////        yOffset = bindY(yOffset, screen.pHeight);
////        // Bind screen offset ("cam position") to level boundaries
////        screen.setOffset(xOffset, yOffset);
////        // Culling x
////        int xTileMin = xOffset >> Gfx.TILE_SIZE_LOG2;
////        int xTileMax = ((xOffset + screen.pWidth) >> Gfx.TILE_SIZE_LOG2) + 1;
////        // Culling y
////        int yTileMin = yOffset >> Gfx.TILE_SIZE_LOG2;
////        int yTileMax = ((yOffset + screen.pHeight) >> Gfx.TILE_SIZE_LOG2) + 2;
////        // Render visible tiles
////        for (int yTile = yTileMin; yTile < yTileMax; yTile++) {
////            for (int xTile = xTileMin; xTile < xTileMax; xTile++) {
////                /*
////                 * LOW RES TILE VISIBILITY:
////                 */
////                if(isTileVisibleFromTile(xTile, yTile, playerWorldX >> Gfx.TILE_SIZE_LOG2, playerWorldY >> Gfx.TILE_SIZE_LOG2)) {
//////                    getTile(xTile,yTile).render(screen, this, xTile << Gfx.TILE_SIZE_LOG2, yTile << Gfx.TILE_SIZE_LOG2 );
////                    getTile(xTile,yTile).renderHighShadow(screen, this, xTile << Gfx.TILE_SIZE_LOG2, yTile << Gfx.TILE_SIZE_LOG2, visChecker);
////                }
////                else {
////                    Tile.VOID.render(screen, this, xTile << Gfx.TILE_SIZE_LOG2, yTile << Gfx.TILE_SIZE_LOG2);
////                }
//////                /*
//////                 * HIGH RES PIXEL VISIBILITY:
//////                 */
//////                getTile(xTile,yTile).renderHighShadow(screen, this, xTile << Gfx.TILE_SIZE_LOG2, yTile << Gfx.TILE_SIZE_LOG2, visChecker);
////
////            }
////        }
////    }
////    private final List<Entity> toRemove = new ArrayList<Entity>();
////    public void removeEntity(Entity entity) {
////        toRemove.add(entity);
////    }
////    private void removeEntities() {
////        for (Entity entity : toRemove) {
////            entities.remove(entity);
////        }
////    }
////    public Player getPlayer() {
////        if(player==null) {
////            for (Entity entity : entities) {
////                if(entity.getEntityName().equals("Player")) {
////                    player = (Player)entity;
////                    return player;
////                }
////            }
////            throw new RuntimeException("Player not found in level!");
////        }
////        else {
////            return player;
////        }
////    }
////    /**
////     * NO CULLING:
////     *  - Will render all tiles, even if outside of current view!
////     * @param screen
////     * @param xOffset
////     * @param yOffset
////     */
////    public void renderTilesNoCull(Screen screen, int xOffset, int yOffset) {
////        if(xOffset < 0) xOffset = 0;
////        if(yOffset < 0) yOffset = 0;
////
////        int xOffMax = (width << SHEET_TILE_WIDTH_LOG2) - screen.width;
////        if(xOffset > xOffMax) xOffset = xOffMax;
////
////        int yOffMax = (height << SHEET_TILE_WIDTH_LOG2) - screen.height;
////        if(yOffset > yOffMax) yOffset = yOffMax;
////
////        // Bind screen offset ("cam position") to level boundaries
////        screen.setOffset(xOffset, yOffset);
////
////        for (int yTile = 0; yTile < height; yTile++) {
////            for (int xTile = 0; xTile < width; xTile++) {
////                getTile(xTile,yTile).render(screen, this, xTile << SHEET_TILE_WIDTH_LOG2, yTile << SHEET_TILE_WIDTH_LOG2 );
////            }
////        }
////    }
////    /**
////     * WITH CULLING
////     * @param screen
////     * @param xOffset
////     * @param yOffset
////     */
////    public void renderTilesNoShadow(Screen screen, int xOffset, int yOffset) {
////        // Bounds x
////        if(xOffset < 0) xOffset = 0;
////        int xOffMax = (levelWidth << Gfx.TILE_SIZE_LOG2) - screen.pWidth;
////        if(xOffset > xOffMax) xOffset = xOffMax;
////        // Bounds y
////        if(yOffset < 0) yOffset = 0;
////        int yOffMax = (levelHeight << Gfx.TILE_SIZE_LOG2) - screen.pHeight;
////        if(yOffset > yOffMax) yOffset = yOffMax;
////        // Bind screen offset ("cam position") to level boundaries
////        screen.setOffset(xOffset, yOffset);
////
////        // Culling x
////        int xTileMin = xOffset >> Gfx.TILE_SIZE_LOG2;
////        int xTileMax = ((xOffset + screen.pWidth) >> Gfx.TILE_SIZE_LOG2) + 1;
////        // Culling y
////        int yTileMin = yOffset >> Gfx.TILE_SIZE_LOG2;
////        int yTileMax = ((yOffset + screen.pHeight) >> Gfx.TILE_SIZE_LOG2) + 2;
////
////        // Render visible tiles
////        for (int yTile = yTileMin; yTile < yTileMax; yTile++) {
////            for (int xTile = xTileMin; xTile < xTileMax; xTile++) {
////                getTile(xTile,yTile).render(screen, this, xTile << Gfx.TILE_SIZE_LOG2, yTile << Gfx.TILE_SIZE_LOG2 );
////            }
////        }
////    }
//// </editor-fold>
//
//// <editor-fold defaultstate="collapsed" desc="old 3">
////    /**
////     *
////     * @param ttx tile (to see) tileIndexX
////     * @param tty tile (to see) tileIndexY
////     * @param ppx player pixel index x
////     * @param ppy player pixel index y
////     * @return
////     */
////    public boolean tileVisibleFromPixel(int ttx, int tty, int ppx, int ppy) {
////
//////        System.out.println("\nttx: " + ttx);
//////        System.out.println("tty: " + tty);
////
////        // player tile index
//////        int ptx = ppx >> Gfx.TILE_SIZE_LOG2;
//////        int pty = ppy >> Gfx.TILE_SIZE_LOG2;
////        int ptx = ppx / Gfx.TILE_SIZE;
////        int pty = ppy / Gfx.TILE_SIZE;
//////        System.out.println("ptx: " + ptx);
//////        System.out.println("pty: " + pty);
//////        System.out.println("Is visible from " + ptx + ", " + pty + "?");
////
////        // delta tile index
////        int dtx = ttx - ptx;
////        int dty = tty - pty;
//////        System.out.println("dtx: " + dtx);
//////        System.out.println("dty: " + dty);
////
//////        // to min/max
//////        int maxdt = Math.max(dtx, dty) -1;
//////        int mindt = Math.min(dtx, dty) -1;
//////        for (int i = 1; i < maxdt; i++) {
//////            int j = (int)Math.round(1.0 * i * mindt / maxdt);
//////            if(getTile(i + ptx, j + pty).isSolid())
//////                return false;
//////        }
////
//////        float fx, fy;
//////        if(Math.abs(dtx) > Math.abs(dty)) {
//////            fx = 1.0f;
//////            fy = (float)dty / Math.abs(dtx);
//////        }
//////        else {
//////            fx = (float)dtx / Math.abs(dty);
//////            fy = 1.0f;
//////        }
////
////        int max = Math.max(Math.abs(dtx), Math.abs(dty));
////        for (int i = 1; i < max - 1; i++) {
////            int tx = ptx + i * dtx / max;
////            int ty = pty + i * dty / max;
////            if (getTile(tx, ty).isSolid()) {
////                return false;
////
////            }
////        }
////
////        return true;
////    }
//// </editor-fold>
//
//// <editor-fold defaultstate="collapsed" desc="old 2">
////    public static int[] rewrap(int[] pixels, int innerRadius, int outerRadius, int newWidth) {
////        // new array
////        int pWidth = Gfx.IMAGE_PIXEL_WIDTH;
////        int pHeight = 2 * ( outerRadius - innerRadius );
////        int size = pWidth * pHeight;
////        int[] convertedPixels = new int[size];
////
////        // Get the center point of the image //
////        int xc = pWidth / 2;
////        int yc = Gfx.IMAGE_PIXEL_HEIGHT / 2;
////
//////        for (int theta = 0; theta < pWidth; theta++) {
//////            for (int r = innerRadius; r < outerRadius; r++) {
//////                double tr = Math.toRadians((theta * 1.0) / pWidth * 360);
//////                int x = (int) Math.round(xc + r * Math.cos(tr));
//////                int y = (int) Math.round(yc + r * Math.sin(tr));
//////                convertedPixels[x + y * pWidth] = pixels[theta + (r-innerRadius) * pWidth];
//////            }
//////        }
////
////        // Do the conversion //
////        for (int r = innerRadius; r < outerRadius/2; r++) {
////            // t = theta in degrees
////            for (int t = 0; t < pWidth/2; t++) {
////                // theta in radians
////                double tr = Math.toRadians((t * 1.0) / pWidth * 360);
////                int x = (int) Math.round(xc + r * Math.cos(tr));
////                int y = (int) Math.round(yc + r * Math.sin(tr));
////
////                int dr = r - innerRadius;
////                int h = pWidth * (pHeight - dr - 1);
////
//////                convertedPixels[h + t] = pixels[pWidth * y + x];
////                pixels[pWidth * y + x] = convertedPixels[h + t];
////            }
////        }
////        return convertedPixels;
////    }
////    public static int[] unwrap(int[] pixels, int innerRadius, int outerRadius, boolean toPolar) {
////
////        // new array
////        int pWidth = Gfx.IMAGE_PIXEL_WIDTH;
////        int pHeight = outerRadius - innerRadius;
////        int size = pWidth * pHeight;
////        int[] convertedPixels = new int[size];
////
////        // Get the center point of the image //
////        int xc = pWidth / 2;
////        int yc = Gfx.IMAGE_PIXEL_HEIGHT / 2;
////
////        // Do the conversion //
////        for (int r = innerRadius; r < outerRadius; r++) {
////            // t = theta in degrees
////            for (int t = 0; t < pWidth; t++) {
////                // theta in radians
////                double tr = Math.toRadians((t * 1.0) / pWidth * 360);
////                int x = (int) Math.round(xc + r * Math.cos(tr));
////                int y = (int) Math.round(yc + r * Math.sin(tr));
////
////                int rDiff = r - innerRadius;
////                int h = pWidth * (pHeight - (rDiff + 1));
////
////                if(toPolar) {
////                    convertedPixels[h + t] = pixels[pWidth * y + x];
////                }
////                else {
////                    pix
////                }
////            }
////        }
////        return convertedPixels;
////    }
////    public static BufferedImage convertPolarImageToCartesian(BufferedImage image, int innerRadius, int outerRadius, int newWidth) {
////
////        int imgWidth = image.getWidth(null);
////        int imgHeight = image.getHeight(null);
////
////
////        // Get this pixels from the original image //
////        int[] packedPixels = new int[imgWidth * imgHeight];
////        PixelGrabber pixelgrabber = new PixelGrabber(image, 0, 0, imgWidth,
////                imgHeight, packedPixels, 0, imgWidth);
////        try {
////            pixelgrabber.grabPixels();
////        } catch (InterruptedException e) {
////            throw new RuntimeException();
////        }
////
////        int width = newWidth;
////        int height = outerRadius - innerRadius;
////        int size = width * height;
////        int[] pixelData = new int[size];
////
////        // Get the center point of the image //
////        int xc = imgWidth / 2;
////        int yc = imgHeight / 2;
////
////        // Do the conversion //
////        for (int r = innerRadius; r < outerRadius; r++) {
////            // t = theta in degrees
////            for (int t = 0; t < width; t++) {
////                // theta in radians
////                double tr = Math.toRadians((t * 1.0) / width * 360);
////                int x = (int) Math.round(xc + r * Math.cos(tr));
////                int y = (int) Math.round(yc + r * Math.sin(tr));
////
////                int rDiff = r - innerRadius;
////                int h = width * (height - (rDiff + 1));
////
////                /*
////                 * THROWS SHIT
////                 */
////                pixelData[h + t] = packedPixels[imgWidth * y + x];
////
////            }
////        }
////
////        DataBuffer dataBuf = new DataBufferInt(pixelData, size);
////
////        ColorModel colorModel = new DirectColorModel(32, 0x00ff0000, 0x0000ff00, 0x000000ff);
////        int[] masks = {0x00ff0000, 0x0000ff00, 0x000000ff};
////        WritableRaster raster = Raster.createPackedRaster(dataBuf, width, height, width, masks, null);
////
////        BufferedImage convertedImage = new BufferedImage(colorModel, raster, false, null);
////
////        return convertedImage;
////
////    }
//// </editor-fold>
//
//// <editor-fold defaultstate="collapsed" desc="old">
//
////    public void renderTiles(Screen screen, int xOffset, int yOffset) {
////        if(xOffset < 0) xOffset = 0;
////        if(xOffset > width << Screen.TILE_WIDTH_LOG2 - screen.width)
////            xOffset = width << Screen.TILE_WIDTH_LOG2 - screen.width;
////
////        if(yOffset < 0) yOffset = 0;
////        if(yOffset > height << Screen.TILE_WIDTH_LOG2 - screen.height)
////            xOffset = height << Screen.TILE_WIDTH_LOG2 - screen.height;
////
////        screen.setOffset(xOffset, yOffset);
////
////        for (int yTile = 0; yTile < height; yTile++) {
////            for (int xTile = 0; xTile < width; xTile++) {
////                getTile(xTile, yTile).render(screen, this, xTile << 3, yTile << 3 );
////            }
////        }
////    }
//
//// </editor-fold>
//
