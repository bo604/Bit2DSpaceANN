/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.level;

import bit2dspaceann.gfx.ColorsLong;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.level.tiles.BasicSolidTile;
import bit2dspaceann.level.tiles.BasicTile;

/**
 *
 * hm, has to be rethought?
 *
 * tile id's not unique... how to e.g. assign a planet as tile?
 * also: do i want to assing planets as tiles????
 *
 *
 * @author Bo
 */
public abstract class Tile {

    private static final int nTiles = 256;
    public static final Tile[] tiles = new Tile[nTiles];
    
    public static final Tile VOID = new BasicSolidTile(0, 0, 2, ColorsLong.get(000, 000, -1, -1, -1, -1, -1, -1), 0xFFFF00FF);
    public static final Tile SPACE = new BasicTile(1, 0, 2, ColorsLong.get(-1, -1, -1, -1, -1, -1, -1, -1), 0xFF000000);

    public static final Tile ASTEROID = new BasicTile(2, 0, 3, ColorsLong.get(-1, 431, 320, 555, 555, 555, 555, 210), 0xFF806000);

    public static final Tile STONE = new BasicSolidTile(3, 5, 2, ColorsLong.get(-1, 111, 222, 333, 444, 333, 222, 111), 0xFF555555);
    
    public static final Tile GRASS = new BasicTile(4, 4, 2, ColorsLong.get(-1, 130, 031, 141, 130, 140, 150, 250), 0xFF00FF00);
//    public static final Tile GRASS1 = new BasicTile(3, 4 + 1, 2, ColorsLong.get(-1, 130, 110, 120, 130, 140, 150, 250), 0xFF00FF00);
//    public static final Tile GRASS2 = new BasicTile(4, 4 + 2, 2, ColorsLong.get(-1, 130, 110, 120, 130, 140, 150, 250), 0xFF00FF00);
//    public static final Tile GRASS3 = new BasicTile(5, 4 + 3, 2, ColorsLong.get(-1, 130, 110, 120, 130, 140, 150, 250), 0xFF00FF00);

    public static final Tile WATER = new BasicSolidTile(5, 0, 2, ColorsLong.get(004, -1, 115, -1, -1, -1, -1, -1), 0xFF0000FF);
    public static final Tile ICE_WATER = new BasicTile(6, 8, 2, ColorsLong.get(-1, 115, 445, -1, -1, -1, -1, -1), 0xFFAAAAFF);
    
//    public static final Tile WOOD = new BasicTile(6, 7, 2, ColorsLong.get(-1, 431, 220, -1, -1, -1, -1, -1), 0xFF806000);

//    public static final Tile BRICKS = new BasicTile(6, 6, 2, ColorsLong.get(000, 222, 333, 444, -1, -1, -1, -1), 0xFFAAAAAA);
    public static final Tile CRAZY = new BasicTile(7, 11, 2, ColorsLong.TEST_COLOR, 0xFFAAAAAA);

//    public static final Tile WATER = new AnimatedTile(3, new int[][]{{0,5},{1,5},{2,5},{3,5}},
//            ColorsLong.get(-1, 004, 115, -1), 0xFF0000FF, 500);

//    public static final Tile TECHWALL = new Basic3DishTile(7, 1, 6, ColorsLong.get(-1, 111, 222, 333, 444, 333, 222, 111), 0xFF123456);
//    public static final Tile METALWALL = new Basic3DishTile(8, 2, 6, ColorsLong.get(-1, 111, 222, 333, 444, 333, 222, 111), 0xFF00FFFF);




    protected static byte sheetIndex = 0;
    protected byte id;
    protected boolean solid;
    protected boolean emitter;

    private int mapColor;


    public Tile(int id, boolean solid, boolean emitter, int mapColor) {
        this.id = (byte)id;
        if(tiles[id]!=null) throw new RuntimeException("Duplicate tile id on " + id);
        this.solid = solid;
        this.emitter = emitter;
        this.mapColor = mapColor;
        tiles[id] = this;
    }

    public byte getID() {
        return id;
    }

    public boolean isSolid() {
        return solid;
    }

    public boolean isEmitter() {
        return emitter;
    }

    public int getMapColor() {
        return mapColor;
    }

    public static void setSheetIndex(byte sheetIndex) {
        Tile.sheetIndex = sheetIndex;
    }

    public abstract void tick();
    public abstract void render(Screen screen, Level level, int x, int y);


//    public abstract void render(Screen screen, Level level, int x, int y, float alpha);
//    public abstract void renderHighShadow(Screen screen, Level level, int x, int y, PixelVisibilityChecker visChecker);

}


// <editor-fold defaultstate="collapsed" desc="old">
//    public static final Tile VOID = new BasicSolidTile(0, 0, 0, ColorsLong.get(000, -1, -1, -1), 0xFF000000);
//
////    public static final Tile STONE = new BasicSolidTile(1, 1, 0, Colors.get(-1, 333, 222, -1), 0xFF555555);
//    public static final Tile STONE = new BasicSolidTile(1, 1, 0, ColorsLong.get(-1, 333, 222, 131), 0xFF555555);
//
//    public static final Tile GRASS = new BasicTile(2, 2, 0, ColorsLong.get(-1, 130, 140, -1), 0xFF00FF00);
//    public static final Tile WATER = new AnimatedTile(3, new int[][]{{0,5},{1,5},{2,5},{3,5}},
//            ColorsLong.get(-1, 004, 115, -1), 0xFF0000FF, 500);
//
//    public static final Tile GOAL = new AnimatedTile(4,
////            new int[][]{{0,6},{1,6},{2,6},{3,6},{4,6},{5,6},{6,6},{7,6},{8,6},{9,6},{10,6},{11,6}},
//            new int[][]{{1,6},{2,6},{3,6},{4,6},{5,6},{6,6},{7,6},{8,6},{9,6},{10,6},{11,6}},
//            ColorsLong.get(102, 203, 215, 325), 0xFFFF0000, 150);
// </editor-fold>

