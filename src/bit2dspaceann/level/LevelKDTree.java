//package bit2dspaceann.level;
//
//import bit2dspaceann.entities.Entity;
//import bit2dspaceann.gfx.Gfx;
//import bit2dspaceann.gfx.Screen;
//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.LinkedList;
//import java.util.List;
//import java.util.concurrent.Callable;
//import java.util.concurrent.Future;
//import java.util.concurrent.ScheduledThreadPoolExecutor;
//import net.sf.javaml.core.kdtree.KDTree;
//
///**
// *
// * @author Bo
// */
//public class LevelKDTree {
//
//    public List<Entity> entities = new ArrayList<Entity>();
//    private KDTree tree;
//
//    public LevelKDTree() {
//        
//    }
//    
//
//    public boolean contains(float x, float y) {
//        if(x<0 || x>=Gfx.IMAGE_PIXEL_WIDTH) return false;
//        if(y<0 || y>=Gfx.IMAGE_PIXEL_HEIGHT) return false;
//        return true;
//    }
//
//    public void addEntity(Entity entity) {
//        entities.add(entity);
////        insert(entity.getKDKey(), entity);
//    }
//    
//    private final ScheduledThreadPoolExecutor exec = new ScheduledThreadPoolExecutor(6);
//    private final LinkedList<Future<Object>> futures = new LinkedList<Future<Object>>();
//
//    public void tick() {
//        // remove entities flagged for removal
//        for (Iterator<Entity> it = entities.iterator(); it.hasNext();) {
//            Entity entity = it.next();
//            if(entity.flaggedForRemoval) {
//                it.remove();
//            }
//        }
//        
//        // rebuild kd-tree
//        rebuildTree();
//        
//        // sumbit entity ticks to executor
//        for (Iterator<Entity> it = entities.iterator(); it.hasNext();) {
//            Entity entity = it.next();
//            futures.add(exec.submit(new EntityTicker(entity)));
//        }
//        
//        // wait for futures to finish
//        boolean isDone = false;
//        while(!isDone) {
//            for (Iterator<Future<Object>> it = futures.iterator(); it.hasNext();) {
//                Future<Object> future = it.next();
//                if(future.isDone()) it.remove();
//            }
//            if(futures.isEmpty()) isDone = true;
//        }
//        
//    }
//
//    private void rebuildTree() {
//        tree = new KDTree(2);
//        for (Entity entity : entities) {
//            tree.insert(entity.getKDKey(), entity);
//        }
//    }
//    
//    public Entity nearestEntityTo(Entity entity) {
//        return (Entity)tree.nearest(entity.getKDKey());
//    }
//    
//    public Entity nearestEntityTo(float x, float y) {
//        return (Entity)tree.nearest(new double[]{x,y});
//    }
//    
//    private class EntityTicker implements Callable<Object> {
//        final Entity entity;
//
//        public EntityTicker(Entity entity) {
//            this.entity = entity;
//        }
//        
//        public Object call() throws Exception {
//            entity.tick();
//            return null;
//        }
//        
//    }
//
//    public void renderEntities(Screen screen) {
//        for (Entity entity : entities) {
//                entity.render(screen);
//        }
//    }
//    
//}