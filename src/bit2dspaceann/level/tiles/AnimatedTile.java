/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.level.tiles;

import bit2dspaceann.gfx.Gfx;

/**
 *
 * @author Bo
 */
public class AnimatedTile extends BasicTile {


    /*
     * AnimationCoords:
     *
     * [ x1 ] [ y1 ]
     * [ x2 ] [ y2 ]
     * [ x3 ] [ y3 ]
     * [ x4 ] [ y4 ]
     *
     * where xi, yi is the index of the ith animation tile
     * on the sprite sheet (counting in number of sprite sheet tiles)
     */
    private int[][] animationCoords;

    private int currentAnimationIndex;
    
    private int animationDelay;     // time between tile switches in ms
    private long nextAnimationTime;


    public AnimatedTile(int id, int[][] animationCoords, long tileColor, int mapColor, int animationDelay) {
        super(id, animationCoords[0][0], animationCoords[0][1], tileColor, mapColor);
        this.animationCoords = animationCoords;
        this.currentAnimationIndex = 0;
        this.animationDelay = animationDelay;
        this.nextAnimationTime = System.currentTimeMillis() + animationDelay;
    }

    @Override
    public void tick() {
        if(System.currentTimeMillis() >= nextAnimationTime) {
            nextAnimationTime = System.currentTimeMillis() + animationDelay;
            updateTileIndex();
        }
    }


    private void updateTileIndex() {
        currentAnimationIndex = (currentAnimationIndex+1) % animationCoords.length;
        tileIndex = animationCoords[currentAnimationIndex][0]
                  + animationCoords[currentAnimationIndex][1] * Gfx.NUM_TILES_IN_SHEET_ROWS;
    }



}
