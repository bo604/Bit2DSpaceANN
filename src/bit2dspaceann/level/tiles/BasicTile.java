/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.level.tiles;

import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.level.Level;
import bit2dspaceann.level.Tile;

/**
 *
 * @author Bo
 */
public class BasicTile extends Tile {

    protected int tileIndex;
    protected long tileColor;

    public BasicTile(int id, int x, int y, long tileColor, int mapColor) {
        super(id,false,false,mapColor);
        tileIndex = x + y * Gfx.NUM_TILES_IN_SHEET_ROWS;
        this.tileColor = tileColor;
    }
    
    public void render(Screen screen, Level level, int x, int y) {
//        screen.render(x, y, sheetIndex, tileIndex, tileColor, 1, Gfx.BIT_MIRROR_NONE);
        screen.render(x, y, sheetIndex, tileIndex, tileColor);
    }

//    public void render(Screen screen, Level level, int x, int y, float alpha) {
//        screen.renderWithLight(x, y, sheetIndex, tileIndex, tileColor, alpha, 1, Gfx.BIT_MIRROR_NONE);
//    }

//    public void renderHighShadow(Screen screen, Level level, int x, int y, PixelVisibilityChecker visChecker) {
//        screen.render(x, y, sheetIndex, tileIndex, tileColor, 1, Gfx.BIT_MIRROR_NONE);
//        screen.render(x, y, sheetIndex, tileIndex, tileColor);
//        screen.renderVisible(x, y, sheetIndex, tileIndex, tileColor, 1, Gfx.BIT_MIRROR_NONE, visChecker);
//    }



    public void tick() {}
   

}
