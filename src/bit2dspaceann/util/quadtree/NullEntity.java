/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.util.quadtree;

import bit2dspaceann.entities.Entity;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;
import java.util.Random;

/**
 *
 * @author Bo
 */
public class NullEntity extends Entity {

    private static final Random rnd = new Random();
    
    public NullEntity() {
        xWorld = Gfx.IMAGE_PIXEL_WIDTH * rnd.nextFloat();
        yWorld = Gfx.IMAGE_PIXEL_HEIGHT * rnd.nextFloat();
    }

    @Override
    public void tick() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void render(Screen screen) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
