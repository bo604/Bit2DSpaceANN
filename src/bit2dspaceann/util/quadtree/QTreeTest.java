/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.util.quadtree;

import bit2dspaceann.entities.Entity;

/**
 *
 * @author Bo
 */
public class QTreeTest {
    
    public static void main(String[] args) {
        
        QTree tree = new QTree();
        
        for (int i = 0; i < 23; i++) {
            Entity ent = new NullEntity();
            tree.insert(ent);
        }
        
        tree.printStructure();
        
        System.out.println("--- Split ---");
        tree.split();
        
        tree.printStructure();
        
    }
    
}
