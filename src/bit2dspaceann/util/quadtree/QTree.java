/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.util.quadtree;

import bit2dspaceann.entities.Entity;
import bit2dspaceann.gfx.Gfx;

/**
 *
 * @author Bo
 */
public class QTree {
    
    public static final int MAX_ENTRIES = 5;
    
    public static final int UP_RIGHT = 0;
    public static final int UP_LEFT = 1;
    public static final int DOWN_LEFT = 2;
    public static final int DOWN_RIGHT = 3;
    
    public static final int MOD_X[] = new int[] {1,-1,-1,1};
    public static final int MOD_Y[] = new int[] {1,1,-1,-1};
    
    QTreeNode root;

    public QTree() {
        root = new QTreeNode(Gfx.IMAGE_PIXEL_WIDTH>>1, Gfx.IMAGE_PIXEL_HEIGHT>>1,
                             Gfx.IMAGE_PIXEL_WIDTH,    Gfx.IMAGE_PIXEL_HEIGHT);
    }
    
    
    public void insert(Entity entity) {
        root.insert(entity);
    }
    
    
    public void printStructure() {
        System.out.println("QTree Structure:");
        root.printStructure(0);
    }
    
    public void split() {
        root.split();
    }
    
    
    
}
