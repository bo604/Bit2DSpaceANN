/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.util.quadtree;

import bit2dspaceann.entities.Entity;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Bo
 */
public class QTreeNode {
    
    /*
     * center x,y and w,h of the node
     * 
     *          BAAAAAAAA
     * 
     * does not work with x,y as center.....
     * -> e.g. rect.contain() will fail!
     * 
     */
    public CenterBounds rect;
    
    public List<Entity> entities = new ArrayList<Entity>();
    
    public QTreeNode[] children = null;
    
    
    public QTreeNode(CenterBounds rect) {
        this.rect = rect;
    }

    public QTreeNode(int x, int y, int w, int h) {
        this(new CenterBounds(x, y, w, h));
    }
    
    private QTreeNode childAt(float x, float y) {
        return x >= rect.x ?
               y >= rect.y ? children[QTree.UP_RIGHT] : children[QTree.DOWN_RIGHT]
             : y >= rect.y ? children[QTree.UP_LEFT] : children[QTree.DOWN_LEFT];
    }
        
    
    
    public void insert(Entity entity) {
        
        if(children!=null) {
            childAt(entity.xWorld, entity.yWorld).insert(entity);
        }
        else {
            entities.add(entity);
        }
        
//        if(entities.size() > QTree.MAX_ENTRIES) {
//            split();
//        }
        
    }
    
    
//    private void pushDown() {
//        for (Iterator<Entity> it = entities.iterator(); it.hasNext();) {
//            Entity entity = it.next();
//            this.insert(entity);
//            it.remove();
//        }
//        entities = null;
//    }
    
    public void split() {
        if(entities.size() < QTree.MAX_ENTRIES) return;
        
        children = new QTreeNode[4];
        for (int i = 0; i < 4; i++) {
            children[i] = new QTreeNode( rect.getQuarter(i) );
        }
        
        List<Entity> tmp = new LinkedList<Entity>(entities);
        entities.clear();
        for (Entity entity : tmp) {
            childAt(entity.xWorld, entity.yWorld).insert(entity);
        }
        
        // ??????
        
        // ... hier nicht fertig! recursive split????
        
        
        for (QTreeNode child : children) {
            split();
        }
        
    }
    
    
    
    public void printStructure(int lvl) {

        System.out.println("--- Node lvl: " + lvl);
        if(children==null) {
            System.out.println("No children. Entities: " + entities.size());
        }
        else {
            System.out.println("Children:");
            for (QTreeNode child : children) {
                child.printStructure(lvl+1);
            }
        }
        
    }
    
    
}



//        if(x >= rect.x) {
//            if(y >= rect.y) {
//                return children[QTree.UP_RIGHT];
//            }
//            else {
//                return children[QTree.DOWN_RIGHT];
//            }
//        }
//        else {
//            if(y >= rect.y) {
//                return children[QTree.UP_LEFT];
//            }
//            else {
//                return children[QTree.DOWN_LEFT];
//            }
//        }
//    }
