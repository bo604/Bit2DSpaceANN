/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.util.quadtree;

/**
 *
 * @author Bo
 */
public class CenterBounds {
    
    public int x, y, w, h;

    public CenterBounds(int x, int y, int w, int h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    public CenterBounds() {
        this(0,0,0,0);
    }
    
    public boolean contains(float x_, float y_) {
        return (   x_ > x-(w>>1)
                && y_ > y-(h>>1)
                && x_ < x+(w>>1)
                && y_ < y+(h>>1));
    }
    
    
    public CenterBounds getQuarter(int qtreeDirectionIndex) {
        int xr = x + (w>>2) * QTree.MOD_X[qtreeDirectionIndex];
        int yr = y + (h>>2) * QTree.MOD_Y[qtreeDirectionIndex];
        return new CenterBounds(xr, yr, w>>1, h>>1);
    }

}
