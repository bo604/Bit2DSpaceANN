/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.util;

/**
 *
 * @author Bo
 */
public class Vec2f {
    
    public float x, y;

    public Vec2f() {
        x = 0; y = 0;
    }

    public Vec2f(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public Vec2f(Vec2f p) {
        this(p.x,p.y);
    }
    
    
    
    /****************
     * CHAIN MATH (RETURN MODIFIED OLD POINT!)
     */
    
    public Vec2f add(Vec2f p) {
        x += p.x;
        y += p.y;
        return this;
    }
    
    public Vec2f sub(Vec2f p) {
        x -= p.x;
        y -= p.y;
        return this;
    }
    
    public Vec2f rotate(float angle) {
        x = (float)(x * Math.cos(angle) - y * Math.sin(angle));
        y = (float)(x * Math.sin(angle) + y * Math.cos(angle));
        return this;
    }
    
    public float scalarProduct(Vec2f p) {
        return x * p.x + y * p.y;
    }
    
    public float norm() {
        return (float)Math.sqrt(x*x + y*y);
    }
    
    public void set(float x, float y) {
        this.x = x;
        this.y = y;
    }
    
    /*************
     *  STATICS (MAKE NEW POINT!)
     */
    
    public static Vec2f add(Vec2f a, Vec2f b) {
        return new Vec2f(a).add(b);
    }
    
    public static Vec2f sub(Vec2f a, Vec2f b) {
        return new Vec2f(a).sub(b);
    }
    
    public static Vec2f rotate(Vec2f a, float angle) {
        return new Vec2f(a).rotate(angle);
    }
    
    public static float scalarProduct(Vec2f a, Vec2f b) {
        return a.x * b.x + a.y * b.y;
    }
    
}
