/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.util;

import bit2dspaceann.util.quadtree.QTree;

/**
 *
 * @author Bo
 */
public class Rect2Di {
    
    public int x, y, w, h;

    public Rect2Di(int x, int y, int w, int h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    public Rect2Di() {
        this(0,0,0,0);
    }
    
    public boolean contains(float x_, float y_) {
        return (x_>x && y_>y && x_<x+w && y_<y+h);
    }
    
    
    public Rect2Di getQuarter(int qtreeDirectionIndex) {
        int xr = x + (w>>2) * QTree.MOD_X[qtreeDirectionIndex];
        int yr = y + (h>>2) * QTree.MOD_Y[qtreeDirectionIndex];
        return new Rect2Di(xr, yr, w>>1, h>>1);
    }
        
//        Rect2Di result = new Rect2Di(0, 0, w>>1, h>>1);
//        if(qtreeDirectionIndex == QTree.UP_RIGHT) {
//            result.x = x+(w>>2);
//            result.y = y+(h>>2);
//            return new Rect2Di(x+(w>>2), y+(h>>2), w>>1, h>>1);
//        }
//        else if(qtreeDirectionIndex == QTree.UP_LEFT) {
//            return new Rect2Di(x+(w>>2), y-(h>>2), w>>1, h>>1);
//        }
//        else if(qtreeDirectionIndex == QTree.DOWN_LEFT) {
//            
//        }
//        else if(qtreeDirectionIndex == QTree.DOWN_RIGHT) {
//            
//        }
//        throw new IllegalArgumentException("qtreeDirIndex must be < 4!!!");
//    }
    
    
}
