/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.util;

/**
 *
 * @author Bo
 */
public class Interval2D<Key extends Comparable<Key>> {
    
    public Interval<Key> intervalX;
    public Interval<Key> intervalY;

    public Interval2D(Interval intervalX, Interval intervalY) {
        this.intervalX = intervalX;
        this.intervalY = intervalY;
    }
    
    public boolean contains(Key x, Key y) {
        return intervalX.contains(x) && intervalY.contains(y);
    }
    
}
