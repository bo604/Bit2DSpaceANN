/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.util;

/**
 *
 * @author Bo
 */
public class ArrayMath {
    
    /**
     * 
     * @param a
     * @param b
     * @param factor 0 = 100% a, 1 = 100% b
     * @return 
     */
    public static float[] mix(float[] a, float[] b, float factor) {
        float[] result = new float[3];
        for (int i = 0; i < 3; i++) {
            result[i] = a[i] + factor * b[i]-a[i];
        }
        return result;
    }

    public static void addInternal(float[] dest, float[] src) {
        int n = dest.length;
        for (int i = 0; i < n; i++) {
            dest[i] += src[i];
        }
    }
    
    public static void divInternal(float[] dest, float a) {
        int n = dest.length;
        for (int i = 0; i < n; i++) {
            dest[i] /= a;
        }
    }
    
    public static void multInternal(float[] dest, float a) {
        int n = dest.length;
        for (int i = 0; i < n; i++) {
            dest[i] *= a;
        }
    }
    
    public static float[] rotate(float[] dr, float angle) {
        return new float[] {
            (float)(dr[0] * Math.cos(angle) - dr[1] * Math.sin(angle)),
            (float)(dr[0] * Math.sin(angle) + dr[1] * Math.cos(angle))
        };
    }
    
}
