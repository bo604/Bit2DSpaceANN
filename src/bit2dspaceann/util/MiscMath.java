/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.util;

/**
 *
 * @author Bo
 */
public class MiscMath {

    public static float sigmoid(float x) {
        return (float)(1.0/( 1.0+Math.exp(-x)));
    }
    
    /**
     * function that
     * decays from 1 to 0
     * over the interval of dist from 0 to maxDist
     * and is equal to 0 for dist >= maxDist
     * 
     * @param dist
     * @param maxDist
     * @return 
     */
    public static float cosDecay(float dist, float maxDist) {
        if(dist>=maxDist) return 0;
        float result = .5f + .5f * (float)Math.cos(Math.PI * dist/maxDist);
        return result;
    }
    
    /**
     * exponential decay
     * @param x fakjefhkaef
     * @param decay faef
     * @param min gaeg
     * @param max aeg
     * @return  gskregjslrgj
     */
    public static float expDecay(float x, float decay, float min, float max) {
        return min + (max - min) * (float)Math.exp(-decay*x);
    }
    
}
