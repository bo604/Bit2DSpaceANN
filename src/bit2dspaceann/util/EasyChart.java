///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//
//package bit2dspaceann.util;
//
//import java.awt.Color;
//import org.jfree.chart.ChartFactory;
//import org.jfree.chart.ChartFrame;
//import org.jfree.chart.JFreeChart;
//import org.jfree.chart.plot.Marker;
//import org.jfree.chart.plot.PlotOrientation;
//import org.jfree.chart.plot.ValueMarker;
//import org.jfree.data.xy.DefaultXYDataset;
////import java.awt.
//
///**
// *
// * @author Bo
// */
//public class EasyChart {
//
//    private ChartFrame frame = null; // new ChartFrame("ChartViewer",null);
//
//    public EasyChart() {
//        frame = new ChartFrame("EasyChart",null);
//    }
//
//
//
//
//
////    public static void viewNeuralData(NeuralData data) {
//////        double[][] dataArray = new double[2][data.size()];
//////        for (int i = 0; i < data.size(); i++) {
//////            dataArray[0][i] = i;
//////            dataArray[1][i] = data.getData()[i];
//////        }
//////        DefaultXYDataset dataSet = new DefaultXYDataset();
//////        dataSet.addSeries("NeuralData", dataArray);
//////        JFreeChart xyPlotChart = ChartFactory.createXYLineChart("NeuralData", "x", "Data", dataSet, PlotOrientation.VERTICAL, true, true, false);
//////        getFrame().getChartPanel().setChart(xyPlotChart);
//////        getFrame().pack();
//////        getFrame().setVisible(true);
////        viewDoubleArray( data.getData() );
////    }
//
//
//    public void viewDoubleArray(double[] data) {
//        double[][] dataArray = new double[2][data.length];
//        for (int i = 0; i < data.length; i++) {
//            dataArray[0][i] = i;
//            dataArray[1][i] = data[i];
//        }
//        viewDoubleArray(dataArray);
//    }
//
//    public void viewDoubleArray(Double[] data) {
//        double[][] dataArray = new double[2][data.length];
//        for (int i = 0; i < data.length; i++) {
//            dataArray[0][i] = i;
//            dataArray[1][i] = data[i];
//        }
//        viewDoubleArray(dataArray);
//    }
//
////    public static <T extends Number> void viewArray(T[] data) {
////        double[][] dataArray = new double[2][data.length];
////        for (int i = 0; i < data.length; i++) {
////            dataArray[0][i] = i;
////            dataArray[1][i] = data[i].doubleValue();
////        }
////        viewDoubleArray(dataArray);
////    }
//
//
//
//    public void viewDoubleArray(double[][] data) {
//        DefaultXYDataset dataSet = new DefaultXYDataset();
//        dataSet.addSeries("Data", data);
//        JFreeChart xyPlotChart = ChartFactory.createXYLineChart
//                ("Data", "x", "Data", dataSet, PlotOrientation.VERTICAL, true, true, false);
//
//        //xyPlotChart.getXYPlot().setRenderer(new XYDotRenderer());
//        //XYShapeRenderer rend = new XYShapeRenderer();
//        //rend.setBaseShape(new Shape());
//
//        //xyPlotChart.getXYPlot().setRenderer(new XYShapeRenderer());
//        
//        frame.getChartPanel().setChart(xyPlotChart);
//        frame.pack();
//        if(!frame.isVisible())
//            frame.setVisible(true);
//    }
//
//
//
//
////    public void view3DDoubleArray(double[][] data) {
////
////        DefaultXYZDataset dataSet = new DefaultXYZDataset();
////        dataSet.addSeries("Data", data);
////
////        JFreeChart xyzPlot = ChartFactory.createBubbleChart("title", "xlab", "ylab", dataSet, PlotOrientation.VERTICAL, true, true, false);
////
////        //JFreeChart chart = ChartFactory.createx
////        //Renderer renderer = new XYBlockRenderer();
////
////        PaintScale scale = new GrayPaintScale(0, 200);
////
////
////        Point2D start = new Point2D.Float(50, 50);
////        Point2D end = new Point2D.Float(200, 200);
////        //float[] dist = {0.0f, 0.2f, 1.0f};
////        //Color[] colors = {Color.RED, Color.WHITE, Color.BLUE};
////
////        GradientPaint p = new GradientPaint(start, Color.blue, end, Color.red, true);
////        //Paint
////
//////        LinearGradientPaint p =
//////                new LinearGradientPaint(start, end, dist, colors);
////
////        PaintScale lscale = new LookupPaintScale(0, 200, p);
////
////
////        XYBlockRenderer rend = new XYBlockRenderer();
////        rend.setPaintScale(scale);
////        rend.setBlockHeight(0.02);
////        rend.setBlockWidth(0.02);
////        xyzPlot.getXYPlot().setRenderer(rend);
////
////
////        //xyzPlot.getXYPlot().setRenderer(new XYBlockRenderer());
////
////        //xyzPlot.getPlot().
////        //XYZPlot plot = (XYZPlot) xyzPlot.getPlot();
////
////        frame.getChartPanel().setChart(xyzPlot);
////        frame.pack();
////        if(!frame.isVisible())
////            frame.setVisible(true);
////
////    }
//
//
//
//
////    public static <T extends Number> void createChartFrame(T[] data) {
////        double[][] dataArray = new double[2][data.length];
////        for (int i = 0; i < data.length; i++) {
////            dataArray[0][i] = i;
////            dataArray[1][i] = data[i].doubleValue();
////        }
////        createChartFrame(dataArray);
////    }
//
//
////    public static ChartFrame createChartFrame(double[] data) {
////        double[][] dataArray = new double[2][data.length];
////        for (int i = 0; i < data.length; i++) {
////            dataArray[0][i] = i;
////            dataArray[1][i] = data[i];
////        }
////        return createChartFrame(dataArray);
////    }
////
////    public static ChartFrame createChartFrame(double[][] data) {
////        DefaultXYDataset dataSet = new DefaultXYDataset();
////        dataSet.addSeries("Data", data);
////        JFreeChart xyPlotChart = ChartFactory.createXYLineChart("Data", "x", "Data", dataSet, PlotOrientation.VERTICAL, true, true, false);
////        ChartFrame newFrame = new ChartFrame("ChartViewer",null);
////        newFrame.getChartPanel().setChart(xyPlotChart);
////        newFrame.pack();
////        newFrame.setVisible(true);
////        return newFrame;
////    }
//
//
//
//    public void addRangeMarker(ChartFrame frame, double value) {
//        addRangeMarker(frame,value,Color.BLUE);
//    }
//
//    public void addRangeMarker(ChartFrame frame, double value, Color color) {
//        Marker marker = new ValueMarker(value);
//        marker.setPaint(color);
//        frame.getChartPanel().getChart().getXYPlot().addRangeMarker(marker);
//    }
//    
//    public void addDomainMarker(ChartFrame frame, double value) {
//        addDomainMarker(frame, value, Color.BLUE);
//    }
//
//    public void addDomainMarker(ChartFrame frame, double value, Color color) {
//        Marker marker = new ValueMarker(value);
//        marker.setPaint(color);
//        frame.getChartPanel().getChart().getXYPlot().addDomainMarker(marker);
//    }
//
//    public void addDomainMarker(ChartFrame frame, double value, Color color, String label) {
//        Marker marker = new ValueMarker(value);
//        marker.setPaint(color);
//        marker.setLabel(label);
//        frame.getChartPanel().getChart().getXYPlot().addDomainMarker(marker);
//    }
//
//
//
////    public static void addAverageAndDeviation(ChartFrame frame) {
////        double avg = ArrayMath.getAverage( frame.getChartPanel().getChart().getXYPlot() )
////    }
//
////    public static ChartFrame createChartFrameWithAverageAndDeviation(double[] data) {
////
////        double avg = ArrayMath.getAverage(data);
////        double std = ArrayMath.getStandardDeviation(data);
////
////        double[][] dataArray = new double[2][data.length];
////        for (int i = 0; i < data.length; i++) {
////            dataArray[0][i] = i;
////            dataArray[1][i] = data[i];
////        }
////
////        DefaultXYDataset dataSet = new DefaultXYDataset();
////        dataSet.addSeries("Data", dataArray);
////        JFreeChart xyPlotChart = ChartFactory.createXYLineChart("Data", "x", "Data", dataSet, PlotOrientation.VERTICAL, true, true, false);
////        ChartFrame newFrame = new ChartFrame("ChartViewer",null);
////        newFrame.getChartPanel().setChart(xyPlotChart);
////        newFrame.pack();
////        newFrame.setVisible(true);
////
////        addRangeMarker(newFrame,avg);
////        addRangeMarker(newFrame,avg+std,Color.GREEN);
////        addRangeMarker(newFrame,avg-std,Color.GREEN);
////
////        return newFrame;
////    }
//
//
//    public void setTitle(String title) {
//        frame.setTitle(title);
//    }
//
//
//}
//
//
////    public static ChartFrame createChartFrameTest(double[] data) {
////        double[][] dataArray = new double[2][data.length];
////        for (int i = 0; i < data.length; i++) {
////            dataArray[0][i] = i;
////            dataArray[1][i] = data[i];
////        }
////        return createChartFrameTest(dataArray);
////    }
////    public static ChartFrame createChartFrameTest(double[][] data) {
////        DefaultXYDataset dataSet = new DefaultXYDataset();
////        dataSet.addSeries("Data", data);
////        JFreeChart xyPlotChart = ChartFactory.createXYLineChart("Data", "x", "Data", dataSet, PlotOrientation.VERTICAL, true, true, false);
////        Marker marker = new ValueMarker(0.01);
////        xyPlotChart.getXYPlot().addDomainMarker(marker);
////        xyPlotChart.getXYPlot().addRangeMarker(marker);
////        ChartFrame newFrame = new ChartFrame("ChartViewer",null);
////        newFrame.getChartPanel().setChart(xyPlotChart);
////        newFrame.pack();
////        newFrame.setVisible(true);
////        return newFrame;
////    }
