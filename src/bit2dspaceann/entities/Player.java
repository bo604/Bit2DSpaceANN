///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//
//package bit2dspaceann.entities;
//
//import bit2dspaceann.game.InputHandler;
//import bit2dspaceann.gfx.ColorsLong;
//import bit2dspaceann.gfx.Screen;
//import bit2dspaceann.level.Level;
//
///**
// *
// * @author Bo
// */
//public class Player extends Mob {
//
//    private long color = ColorsLong.GRAY_RED;
//    
//    private InputHandler input = null;
//    
//    private int tickCount = 0;
//
//    private String userName = "Soot";
//
////    public Player(Level level, int x, int y, InputHandler input) {
////        super(level, "Player", x, y, 1);
////        this.input = input;
////    }
////    public Player() {
////        super(level, "Player", x, y, 1);
////        this.input = input;
////        this.userName = userName;
////    }
//
//    
//
////    public boolean init(String userName, InputHandler input, Level level, int x, int y, float speed) {
////        boolean init = super.init("Player", level, x, y, speed);
////        this.userName = userName;
////        this.input = input;
////        return init;
////    }
//
//
//    
////    @Override
////    public boolean hasCollided(int dx, int dy) {
//////        int xMin = 0;
////////        int xMax = 7;
////////        int yMin = 3;
////////        int yMax = 7;
////////        for (int i = xMin; i < xMax; i++) {
////////            if(isSolidTile(dx, dy, i, yMin)) return true;
////////        }
////////        for (int i = xMin; i < xMax; i++) {
////////            if(isSolidTile(dx, dy, i, yMax)) return true;
////////        }
////////        for (int j = yMin; j < yMax; j++) {
////////            if(isSolidTile(dx, dy, xMin, j)) return true;
////////        }
////////        for (int j = yMin; j < yMax; j++) {
////////            if(isSolidTile(dx, dy, xMax, j)) return true;
////////        }
////////        return false;
//////
//////
//////        if( movingDir == Mob.DIR_LEFT && xWorld + dx - (Gfx.IMAGE_PIXEL_WIDTH>>1) < 0 ) return true;
//////        if( movingDir == Mob.DIR_UP && yWorld + dy - (Gfx.IMAGE_PIXEL_HEIGHT>>1) < 0 ) return true;
//////
//////        /*
//////         * replacement for bodyless player:
//////         */
//////        return isMovingOutOfBounds(dx, dy, 0, 0);
////
////
////        int xMin = -(Gfx.IMAGE_PIXEL_WIDTH>>1);
////        int xMax = Gfx.IMAGE_PIXEL_WIDTH>>1;
////        int yMin = -(Gfx.IMAGE_PIXEL_HEIGHT>>1);
////        int yMax = Gfx.IMAGE_PIXEL_HEIGHT>>1;
////
////        if(isMovingOutOfBounds(dx, dy, xMin, yMin)) return true;
////        if(isMovingOutOfBounds(dx, dy, xMin, yMax)) return true;
////        if(isMovingOutOfBounds(dx, dy, xMax, yMin)) return true;
////        if(isMovingOutOfBounds(dx, dy, xMax, yMax)) return true;
////        return false;
////    }
//
//
//    @Override
//    public void tick() {
////        int dx = 0;
////        int dy = 0;
////        if(input.up.isPressed()) {
////            dy--;
////        }
////        if(input.down.isPressed()) {
////            dy++;
////        }
////        if(input.left.isPressed()) {
////            dx--;
////        }
////        if(input.right.isPressed()) {
////            dx++;
////        }
////        if(dx!=0||dy!=0) {
////            move(dx,dy);
////            isMoving = true;
////        }
////        else {
////            isMoving = false;
////        }
////        tickCount++;
//    }
//
//
//    @Override
//    public void render(Screen screen) {
////        int xTile = 0;
////        int yTile = 0;
////
////        int walkingSpeed = 3;   // animation speed
////        int flipTop = ( moveCount >> walkingSpeed ) & 1;         // number between 0 and 1... why? check this out at some point
////        int flipBottom = ( moveCount >> walkingSpeed ) & 1;
////
////        if(movingDir == DIR_DOWN) {
////            xTile+=2;
////        }
////        else if(movingDir > 1) {
////            xTile += 4 + ((moveCount >> walkingSpeed) & 1) * 2;
////            flipTop = (movingDir - 1) % 2;
////        }
////
////        int modifier = 8 * scale;
////        int xOff = xWorld - modifier / 2;
////        int yOff = yWorld - modifier / 2 - 4;    // 4 = optional choice for pixel offset to make waist the center
////
////        screen.render(xOff + modifier*flipTop, yOff, sheetIndex, xTile + yTile * 32, color, scale, flipTop);
////        screen.render(xOff+modifier - modifier*flipTop, yOff, sheetIndex, xTile+1 + yTile * 32, color, scale, flipTop);
////
////        screen.render(xOff + modifier*flipBottom, yOff+modifier, sheetIndex, xTile + (yTile+1) * 32,   color, scale, flipBottom);
////        screen.render(xOff+modifier - modifier*flipBottom, yOff+modifier, sheetIndex, xTile+1 + (yTile+1) * 32, color, scale, flipBottom);
//    }
//
//
//}
//
//
//// <editor-fold defaultstate="collapsed" desc="old">
//
////    @Override
////    public void render(Screen screen) {
////        int xTile = 0;
////        int yTile = 28;
////
////        int modifier = 8 * scale;
////
////        int xOff = x - modifier / 2;
////        int yOff = y - modifier / 2 - 4;    // 4 = optional choice for offset to make waist the center
////
////        screen.render(xOff,          yOff,          xTile + yTile * 32,       color, scale, 0x00);
////        screen.render(xOff+modifier, yOff,          xTile+1 + yTile * 32,     color, scale, 0x00);
////        screen.render(xOff,          yOff+modifier, xTile + (yTile+1) * 32,   color, scale, 0x00);
////        screen.render(xOff+modifier, yOff+modifier, xTile+1 + (yTile+1) * 32, color, scale, 0x00);
////    }
//
//// </editor-fold>
//
