package bit2dspaceann.entities;

import bit2dspaceann.evo.Gene;
import bit2dspaceann.entities.controls.ANNControl;
import bit2dspaceann.entities.controls.Control;
import bit2dspaceann.entities.controls.FeedControl;
import bit2dspaceann.entities.controls.FuelColorControl;
import bit2dspaceann.entities.controls.FuelExchangeControl;
import bit2dspaceann.entities.controls.FueledDifferentialMoveControl;
import bit2dspaceann.entities.controls.SlowProximityControl;
import bit2dspaceann.entities.controls.TrajectoryControl;
import bit2dspaceann.entities.controls.vision.LineIntersect_RayVisionControl;
import bit2dspaceann.entities.controls.vision.VisionControl;
import bit2dspaceann.evo.GenePool;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.level.Level;
import java.util.Random;

/**
 *
 * @author Bo
 */
public class Fish extends EvoMob {
    
    private static final Random rnd = new Random();
    
    private static final int visionInputs = 21;
    
////    private static final int[] layerLayout = new int[] { 28, 14, 2 };
//    private static final int[] layerLayout = new int[] { 22, 16, 8, 2 };
////    private static final int[] layerLayout = new int[] { 22, 14, 2 };
////    private static final int[] layerLayout = new int[] { 16, 12, 2 };
////    private static final int[] layerLayout = new int[] { 10, 12, 2 };
////    private static final int[] layerLayout = new int[] { 4, 4, 2 };
    
//    private static final int[] layerLayout = new int[] { 23, 16, 8, 2 };
//    private static final int[] layerLayout = new int[] { 23, 18, 9, 2 };
//    private static final int[] layerLayout = new int[] { 23, 9, 5, 2 };
//    private static final int[] layerLayout = new int[] { 23, 18, 10, 2 };
//    private static final int[] layerLayout = new int[] { 23, 18, 8, 2 };
//    private static final int[] layerLayout = new int[] { 22, 16, 2 };
//    private static final int[] layerLayout = new int[] { 22, 16, 8, 2 };
//    private static final int[] layerLayout = new int[] { 24, 16, 2 };
//    private static final int[] layerLayout = new int[] { 23, 16, 2 };
    
//    private static final int[] layerLayout = new int[] { 23, 2 };
    private static final int[] layerLayout = new int[] { 23, 8, 2 };
    
    private ANNControl ann;
    
    public Fish(Level level, float xWorld, float yWorld) {
        super("Fish", level, xWorld, yWorld);
        initControls();
        setColor(1, 0, 0);
        vision = new float[visionInputs];
    }
    
    public Fish(Level level) {
        this(level,0,0);
        xWorld = rnd.nextFloat() * Gfx.IMAGE_PIXEL_WIDTH;
        yWorld = rnd.nextFloat() * Gfx.IMAGE_PIXEL_HEIGHT;
    }
    
    public Fish(Level level, float[] gene) {
        this(level);
        Control control = getControl(ANNControl.class);
        if(control!=null) {
            ANNControl annc = (ANNControl)control;
            annc.setGene(gene);
        }
        else {
            System.out.println("Failed to implant gene!");
        }
    }
    
    public Gene getGene() {
        return new Gene(ann.getGene(), fitness(), age);
    }

    @Override
    public void setGene(float[] gene) {
        ann.setGene(gene);
        age = 1;
    }
    

    @Override
    public void evolve(GenePool genePool) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void randomizeGene() {
        ann.randomizeANN();
    }

    
    
    
    
    @Override
    public void render(Screen screen) {
        super.render(screen);
    }
    
    public void kill() {
        this.flaggedForRemoval = true;
    }
    
    private void initControls() {

        // PROXIMITY
        SlowProximityControl pc = new SlowProximityControl(this);

        // VISION
//        VisionControl vc = new Eye2_RayVisionControl();
//        VisionControl vc = new Eye5_RayVisionControl();
//        VisionControl vc = new Eye3_Recurrent_RayVisionControl();
//        VisionControl vc = new Eye3_RayVisionControl();
        VisionControl vc = new LineIntersect_RayVisionControl(this);
        
        // DIFF MOVE
//        DifferentialMoveControl dmc = new DifferentialMoveControl(this);
        FueledDifferentialMoveControl dmc = new FueledDifferentialMoveControl(this);

        // ANN
        ann = new ANNControl(this,layerLayout);
        
        // MISC
        FeedControl fc = new FeedControl(this);
        TrajectoryControl tc = new TrajectoryControl(this);
        FuelColorControl fcc = new FuelColorControl(this);
        FuelExchangeControl fec = new FuelExchangeControl(this);
        
    }
    
}