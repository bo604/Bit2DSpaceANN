/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.entities;

import bit2dspaceann.entities.controls.Control;
import bit2dspaceann.gfx.ColorsLong;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.gfx.spriteutil.RotationSpriteMask;
import bit2dspaceann.level.Level;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Bo
 */
public class ControlledMob extends Mob {


    protected final List<Control> controls = new ArrayList<Control>(0);
    protected RotationSpriteMask sprite;

    // winkel
    public int angleRender = 64; // Range: [0..0xFF]    // 64 entspricht move to 0° for spaceship sprite!
    public float angleMove = 0;  // in radians
    
    // radgeschwindigkeiten
    public float vl = 0.48f;
    public float vr = 0.5f;
    
    public float[] vision; // = new float[9];
    private static final int visionRenderSize = 10;
    

    public final ArrayList<Entity> proximity = new ArrayList<Entity>();
    
    

    public ControlledMob(Level level, float xWorld, float yWorld) {
        this("ControlledMob", level, xWorld, yWorld);
    }

    public ControlledMob(String entityTypeName, Level level, float xWorld, float yWorld) {
        super(entityTypeName, level, xWorld, yWorld);
        this.sprite = defaultSprite();
    }

    public ControlledMob(String entityTypeName, Level level, float xWorld, float yWorld, RotationSpriteMask sprite) {
        super(entityTypeName, level, xWorld, yWorld);
        this.sprite = sprite;
    }

    private RotationSpriteMask defaultSprite() {
        return new RotationSpriteMask(2, 30, 2, 2, ColorsLong.DEF_SHIP);
//        return new RotationSpriteMask(0, 29, 1, 1, ColorsLong.DEF_SHIP);
    }

//    public boolean init(Level level, float x, float y, float speed) {
//        super.init("ControlledMob", level, x, y, speed);
//        sprite = new RotationSpriteMask(2, 30, 2, 2, ColorsLong.DEF_SHIP);
//        return true;
//    }
//
//    public boolean init(Level level, float x, float y, float speed, RotationSpriteMask sprite) {
//        super.init("ControlledMob", level, x, y, speed);
//        if(sprite==null) return false;
//        this.sprite = sprite;
//        return true;
//    }


    public boolean hasCollided(int dx, int dy) {
        return false;
    }

    public void render(Screen screen) {
        for (Iterator<Control> it = controls.iterator(); it.hasNext();) {
            Control control = it.next();
            control.render(screen);
        }
        /*
         * OPTIMIZE!
         */
//        // Range: [0..0xFF]    // 64 entspricht move to 0° for spaceship sprite!
//        int angleRender = 
        sprite.render(screen, Math.round(xWorld), Math.round(yWorld), angleRender%Gfx.NUM_ANGLES);
        
//        int colorScale = 3;
//        for (int i = -colorScale; i <= colorScale; i++) {
//            for (int j = -colorScale; j <= colorScale; j++) {
//                screen.renderPixelWithRGB(Math.round(xWorld+i), Math.round(yWorld+j),
//                        new Color(color[0],color[1],color[2]).getRGB());
//            }
//        }
        if(Gfx.RENDER_FISH_VISION) renderVision(screen);
        
//        renderDetectRadius(screen);
        
        
    }
    
    protected void renderVision(Screen screen) {
        if(vision==null) return;
        int n = vision.length / 3;
        for (int i = 0; i < n; i++) {
            float r = vision[i*3];
            float g = vision[i*3+1];
            float b = vision[i*3+2];
            int vColor = getVisionRenderColor(r, g, b);
            for (int j = 0; j < visionRenderSize; j++) {
                for (int k = 0; k < visionRenderSize; k++) {
                    int x = (int)(xWorld - (0.5f*n*visionRenderSize) + j + i * visionRenderSize);
                    int y = (int)(yWorld + k);
                    screen.renderPixelWithRGB(x, y, vColor);
                }
            }
        }
    }
    
    
    private static final int detectRadiusColor = Color.red.getRGB();
    
    private void renderDetectRadius(Screen screen) {
        float nPoints = 30.0f;
        for (int i = 0; i < nPoints; i++) {
            int x = Math.round(xWorld + getDetectRadius() * (float)Math.cos(i/nPoints * 2*Math.PI));
            int y = Math.round(yWorld + getDetectRadius() * (float)Math.sin(i/nPoints * 2*Math.PI));
            screen.renderPixelWithRGB(x, y, detectRadiusColor);
        }
    }
    
    private int getVisionRenderColor(float r, float g, float b) {
        float max = (r>g ? (r>b ? r:b) : (g>b ? g:b));
        if(max>1){
            r/=max;
            g/=max;
            b/=max;
        }
//        r/=3.0f;
//        g/=3.0f;
//        b/=3.0f;
        return new Color(r,g,b).getRGB();
    }

    public void tick() {
        for (Iterator<Control> it = controls.iterator(); it.hasNext();) {
            Control control = it.next();
            control.tick();
        }
    }

    public boolean addControl(Control control) {
        return controls.add(control);
    }

    public Control getControl(Class clazz) {
        for (Iterator<Control> it = controls.iterator(); it.hasNext();) {
            Control control = it.next();
            if(control.getClass() == clazz)
                return control;
        }
        return null;
    }

    public int getRenderAngle() {
        return angleRender;
    }

    public void setRenderAngle(int renderAngle) {
        this.angleRender = renderAngle;
    }

}

