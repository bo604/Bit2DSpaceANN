/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.entities;

import bit2dspaceann.entities.controls.DriftControl;
import bit2dspaceann.entities.controls.FoodLauncherControl;
import bit2dspaceann.entities.controls.PlanToxiControl;
import bit2dspaceann.entities.controls.SlowProximityControl;
import bit2dspaceann.evo.Gene;
import bit2dspaceann.evo.GenePool;
import bit2dspaceann.gfx.ColorsLong;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.gfx.renderers.GeoRenderer;
import bit2dspaceann.gfx.spriteutil.RotationSpriteMask;
import bit2dspaceann.level.Level;
import java.awt.Color;
import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author Bo
 */
public class Plant extends EvoMob {

    private static final Random rnd = new Random();
    public static long SPRITE_COLOR = ColorsLong.get(-1, -1, 010, 053, 440, 040, 252, 025);
    
    private boolean isToxic = false;
    private int colorRGB = -1;
    
    private float[] gene;

    public Plant(Level level, boolean isToxic) {
        this(level, 0, 0, isToxic);
        xWorld = rnd.nextInt(Gfx.IMAGE_PIXEL_WIDTH);
        yWorld = rnd.nextInt(Gfx.IMAGE_PIXEL_HEIGHT);
    }

    
    public Plant(Level level, float xWorld, float yWorld, boolean isToxic) {
        super("Plant", level, xWorld, yWorld);
        this.sprite = new RotationSpriteMask((byte)0, 0, 17, 4, 4, SPRITE_COLOR);
        this.isToxic = isToxic;
        setDetectRadius(20);
        initGene();
        initControls();
    }
    
    private void initControls() {
        if(isToxic()) {
            SlowProximityControl spc = new SlowProximityControl(this);
            spc.setRectSize(60);
            PlanToxiControl ptc = new PlanToxiControl(this);
//            setColor(.6f, .2f, .7f);
            setColor(1, 0, 1);
        }
        else {
            FoodLauncherControl flc = new FoodLauncherControl(level, this);
//            setColor(.8f, .7f, .2f);
//            setColor(.2f, .7f, .8f);
            setColor(0, 1, 1);
        }
        DriftControl dc = new DriftControl(this);
        dc.temp = 1;
    }

    private void initGene() {
        if(gene==null)
            gene = new float[6];
        for (int i = 0; i < gene.length; i++) {
            gene[i] = rnd.nextFloat();
        }
        setColor(Arrays.copyOf(gene, 3));
    }
    
    @Override
    public void tick() {
        super.tick();
    }

    @Override
    public void render(Screen screen) {
        GeoRenderer.drawCircleThick(screen, colorRGB, (int)xWorld, (int)yWorld, 20, 80);
        super.render(screen);
    }

    @Override
    public Gene getGene() {
        return new Gene(gene, foodCollected, age);
    }
    
    @Override
    public void setGene(float[] gene) {
        this.gene = gene;
        age = 1;
    }

    @Override
    public void evolve(GenePool genePool) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void randomizeGene() {
        initGene();
    }
    
    
    
    public boolean isToxic() {
        return isToxic;
    }

    @Override
    public void setColor(float r, float g, float b) {
        super.setColor(r, g, b);
        colorRGB = new Color(r,g,b).getRGB();
    }
    
    public void setColor(float[] col) {
        color = col;
        colorRGB = new Color(col[0],col[1],col[2]).getRGB();
    }
    
}
