package bit2dspaceann.entities;

import bit2dspaceann.evo.Gene;
import bit2dspaceann.evo.GenePool;
import bit2dspaceann.gfx.Font;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.gfx.spriteutil.RotationSpriteMask;
import bit2dspaceann.level.Level;

/**
 *
 * @author Bo
 */
public abstract class EvoMob extends ControlledMob {

    public int age = 1;
    public int foodCollected = 0;
    public int ticksNoSnack = 0;
    
    public float fuel = 1.0f;

    public EvoMob(Level level, float xWorld, float yWorld) {
        this("EvoMob", level, xWorld, yWorld);
    }

    public EvoMob(String entityTypeName, Level level, float xWorld, float yWorld) {
        super(entityTypeName, level, xWorld, yWorld);
    }

    public EvoMob(String entityTypeName, Level level, float xWorld, float yWorld, RotationSpriteMask sprite) {
        super(entityTypeName, level, xWorld, yWorld, sprite);
    }
    
    public abstract Gene getGene();
    public abstract void setGene(float[] gene);
    public abstract void randomizeGene();
    
    public abstract void evolve(GenePool genePool);

    @Override
    public void render(Screen screen) {
        super.render(screen);
        if(Gfx.RENDER_ENTITY_INFO && foodCollected>0)
            Font.render(
                    Integer.toString(foodCollected), screen,
                    (int)xWorld + (sprite.xTileSize<<2),
                    (int)yWorld + (sprite.yTileSize<<2),
                    Font.DEF_COLOR, 1);
    }

    @Override
    public void tick() {
        super.tick();
    }
    
    public void reset() {
        ticksNoSnack = 0;
        foodCollected = 0;
        fuel = 1.0f;
    }
    
    public float fitness() {
        float fitness = 1 + foodCollected;
        return fitness;
    }
    
}
