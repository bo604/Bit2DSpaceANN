/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.entities;

import bit2dspaceann.entities.controls.Control;
import bit2dspaceann.entities.controls.DriftControl;
import bit2dspaceann.gfx.ColorsLong;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.gfx.spriteutil.SpriteMask;
import bit2dspaceann.level.Level;
import java.util.Iterator;
import java.util.Random;

/**
 *
 * @author Bo
 */
public class Food extends ControlledMob {
    
    private static final Random rnd = new Random();
    
    protected SpriteMask sprite;

    public Food(Level level) {
        this(level,0,0);
        // create everywhere:
        xWorld = rnd.nextFloat() * Gfx.IMAGE_PIXEL_WIDTH;
        yWorld = rnd.nextFloat() * Gfx.IMAGE_PIXEL_HEIGHT;
//        // only create at top:
//        xWorld = rnd.nextFloat() * Gfx.IMAGE_PIXEL_WIDTH;
//        yWorld = rnd.nextFloat() * 0.1f * Gfx.IMAGE_PIXEL_HEIGHT;
    }
    
    public Food(Level level, float xWorld, float yWorld) {
        super("Food", level, xWorld, yWorld);
//        sprite = new SpriteMask((byte)0, 0, 21, 1, 1, ColorsLong.DEF_FOOD);
        sprite = new SpriteMask((byte)0, 0, 22, 2, 2, ColorsLong.DEF_FOOD);
        setColor(0,1,0);
        initControls();
        setDetectRadius(8);
    }
    
    private void initControls() {
        DriftControl dc = new DriftControl(this);
//        SinkControl sc = new SinkControl(this);
//        TrajectoryControl tc = new TrajectoryControl(this);
    }

    public void setSpriteColor(long col) {
        sprite.color = col;
    }

    @Override
    public void render(Screen screen) {
        for (Iterator<Control> it = controls.iterator(); it.hasNext();) {
            Control control = it.next();
            control.render(screen);
        }
        sprite.render(screen, Math.round(xWorld)-8, Math.round(yWorld)-8);
    }
    
}
