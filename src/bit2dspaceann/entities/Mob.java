/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.entities;

import bit2dspaceann.gfx.Screen;
import bit2dspaceann.level.Level;

/**
 *
 * @author Bo
 */
public abstract class Mob extends Entity {

    protected byte sheetIndex = 1;
    protected int scale = 1;

    public Mob(String entityTypeName, Level level, float xWorld, float yWorld) {
        super(entityTypeName, level, xWorld, yWorld);
    }
    
    public abstract void tick();

    public abstract void render(Screen screen);

    public String getName() {
        return entityTypeName;
    }
    
}

//    public static final int DIR_UP = 0;
//    public static final int DIR_DOWN = 1;
//    public static final int DIR_LEFT = 2;
//    public static final int DIR_RIGHT = 3;
//    protected float speed;
//    protected int moveCount = 0;     // total number of steps taken by the mob since in like ever! DOH!
//    protected boolean isMoving;
//    protected int movingDir = 1;    //0 up, 1 down, 2 left, 3 right
//    public Mob() {


////        super(level,mobTypeName);
//        super();
//    }

//    public boolean init(String entityTypeName, Level level, float x, float y, float speed) {
//        boolean init = super.init(entityTypeName, level);
//        this.xWorld = x;
//        this.yWorld = y;
//        this.speed = speed;
//        return init;
//    }


// <editor-fold defaultstate="collapsed" desc="old">

//    public abstract boolean hasCollided(int dx, int dy);
//    private float fdx = 0;
//    private float fdy = 0;

//    public void move(int dx, int dy) {
//        if (dx != 0 && dy != 0) {
//            move(dx, 0);
//            move(0, dy);
//            moveCount--;
//            return;
//        }
//        moveCount++;
//        if (!hasCollided(dx, dy)) {
//            if (dy < 0) {
//                movingDir = DIR_UP;      //0;
//            }
//            if (dy > 0) {
//                movingDir = DIR_DOWN;    //1;
//            }
//            if (dx < 0) {
//                movingDir = DIR_LEFT;    //2;
//            }
//            if (dx > 0) {
//                movingDir = DIR_RIGHT;   //3;
//            }
//            /*
//             * TODO: import float movement from Bit2D555
//             */
//
////            // Apply move
////            xWorld += dx * speed;
////            yWorld += dy * speed;
//
//            fdx += dx * speed;
//            fdy += dy * speed;
//
//            xWorld += (int) fdx;
//            yWorld += (int) fdy;
//
//            fdx -= (int) fdx;
//            fdy -= (int) fdy;
//
////            System.out.println("x=" + xWorld + ", y=" + yWorld);
//        }
//
//    }
    
//    private static final int logSize = Gfx.TILE_SIZE_LOG2;

//    protected boolean isSolidTile(int dx, int dy, int xOff, int yOff) {
//        if (level == null) {
//            return false;
//        }
//        // Transition pixels to tiles
//        int x = Math.round(xWorld);
//        int y = Math.round(yWorld);
//        Tile lastTile = level.getTile((x + xOff) >> logSize, (y + yOff) >> logSize);
//        Tile newTile = level.getTile((x + dx + xOff) >> logSize, (y + dy + yOff) >> logSize);
//        // Changes position to a solid tile?
//        if (!lastTile.equals(newTile) && newTile.isSolid()) {
//            return true;
//        }
//        return false;
//    }

//    protected boolean isMovingOutOfBounds(int dx, int dy, int xOff, int yOff) {
//        if (level == null) {
//            return false;
//        }
//        int x = Math.round(xWorld);
//        int y = Math.round(yWorld);
//        Tile newTile = level.getTile((x + dx + xOff) >> logSize, (y + dy + yOff) >> logSize);
//        if (newTile.getID() == Tile.VOID.getID())
//            return true;
//        return false;
//    }

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="old">


//    private int dxAccu = 0;
//    private int dyAccu = 0;
////    private int maxAccu = 10;
//    private void accumulateAndMove(int dx, int dy) {
//        dxAccu += dx;
//        dyAccu += dy;
////        if(dxAccu/maxAccu != 0) {
////            x += dxAccu/10;
////            dxAccu = dxAccu%10;
////        }
////        if(dyAccu/maxAccu != 0) {
////            y += dyAccu/10;
////            dyAccu = dyAccu%10;
////        }
//        if(Math.abs(dxAccu) >= speed) {
//            xWorld += Math.signum(dxAccu);
//            dxAccu -= Math.signum(dxAccu) * speed;
//        }
//        if(Math.abs(dyAccu) >= speed) {
//            yWorld += Math.signum(dyAccu);
//            dyAccu -= Math.signum(dyAccu) * speed;
//        }
//    }

// </editor-fold>


