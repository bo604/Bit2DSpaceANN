package bit2dspaceann.entities;

import bit2dspaceann.entities.controls.ANNControl;
import bit2dspaceann.entities.controls.CarniFeedControl;
import bit2dspaceann.entities.controls.DifferentialMoveControl;
import bit2dspaceann.entities.controls.TrajectoryControl;
import bit2dspaceann.entities.controls.vision.Eye3_RayVisionControl;
import bit2dspaceann.entities.controls.vision.VisionControl;
import bit2dspaceann.evo.Gene;
import bit2dspaceann.evo.GenePool;
import bit2dspaceann.gfx.ColorsLong;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.gfx.spriteutil.RotationSpriteMask;
import bit2dspaceann.level.Level;
import java.util.Random;

/**
 *
 * @author Bo
 */
public class Shark extends EvoMob {
    
    private static final Random rnd = new Random();
    
    private static final int[] layerLayout = new int[] { 10, 6, 2 };
    private static final int visionInputs = 9;
    
    private ANNControl ann;
    
    public Shark(Level level, float xWorld, float yWorld) {
        super("Shark", level, xWorld, yWorld,
              new RotationSpriteMask(4, 24, 4, 4,
                  ColorsLong.get(-1,100,110,220,330,541,552,554)));
        initControls();
        setColor(.99f, .99f, .99f);
        setDetectRadius(16.0f);
        vision = new float[visionInputs];
    }

    public Shark(Level level) {
        this(level,0,0);
        xWorld = rnd.nextFloat() * Gfx.IMAGE_PIXEL_WIDTH;
        yWorld = rnd.nextFloat() * Gfx.IMAGE_PIXEL_HEIGHT;
    }
    
    public Shark(Level level, float[] gene) {
        this(level);
//        Control control = getControl(ANNControl.class);
//        if(control!=null) {
//            ANNControl annc = (ANNControl)control;
//            annc.setGene(gene);
//        }
//        else {
//            System.out.println("Failed to implant gene!");
//        }
        ann.setGene(gene);
    }

    @Override
    public Gene getGene() {
        return new Gene(ann.getGene(), fitness(), age);
    }
    
    @Override
    public void setGene(float[] gene) {
        ann.setGene(gene);
        age = 1;
    }

    @Override
    public void evolve(GenePool genePool) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void randomizeGene() {
        ann.randomizeANN();
    }
    
    
    
    @Override
    public void render(Screen screen) {
        super.render(screen);
    }
    
    
    
    private void initControls() {
        
//        VisionControl vc = new Eye2_RayVisionControl();
//        VisionControl vc = new Eye5_RayVisionControl();
        VisionControl vc = new Eye3_RayVisionControl(this);
        
        ann = new ANNControl(this, layerLayout);
        
        DifferentialMoveControl dmc = new DifferentialMoveControl(this);
        dmc.dt *= 0.5f;
        
        CarniFeedControl fc = new CarniFeedControl(this);
        TrajectoryControl tc = new TrajectoryControl(this);
//        ColorControl cc = new ColorControl();
        
    }
    
    
    
}