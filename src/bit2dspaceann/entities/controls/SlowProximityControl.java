/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.entities.controls;

import bit2dspaceann.entities.ControlledMob;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.util.Rect2Di;
import java.awt.Color;
import java.util.Random;

/**
 *
 * @author Bo
 */
public class SlowProximityControl extends Control {
    
    private int rectSize = 60;
    
//    private final Rectangle2D proxRect = new Rectangle();
    private final Rect2Di rect = new Rect2Di(0,0,rectSize,rectSize);
    
    private Random rnd = new Random();
    private int ticks = 0;
    private int proxRebuildTicks = 10;
    private boolean renderSwitch = true;

    public SlowProximityControl(ControlledMob mob) {
        super(mob);
    }

    
    @Override
    public void tick() {
        updateProximity();
    }
    
    
    private void updateProximity() {
        ticks++;
        if(ticks+rnd.nextInt(5) > proxRebuildTicks) {
            ticks = 0;
            renderSwitch = !renderSwitch;
            // set prox rect
//            float x = mob.xWorld - (rectSize>>1);
//            float y = mob.yWorld - (rectSize>>1);
//            proxRect.setRect(x, y, rectSize, rectSize);
            updateProxRect();
//            mob.level.getEntitiesInside(proxRect, mob.proximity);
            mob.level.getEntitiesInside(rect.x, rect.y, rect.w, rect.h, mob.proximity);
        }
    }
    
    private void updateProxRect() {
//        float x = mob.xWorld - (rectSize>>1);
//        float y = mob.yWorld - (rectSize>>1);
//        proxRect.setRect(x, y, rectSize, rectSize);
        // double implementation: why does lower not work?
        rect.x = (int)(mob.xWorld - (rectSize>>1));
        rect.y = (int)(mob.yWorld - (rectSize>>1));
    }
    
    public void setRectSize(int size) {
        rectSize = size;
        rect.w = size;
        rect.h = size;
    }

    private static final int color = Color.yellow.getRGB();
    
    @Override
    public void render(Screen screen) {
//        if(renderSwitch)
//            GeoRenderer.drawRect(screen, color, rect);
    }
    
    
}
