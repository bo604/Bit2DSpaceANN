/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.entities.controls;

import bit2dspaceann.entities.ControlledMob;
import bit2dspaceann.gfx.Screen;
import java.util.Random;

/**
 *
 * @author Bo
 */
public class DifferentialMoveControl extends Control {
    
    Random rnd = new Random();

    public DifferentialMoveControl(ControlledMob mob) {
        super(mob);
    }
    
    // radabstand
    public float b = 1.0f;
    
//    // radgeschwindigkeiten
//    public float vl = 0.48f;
//    public float vr = 0.5f;

    // delta time
    public float dt = 2.0f;
    
    @Override
    public void tick() {
        float dAngle = dt * (mob.vr-mob.vl) / b;
        float dx = dt * (mob.vl+mob.vr)/2 * (float)Math.cos(mob.angleMove);
        float dy = dt * (mob.vl+mob.vr)/2 * (float)Math.sin(mob.angleMove);
        
//        System.out.println("dDir="+dDir+", dx="+dx+", dy="+dy);
        
        mob.xWorld += dx;
        mob.yWorld += dy;
        mob.angleMove += dAngle;
        mob.angleRender = Math.round(mob.angleMove / 2.0f / (float)Math.PI * 256 + 64) & 0xFF;
        
//        vl += 0.01 * (2 * rnd.nextFloat() - 1);
//        vr += 0.01 * (2 * rnd.nextFloat() - 1);
        
    }

    @Override
    public void render(Screen screen) { }
    
}
