/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.entities.controls;

import bit2dspaceann.entities.Fish;
import bit2dspaceann.gfx.Screen;

/**
 *
 * @author Bo
 */
public class FuelColorControl extends Control {

    Fish f;
    
    public FuelColorControl(Fish fish) {
        super(fish);
        f = fish;
    }

    
    @Override
    public void tick() {
        f.color[0] = 1.0f;
        f.color[1] = (float)Math.exp(- f.fuel);
        f.color[2] = 0; //1.0f - (float)Math.exp(- f.fuel);
    }
    
////    int color;
//    float maxRadius = 10.0f;
//    float nRings = 4.0f;
////    float nAngles = 36.0f;
//    float pointDist = 8.0f;

    @Override
    public void render(Screen screen) {
//        int color = new Color(mob.color[0],mob.color[1],mob.color[2]).getRGB();
//        float colorNorm = (mob.color[0] + mob.color[1] + mob.color[2]); // * 0.333f;
//        
////        float radius = colorNorm * maxRadius;
//        float radius = maxRadius;
//        
//        GeoRenderer.drawCircle(screen, color, (int)mob.xWorld, (int)mob.yWorld, radius, 30);
//        
////        float dr = radius / nRings;
////        for (int i = 1; i <= nRings; i++) {
////            float r = i * dr;
////            float nAngles = (int)((2 * (float)Math.PI * r) / pointDist);
////            for (int j = 0; j < nAngles; j++) {
////                float angle = 2.0f * (float)Math.PI / nAngles * j;
////                float dx = r * (float)Math.cos(angle);
////                float dy = r * (float)Math.sin(angle);
////                screen.renderPixelWithRGB(Math.round(mob.xWorld+dx),
////                        Math.round(mob.yWorld+dy), color);
////            }
////        }
    }
    
    
}
