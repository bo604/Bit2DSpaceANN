/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.entities.controls;

import bit2dspaceann.entities.ControlledMob;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.level.Level;
import java.util.Random;

/**
 *
 * @author Bo
 */
public class FoodLauncherControl extends Control {
    
    Level level;
    private static final Random rnd = new Random();

    public FoodLauncherControl(Level level, ControlledMob mob) {
        super(mob);
        this.level = level;
    }

    private static final int foodPeriod = 100;
    private int ticks = 0;

    @Override
    public void tick() {
        
        if(ticks + rnd.nextInt(10) > foodPeriod) {
            ticks = 0;
            
//            Food food = new Food(level, mob.xWorld, mob.yWorld);
//            food.color = Arrays.copyOf(mob.color, 3);
//            VelocityControl vc = new VelocityControl(food);
//            vc.v.set(2*rnd.nextFloat()-1, 2*rnd.nextFloat()-1);
            
            level.prepareLaunchFood(mob);
        }
        
        ticks++;
    }
    

    @Override
    public void render(Screen screen) {
        
    }
    
}
