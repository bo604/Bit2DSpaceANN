/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.entities.controls;

import bit2dspaceann.entities.ControlledMob;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;

/**
 *
 * @author Bo
 */
public class SinkControl extends Control {
    
//    private static final Random rnd = new Random();
    
    private static final float aGrav = 0.02f;
//    private static final float groundAccelFactor = 0.02f;
    private static final float groundDecay = 0.1f;
    private static final float dampeningFactor = 0.08f;
    private float vy = 0;

    public SinkControl(ControlledMob mob) {
        super(mob);
    }
                
    @Override
    public void tick() {
        float groundDistance = 0.98f * Gfx.IMAGE_PIXEL_HEIGHT - mob.yWorld;
//        float aGround = - groundAccelFactor * groundDistance;
        
        float aGround = - (float)Math.exp(-groundDecay*groundDistance);
        
        float aDampen = - dampeningFactor * vy;
        
        vy += aGrav + aGround + aDampen;
        mob.yWorld += vy;
    }

    @Override
    public void render(Screen screen) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
