/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.entities.controls;

import bit2dspaceann.entities.Entity;
import bit2dspaceann.entities.EvoMob;
import bit2dspaceann.gfx.Screen;
import java.util.List;

/**
 *
 * @author Bo
 */
public class FeedControl extends Control {
    
//    Random rnd = new Random();
    private static final float feedRadius2 = 130.0f;
    
//    // proximity
//    private static final int rectSize = 50;
//    private final ArrayList<Entity> proximity = new ArrayList<Entity>();
//    private final Rectangle2D r = new Rectangle();
    
    private String foodType = "Food";
    
//    private Class foodClass = Food.class;
    
    EvoMob emob;

    public FeedControl(EvoMob mob) {
        super(mob);
        emob = mob;
    }
    
    
    @Override
    public void tick() {
        emob.ticksNoSnack++;
        
//        updateProximity();
        
        // get ents
        List<Entity> foodList = getFoodList();
        for (Entity food : foodList) {
            if(!food.flaggedForRemoval && food.getEntityTypeName().equals(foodType)) {
                float dx = emob.xWorld - food.xWorld;
                float dy = emob.yWorld - food.yWorld;
                if(dx*dx + dy*dy < feedRadius2) {
                    // eat
                    food.flaggedForRemoval = true;
                    emob.foodCollected++;
                    emob.ticksNoSnack = 0;
//                    System.out.println("I just ate " + food.getClass().toString());
//                    if(food.getClass() == foodClass) {
//                        System.out.println("I also ate " + foodClass.toString());
//                    }
                    emob.fuel += 0.15f;
//                    emob.fuel = emob.fuel > 1.0f ? 1.0f : emob.fuel;
                    break;
                }
            }
        }
        
    }
    
    private List<Entity> getFoodList() {
        return emob.proximity;
//        return mob.level.entities;
    }
    
    
    @Override
    public void render(Screen screen) {
    }
    
}

//        for (Iterator<Entity> it = mob.level.entities.iterator(); it.hasNext();) {
//            Entity entity = it.next();
//            if(!entity.flaggedForRemoval && entity.getEntityTypeName().equals("Food")) {
//                float dx = mob.xWorld - entity.xWorld;
//                float dy = mob.yWorld - entity.yWorld;
//                float dist2 = dx*dx + dy*dy;
//                if(dist2<feedRadius2) {
////                    System.out.println("OMNOMNOM!");
//                    entity.flaggedForRemoval = true;
//                    mob.foodCollected++;
//                    mob.ticksNoSnack = 0;
//                    break;
//                }
//            }
//        }
    
    

//    @Override
//    public void tick() {
//        mob.ticksNoSnack++;
//        for (Iterator<Entity> it = mob.level.entities.iterator(); it.hasNext();) {
//            Entity entity = it.next();
//            if(!entity.flaggedForRemoval && entity.getEntityTypeName().equals("Food")) {
//                float dx = mob.xWorld - entity.xWorld;
//                float dy = mob.yWorld - entity.yWorld;
//                float dist2 = dx*dx + dy*dy;
//                if(dist2<feedRadius2) {
////                    System.out.println("OMNOMNOM!");
//                    entity.flaggedForRemoval = true;
//                    mob.foodCollected++;
//                    mob.ticksNoSnack = 0;
//                    break;
//                }
//            }
//        }
//    }

