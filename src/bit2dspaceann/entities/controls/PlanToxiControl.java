/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.entities.controls;

import bit2dspaceann.entities.Entity;
import bit2dspaceann.entities.EvoMob;
import bit2dspaceann.entities.Fish;
import bit2dspaceann.entities.Food;
import bit2dspaceann.gfx.Font;
import bit2dspaceann.gfx.Screen;
import java.util.List;

/**
 *
 * @author Bo
 */
public class PlanToxiControl extends Control {
    
//    Random rnd = new Random();
    private static final float feedRadius2 = 400.0f;
    
    EvoMob emob;
    
    public PlanToxiControl(EvoMob mob) {
        super(mob);
        emob = mob;
    }
    
    
    @Override
    public void tick() {
        emob.ticksNoSnack++;
        
        // get ents
        List<Entity> possibleFoods = getFoodList();
        for (Entity possFood : possibleFoods) {
            if(possFood.flaggedForRemoval) {
                continue;
            }
            
            if(possFood.getEntityTypeName().equals("Fish")) {
                if(distanceSquared(possFood) < feedRadius2) {
                    Fish fish = (Fish)possFood;
                    fish.kill();
                    reward();
                    break;
                }
            }
            
            if(possFood.getEntityTypeName().equals("Food")) {
                if(distanceSquared(possFood) < feedRadius2) {
                    Food food = (Food)possFood;
                    food.flaggedForRemoval = true;
//                    reward();
                    break;
                }
            }
        
        }
    }
    
    private void reward() {
        emob.foodCollected++;
        emob.ticksNoSnack = 0;
    }

    private float distanceSquared(Entity entity) {
        float dx = emob.xWorld - entity.xWorld;
        float dy = emob.yWorld - entity.yWorld;
        return dx*dx + dy*dy;
    }
    
    private List<Entity> getFoodList() {
        return emob.proximity;
//        return mob.level.entities;
    }
    
    private static final String tox = "Tox";
    private static final int toxLength = Font.sizeOf(tox, 1);
    
    @Override
    public void render(Screen screen) {
        Font.render(tox, screen, (int)emob.xWorld-toxLength/2, (int)emob.yWorld+22, Font.DEF_COLOR, 1);
    }
    
}
    
    

//    @Override
//    public void tick() {
//        mob.ticksNoSnack++;
//        for (Iterator<Entity> it = mob.level.entities.iterator(); it.hasNext();) {
//            Entity entity = it.next();
//            if(!entity.flaggedForRemoval && entity.getEntityTypeName().equals("Food")) {
//                float dx = mob.xWorld - entity.xWorld;
//                float dy = mob.yWorld - entity.yWorld;
//                float dist2 = dx*dx + dy*dy;
//                if(dist2<feedRadius2) {
////                    System.out.println("OMNOMNOM!");
//                    entity.flaggedForRemoval = true;
//                    mob.foodCollected++;
//                    mob.ticksNoSnack = 0;
//                    break;
//                }
//            }
//        }
//    }

