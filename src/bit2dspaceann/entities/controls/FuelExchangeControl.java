/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.entities.controls;

import bit2dspaceann.entities.Entity;
import bit2dspaceann.entities.Fish;
import bit2dspaceann.gfx.Screen;

/**
 *
 * @author Bo
 */
public class FuelExchangeControl extends Control {
    
    private final Fish f;
    
    private static final float exchangeRadiusSquared = 150.0f;

    public FuelExchangeControl(Fish mob) {
        super(mob);
        f = mob;
    }
    

    @Override
    public void tick() {
                
//        for (Entity entity : mob.proximity) {
//            if(entity == mob) continue;
//            if(entity.getClass() == Fish.class) {
//                Fish fish = (Fish)entity;
//                if(distanceSquared(fish) < exchangeRadiusSquared) {
//                    float totalFuel = fish.fuel + f.fuel;
//                    fish.fuel = .5f * totalFuel;
//                    f.fuel = .5f * totalFuel;
//                }
//            }
//        }
        
        mob.proximity.stream().filter(e -> canExchangeFuel(e)).forEach(e -> exchangeFuel(e));
                
    }
    
    private boolean canExchangeFuel(Entity e) {
        return e!=null && e!=mob && e.getClass()==Fish.class
               && distanceSquared(e) < exchangeRadiusSquared;
    }
    
    private void exchangeFuel(Entity e) {
        Fish fish = (Fish)e;
        float totalFuel = fish.fuel + f.fuel;
        fish.fuel = .5f * totalFuel;
        f.fuel = .5f * totalFuel;
    }
    
    private float distanceSquared(Entity e) {
        float dx = e.xWorld - f.xWorld;
        float dy = e.yWorld - f.yWorld;
        return dx*dx + dy*dy;
    }

    @Override
    public void render(Screen screen) {
        
    }
    
    
    
}
