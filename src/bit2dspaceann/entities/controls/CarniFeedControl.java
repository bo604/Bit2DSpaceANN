/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.entities.controls;

import bit2dspaceann.entities.Entity;
import bit2dspaceann.entities.EvoMob;
import bit2dspaceann.entities.Fish;
import bit2dspaceann.gfx.Screen;
import java.util.Iterator;
import java.util.Random;

/**
 *
 * @author Bo
 */
public class CarniFeedControl extends Control {
    
    Random rnd = new Random();
    
    private static final float feedRadius2 = 300.0f;
    
    EvoMob emob;

    public CarniFeedControl(EvoMob mob) {
        super(mob);
        emob = mob;
    }
    
    
    @Override
    public void tick() {
//        ticksNoSnack++;
        emob.ticksNoSnack++;
        for (Iterator<Entity> it = emob.level.entities.iterator(); it.hasNext();) {
            Entity entity = it.next();
            if(!entity.flaggedForRemoval && entity.getEntityTypeName().equals("Fish")) {
                Fish fish = (Fish)entity;
                float dx = emob.xWorld - fish.xWorld;
                float dy = emob.yWorld - fish.yWorld;
                float dist2 = dx*dx + dy*dy;
                if(dist2<feedRadius2) {
                    System.out.println("OMNOMNOM!");
                    fish.kill();
                    emob.foodCollected++;
                    emob.ticksNoSnack = 0;
                    break;
                }
            }
        }
    }

    @Override
    public void render(Screen screen) {
    }
    
}
