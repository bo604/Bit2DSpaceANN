/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.entities.controls;

import bit2dspaceann.entities.ControlledMob;
import bit2dspaceann.gfx.Screen;
import java.util.Random;

/**
 *
 * @author Bo
 */
public class DriftControl extends Control {
    
    private static final Random rnd = new Random();
    
//    private float ax = 0;
//    private float ay = 0;
//    private float aMax = 0.01f;
//    private float aMin = -aMax;
//
//    private float vx = 0;
//    private float vy = 0;
//    private float vMax = 1.0f;
//    private float vMin = -vMaxf;
    
    public float temp = 0.5f;

    public DriftControl(ControlledMob mob) {
        super(mob);
    }

    @Override
    public void tick() {
//        float dax = 0.0001f * (2*rnd.nextFloat()-1);
//        float day = 0.0001f * (2*rnd.nextFloat()-1);
//        ax = Math.max(aMin,Math.min(ax+dax, aMax));
//        ay = Math.max(aMin,Math.min(ay+day, aMax));
//        
////        ax = ax > 0 ? (ax < aMax ? ax : aMax) : (ax > aMin ? ax : aMin);
////        ay = ay > 0 ? (ay < aMax ? ay : aMax) : (ay > aMin ? ay : aMin);
//        
//        vx = Math.max(vMin,Math.min(vx+ax, vMax));
//        vy = Math.max(vMin,Math.min(vy+ay, vMax));
//        
//        mob.xWorld += vx;
//        mob.yWorld += vy;
        
        mob.xWorld += temp * (2*rnd.nextFloat()-1);
        mob.yWorld += temp * (2*rnd.nextFloat()-1);
        
    }

    @Override
    public void render(Screen screen) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
