/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.entities.controls;

import bit2dspaceann.entities.EvoMob;
import bit2dspaceann.game.BlackBoard;
import bit2dspaceann.gfx.Font;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.gfx.renderers.GeoRenderer;
import java.awt.Color;
import java.util.Random;

/**
 *
 * @author Bo
 */
public class FueledDifferentialMoveControl extends Control {
    
    Random rnd = new Random();
    EvoMob evoMob;

    public FueledDifferentialMoveControl(EvoMob mob) {
        super(mob);
        evoMob = mob;
    }
    
    // radabstand
    public float b = 1.0f;
    
//    // radgeschwindigkeiten
//    public float vl = 0.48f;
//    public float vr = 0.5f;

    // delta time
    public float dt = 2.0f;
    
//    public float consumeFactor = 0.0005f;
    public float consumeFactor = 0.001f;
    
    private float gas = 0;
    
    @Override
    public void tick() {
        float dAngle = dt * (mob.vr-mob.vl) / b;
        float dx = dt * (mob.vl+mob.vr)/2 * (float)Math.cos(mob.angleMove);
        float dy = dt * (mob.vl+mob.vr)/2 * (float)Math.sin(mob.angleMove);
//        System.out.println("dDir="+dDir+", dx="+dx+", dy="+dy);
        
        // fuel
        float v2 = dx*dx + dy*dy;
        float fuelConsumed = consumeFactor * v2;
//        System.out.println("fuel consumed: " + fuelConsumed);
        if(evoMob.fuel < fuelConsumed) {
            dx = 0;
            dy = 0;
            dAngle = 0;
//            evoMob.flaggedForRemoval = true;
        }
        else {
//            dx *= evoMob.fuel;
//            dy *= evoMob.fuel;
//            dAngle *= evoMob.fuel;
            evoMob.fuel -= fuelConsumed;
        }
        
        mob.xWorld += dx;
        mob.yWorld += dy;
        mob.angleMove += dAngle;
        mob.angleRender = Math.round(mob.angleMove / 2.0f / (float)Math.PI * 256 + 64) & 0xFF;
        
//        vl += 0.01 * (2 * rnd.nextFloat() - 1);
//        vr += 0.01 * (2 * rnd.nextFloat() - 1);
        
        gas = (float)Math.sqrt(v2);
        
        color = new Color(mob.color[0], mob.color[1], mob.color[2]).getRGB();
        
    }

    int color = Color.red.getRGB();
    
    @Override
    public void render(Screen screen) { 
        
        if(Gfx.RENDER_ENTITY_INFO) {
            Font.render(BlackBoard.dec2.format(evoMob.fuel), screen, (int)mob.xWorld+8, (int)mob.yWorld-8, Font.DEF_COLOR, 1);
            GeoRenderer.drawCircle(screen, color, (int)mob.xWorld, (int)mob.yWorld, 20 * evoMob.fuel, 30);
            
//            Font.render(BlackBoard.dec2.format(gas), screen, (int)mob.xWorld+8, (int)mob.yWorld, Font.DEF_COLOR, 1);
//            GeoRenderer.drawCircle(screen, color, (int)mob.xWorld, (int)mob.yWorld, 20 * gas, 30);
        }
        
    }
    
}
