/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.entities.controls.vision;

import bit2dspaceann.entities.ControlledMob;
import bit2dspaceann.entities.Entity;
import static bit2dspaceann.entities.controls.vision.VisionControl.maxVisionDist;
import bit2dspaceann.gfx.Screen;
import java.awt.Color;
import java.util.List;

/**
 *
 * @author Bo
 */
public abstract class RayVisionControl extends VisionControl {
    
    protected static final float rayStepLength = 7.0f;
    
    protected static final float maxRenderDistance = maxVisionDist; //100.0f;

    public RayVisionControl(ControlledMob mob) {
        super(mob);
    }
    
    @Override
    public void tick() {
        super.tick();
    }
    
    @Override
    public abstract void render(Screen screen);
//        // render vision area rectangle
//        Rectangle2D rect = constructVisionRect();
//        for (int i = 0; i < rect.getWidth(); i++) {
//            screen.renderPixelWithRGB((int)rect.getX()+i, (int)rect.getY(), Color.red.getRGB());
//            screen.renderPixelWithRGB((int)rect.getX()+i, (int)rect.getY()+(int)rect.getHeight(), Color.red.getRGB());
//        }
//        for (int j = 0; j < rect.getHeight(); j++) {
//            screen.renderPixelWithRGB((int)rect.getX(), (int)rect.getY()+j, Color.red.getRGB());
//            screen.renderPixelWithRGB((int)rect.getX()+(int)rect.getWidth(), (int)rect.getY()+j, Color.red.getRGB());
//
//        }

    
    protected float[] castRay(float[] dr) {
        return castRay(dr[0], dr[1], 0);
    }
    
    protected float[] castRay(float[] dr, float minDist) {
        return castRay(dr[0], dr[1], minDist);
    }
    
    protected float[] castRay(float dx, float dy, float minDist) {
        float x = mob.xWorld;
        float y = mob.yWorld;
        float dist = 0;
        float ds = (float)Math.sqrt(dx*dx+dy*dy);
        float[] result = new float[3];
        
        // skip ray beyond minimum distance
        if(dist<minDist) {
            float dd = minDist-dist;
            int steps = (int)(dd/ds)+1;
            x += steps * dx;
            y += steps * dy;
            dist += steps * ds;
        }
        
        // cast ray
        while(dist<maxVisionDist) {
            // prep next step
            x += dx;
            y += dy;
            dist += ds;
            float colorFactor = colorFactor(dist);
            
            // see wall?
            if(!mob.level.contains(x, y)) {
                result[2] = colorFactor;
//                System.out.println("Seeing level wall with color " + Arrays.toString(result));
                return result;
            }
            else {
                float dxe = 0, dye = 0;
                List<Entity> closeEntities = getEntitiesInVisionRect();
                // check close entities
                for (Entity entity : closeEntities) {
                    if(entity == mob) continue;
                    // see other entity?
//                    if(entity.contains(x, y)) {
//                        for (int i = 0; i < 2; i++) {
//                            result[i] = colorFactor * entity.color[i];
//                        }
////                        System.out.println("Seeing entity " + entity.getEntityTypeName()
////                                         + " with color " + Arrays.toString(result));
//                        return result;
//                    }
                    
                    // inline:
                    dxe = entity.xWorld-x;
                    dye = entity.yWorld-y;
                    if(dxe*dxe + dye*dye < entity.detectRadiusSquared) {
                        for (int i = 0; i < 2; i++) {
                            result[i] = colorFactor * entity.color[i];
                        }
                        return result;
                    }
                    
                }
            }
        }
        return result;
    }
    
    protected void renderRay(Screen screen, float[] dr) {
        renderRay(screen, dr[0], dr[1], 0);
    }
    
    protected void renderRay(Screen screen, float[] dr, float minDist) {
        renderRay(screen, dr[0], dr[1], minDist);
    }
    
    protected void renderRay(Screen screen, float dx, float dy, float minDist) {
        float x = mob.xWorld;
        float y = mob.yWorld;
        float dist = 0;
        float ds = (float)Math.sqrt(dx*dx+dy*dy);
        
        // skip ray beyond minimum distance
        if(dist<minDist) {
            float dd = minDist-dist;
            int steps = (int)(dd/ds)+1;
            x += steps * dx;
            y += steps * dy;
            dist += steps * ds;
        }
        
        while(mob.level.contains(x, y) && dist < maxRenderDistance) {
            float colorFactor = dist/maxVisionDist;
            colorFactor = colorFactor*colorFactor;
            Color color = new Color(colorFactor, 1-colorFactor, 0);
            screen.renderPixelWithRGB(Math.round(x), Math.round(y), color.getRGB());
            x+=dx;
            y+=dy;
            dist+=ds;
        }
    }
    
    
}