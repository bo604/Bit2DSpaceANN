package bit2dspaceann.entities.controls.vision;

import bit2dspaceann.entities.ControlledMob;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.util.ArrayMath;

/**
 *
 * @author Bo
 */
public class Eye2_RayVisionControl extends RayVisionControl {
    
    protected float rayRotAngle = .05f; //0.06f;
    protected float eyeRotAngle = 0; //-.01f;
    
    protected static final int nSideRays = 3;

    public Eye2_RayVisionControl(ControlledMob mob) {
        super(mob);
    }
    
    @Override
    public void tick() {
        // SUPER!
        super.tick();
        
        // movement direction unit vector
        float[] dr = new float[] {
            rayStepLength * (float)Math.cos(mob.angleMove),
            rayStepLength * (float)Math.sin(mob.angleMove)
        };

        // cast and add rays
        float[] left = new float[3];
        float[] right = new float[3];
        for (int i = 0; i <= nSideRays; i++) {
            ArrayMath.addInternal(left, castRay(ArrayMath.rotate(dr, -eyeRotAngle - i * rayRotAngle)));
            ArrayMath.addInternal(right, castRay(ArrayMath.rotate(dr, eyeRotAngle + i * rayRotAngle)));
        }

        // copy vision to mob
        System.arraycopy(left, 0, mob.vision, 0, 3);
        System.arraycopy(right, 0, mob.vision, 3, 3);
    }

    
    @Override
    public void render(Screen screen) {
        // do render?
        if(!Gfx.RENDER_VISION_RAYS) return;
        
        // move direction unit vector
        float[] dr = new float[] {
            rayStepLength * (float)Math.cos(mob.angleMove),
            rayStepLength * (float)Math.sin(mob.angleMove)
        };
        
        // render rays
        for (int i = 0; i <= nSideRays; i++) {
            renderRay(screen, ArrayMath.rotate(dr, -eyeRotAngle - i * rayRotAngle));
            renderRay(screen, ArrayMath.rotate(dr, eyeRotAngle + i * rayRotAngle));
        }
        
    }
    
}