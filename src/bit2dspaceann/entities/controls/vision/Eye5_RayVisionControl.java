package bit2dspaceann.entities.controls.vision;

import bit2dspaceann.entities.ControlledMob;
import bit2dspaceann.entities.Fish;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.util.ArrayMath;
import java.util.ArrayList;

/**
 *
 * @author Bo
 */
public class Eye5_RayVisionControl extends RayVisionControl {
    
    protected float rayRotAngle = .06f; //0.06f;
//    protected float eyeRotAngle = .06f; //-.01f;
    
    protected static final int nRays = 10;

    public Eye5_RayVisionControl(ControlledMob mob) {
        super(mob);
    }
    
    @Override
    public void tick() {
        // SUPER!
        super.tick();
        
        // movement direction unit vector
        float[] dr = new float[] {
            rayStepLength * (float)Math.cos(mob.angleMove),
            rayStepLength * (float)Math.sin(mob.angleMove)
        };

        
//        System.out.println("Casting rays...");
        float[] startdr = ArrayMath.rotate(dr, - (nRays/2.0f-.5f) * rayRotAngle );
        ArrayList<float[]> rays = new ArrayList<float[]>(nRays);
        for (int i = 0; i < nRays; i++) {
            rays.add(castRay(ArrayMath.rotate(startdr, i * rayRotAngle)));
        }
        
//        System.out.println("Combine and copy rays...");
        for (int i = 0; i < nRays/2; i++) {
            float[] eyeVision = new float[3];
            ArrayMath.addInternal(eyeVision,rays.get(i*2));
            ArrayMath.addInternal(eyeVision,rays.get(i*2+1));
            System.arraycopy(eyeVision, 0, mob.vision, i*3, 3);
        }

    }

    
    @Override
    public void render(Screen screen) {
        // do render?
        if(!Gfx.RENDER_VISION_RAYS) return;
        
        // move direction unit vector
        float[] dr = new float[] {
            rayStepLength * (float)Math.cos(mob.angleMove),
            rayStepLength * (float)Math.sin(mob.angleMove)
        };

        // render rays
        float[] startdr = ArrayMath.rotate(dr, - (nRays/2.0f-.5f) * rayRotAngle );
        for (int i = 0; i < nRays; i++) {
            renderRay(screen, ArrayMath.rotate(startdr, i * rayRotAngle));
        }
    }
    
}