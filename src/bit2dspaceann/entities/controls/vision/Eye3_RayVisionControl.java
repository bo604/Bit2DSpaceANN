package bit2dspaceann.entities.controls.vision;

import bit2dspaceann.entities.ControlledMob;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.util.ArrayMath;

/**
 *
 * @author Bo
 */
public class Eye3_RayVisionControl extends RayVisionControl {
    
    protected float rayRotAngle = .07f; //0.06f;
//    protected float eyeRotAngle = .06f; //-.01f;
    
    protected static final int nSideRays = 3;

    public Eye3_RayVisionControl(ControlledMob mob) {
        super(mob);
    }
    
    @Override
    public void tick() {
        // SUPER!
        super.tick();
        
        // movement direction unit vector
        float[] dr = new float[] {
            rayStepLength * (float)Math.cos(mob.angleMove),
            rayStepLength * (float)Math.sin(mob.angleMove)
        };

        // cast and add rays
        float[] left = new float[3];
        float[] mid = new float[3];
        float[] right = new float[3];
        
        // center ray
        ArrayMath.addInternal(mid, castRay(dr));
        
        //side rays
        float[] rayNeg, rayPos;
        for (int i = 0; i < nSideRays; i++) {
//            rayNeg = castRay(rotate(dr, -eyeRotAngle - i * rayRotAngle));
            rayNeg = castRay(ArrayMath.rotate(dr, -rayRotAngle - i * rayRotAngle));
            ArrayMath.addInternal(left,rayNeg);
//            rayPos = castRay(rotate(dr, eyeRotAngle + i * rayRotAngle));
            rayPos = castRay(ArrayMath.rotate(dr, rayRotAngle + i * rayRotAngle));
            ArrayMath.addInternal(right,rayPos);
            if(i==0) {
                ArrayMath.addInternal(mid,rayNeg);
                ArrayMath.addInternal(mid,rayPos);
            }
        }
        
        // renormalize brightness depending on ray number
        ArrayMath.multInternal(left, 1.0f/3.0f);
        ArrayMath.multInternal(right, 1.0f/3.0f);
        ArrayMath.multInternal(mid, 1.0f/3.0f);

        // copy vision to mob
        System.arraycopy(left, 0, mob.vision, 0, 3);
        System.arraycopy(mid, 0, mob.vision, 3, 3);
        System.arraycopy(right, 0, mob.vision, 6, 3);
    }

    
    @Override
    public void render(Screen screen) {
        // do render?
        if(!Gfx.RENDER_VISION_RAYS) return;
        
        // move direction unit vector
        float[] dr = new float[] {
            rayStepLength * (float)Math.cos(mob.angleMove),
            rayStepLength * (float)Math.sin(mob.angleMove)
        };
        
        // render rays
        for (int i = 0; i <= nSideRays; i++) {
//            renderRay(screen, rotate(dr, -eyeRotAngle - i * rayRotAngle));
//            renderRay(screen, rotate(dr, eyeRotAngle + i * rayRotAngle));
            renderRay(screen, ArrayMath.rotate(dr, -rayRotAngle - i * rayRotAngle));
            renderRay(screen, ArrayMath.rotate(dr, rayRotAngle + i * rayRotAngle));
        }
        
        // mid
        renderRay(screen, dr);
//        renderRay(screen, ArrayMath.rotate(dr, -rayRotAngle));
//        renderRay(screen, ArrayMath.rotate(dr, rayRotAngle));
    }
    
}