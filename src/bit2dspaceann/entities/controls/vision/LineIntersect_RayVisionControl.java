/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.entities.controls.vision;

import bit2dspaceann.entities.ControlledMob;
import bit2dspaceann.entities.Entity;
import static bit2dspaceann.entities.controls.vision.VisionControl.maxVisionDist;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.gfx.renderers.GeoRenderer;
import bit2dspaceann.util.ArrayMath;
import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Bo
 */
public class LineIntersect_RayVisionControl extends VisionControl {
    
    
    private static final int color1 = Color.orange.getRGB();
    private static final int color2 = Color.green.getRGB();
    
    protected static final float maxRenderDistance = maxVisionDist; //100.0f;
    
    private static final int nEyes = 7;
    private static final int nRaysPerEye = 1;
    private static final int nRays = nEyes * nRaysPerEye;
    
    float rayAngle = 0.06f / nRaysPerEye;
    
    protected Ray[] rays;
    protected LinkedList<Entity> detectedEntities = new LinkedList<Entity>();
    
    private static final float[] black = new float[]{0,0,0};
    
    private ArrayList<Entity> allDetected = new ArrayList<Entity>();
    
    Point lastScreenIntersect;

    public LineIntersect_RayVisionControl(ControlledMob mob) {
        super(mob);
        initRays();
    }
    

    @Override
    public void tick() {
        // SUPER!
        super.tick();
        
        castEyes();
    }
    
    
    
    @Override
    public void render(Screen screen) {
        // do render?
        if(!Gfx.RENDER_VISION_RAYS) return;
        
        renderRays(screen);
        
//        if(!detectedEntities.isEmpty()) {
//            for (Entity entity : detectedEntities) {
//                GeoRenderer.drawCircle(screen, color1,
//                        (int)entity.xWorld, (int)entity.yWorld, 20, 100);
//            }
//            // mark closest
//            Entity closest = detectedEntities.getFirst();
//                GeoRenderer.drawCircle(screen, color2,
//                        (int)closest.xWorld, (int)closest.yWorld, 25, 100);
//        }
        
        if(!allDetected.isEmpty()) {
            // sort if more than one eye
            if(nEyes>1) Collections.sort(allDetected, new DistanceComparator(mob));
            // mark detected entities
            for (Entity entity : allDetected) {
                GeoRenderer.drawCircle(screen, color1,
                        (int)entity.xWorld, (int)entity.yWorld, 20, 100);
            }
            // mark closest detected entity
            Entity closest = allDetected.get(0);
            GeoRenderer.drawCircle(screen, color2,
                    (int)closest.xWorld, (int)closest.yWorld, 25, 100);
        }
        
        if(lastScreenIntersect!=null) {
            GeoRenderer.drawCircle(screen, color2,
                    lastScreenIntersect.x, lastScreenIntersect.y, 5, 50);
            GeoRenderer.drawCircle(screen, color2,
                    lastScreenIntersect.x, lastScreenIntersect.y, 6, 50);
        }
        
    }

    
    
    private void initRays() {
//        boolean result = super.init(mob);
        rays = new Ray[nRays];
        for (int i = 0; i < nRays; i++) {
//            rays[i] = new Ray(mob.xWorld, mob.yWorld, mob.angleMove);
            rays[i] = new Ray();
        }
//        return result;
    }
    
    
    
    
    private void updateRays() {
        for (int i = 0; i < nRays; i++) {
            rays[i].set(mob, (i-(nRays>>1))*rayAngle);
        }
    }

    /********************
     * STUFF TO FIX!
     * 
     * - vision macht zicken
     *   (siehe "seen pixels" in simulation -> einige zuviel schwarz?)
     * 
     */
    
    
    protected void castEyes() {
        updateRays();
        allDetected.clear();
        float[] vision = new float[3*nEyes];
        for (int i = 0; i < nEyes; i++) {
            float[] eyeVision = new float[3];
            for (int j = 0; j < nRaysPerEye; j++) {
                Entity closest = closestRayCollision(rays[i*nRaysPerEye + j]);
                if(closest!=null) {
                    ArrayMath.addInternal(eyeVision, getColor(closest));
                }
                else {
                    Point screenIntersect = rays[i*nRaysPerEye + j].getScreenIntersect();
                    if(screenIntersect!=null) {
                        eyeVision[2] += colorFactor(distance(screenIntersect));
//                        if(!screenIntersect.equals(lastScreenIntersect)) {
    //                        System.out.println("Screen intersect: " + screenIntersect.toString());
//                        }
                        lastScreenIntersect = screenIntersect;
                    }
                    else {
//                        eyeVision[2] += colorFactor(0);
                        eyeVision[2] += 1.0f;
                    }
                }
            }
            for (int j = 0; j < 3; j++) {
                vision[i*3+j] = eyeVision[j];
            }
        }
        
        
        mob.vision = vision;
        /**
         * NORMALIZE VISION OR NOT?!?!?!?
         */
        // try: default by number of rays
        if(nRaysPerEye>1) ArrayMath.multInternal(mob.vision, 1.0f/nRaysPerEye);
        
        
//        System.out.println("Full Vision: " + Arrays.toString(vision));
    }
    
    
    
    private float[] getColor(Entity entity) {
//        if(entity==null) {
////            return new float[] {0,0,colorFactor(getWallDist())};
//            return Arrays.copyOf(black, 3);
//        }
//        else {
            float[] color = Arrays.copyOf(entity.color, 3);
            float dist = distance(entity);
            float factor = colorFactor(dist);
//            System.out.println("dist="+dist +", colfac="+factor);
            ArrayMath.multInternal(color, factor);
            return color;
//        }
    }
    
    
    
    private float distance(Point p) {
        float dx = p.x - mob.xWorld;
        float dy = p.y - mob.yWorld;
        return (float)Math.sqrt(dx*dx+dy*dy);
    }
    
    private float distance(Entity entity) {
        float dx = entity.xWorld - mob.xWorld;
        float dy = entity.yWorld - mob.yWorld;
        return (float)Math.sqrt(dx*dx + dy*dy);
    }
    
    protected Entity closestRayCollision(Ray ray) {
        detectedEntities.clear();
        
        for (Entity entity : getEntitiesInVisionRect()) {
            if(entity == mob) continue;
            if(ray.intersectsCircle(entity.xWorld, entity.yWorld, entity.getDetectRadius())) {
                detectedEntities.add(entity);
            }
        }
        
        if(detectedEntities.isEmpty()) {
            
////            System.out.println("Need to find wall:");
//            Point p = ray.getScreenIntersect();
//            System.out.println("Ray.getSC: " + p.toString());
//            lastScreenIntersect = p;
            
            return null;
        }
        
        if(detectedEntities.size() > 1) {
            Collections.sort(detectedEntities, new DistanceComparator(mob));
        }
        
        allDetected.addAll(detectedEntities);
        
        return detectedEntities.getFirst();
    }
    
    
    
    protected float[] ___castRay() {
        
//        ray = new Ray(mob.xWorld, mob.yWorld, mob.angleMove);
        for (int i = 0; i < nRays; i++) {
            rays[i].set(mob, (i-nRays/2)*rayAngle);
        }
        
        detectedEntities.clear();
        
        List<Entity> checkList;
        
//        checkList = mob.level.entities;
        checkList = this.getEntitiesInVisionRect();
        
        for (Entity entity : checkList) {
            if(entity == mob) continue;
            
//            if(ray.intersectsCircle(entity.xWorld, entity.yWorld, entity.getDetectRadius())) {
////                System.out.println("Seeing entity: " + entity.getEntityTypeName());
////                lastSeen = entity;
////                return entity.color;
//                detectedEntities.add(entity);
//            }
            
            for (Ray ray : rays) {
                if(ray.intersectsCircle(entity.xWorld, entity.yWorld, entity.getDetectRadius())) {
                    detectedEntities.add(entity);
                }
            }
            
        }
        
        if(detectedEntities.isEmpty()) {
            return black;
        }
        
        if(detectedEntities.size() > 1)
            Collections.sort(detectedEntities, new DistanceComparator(mob));
        
        return detectedEntities.getFirst().color;
    }
    
    
    
    public void renderRays(Screen screen) {
        for (Ray ray : rays) {
            ray.render(screen);
        }
    }
    
    
    protected float[] __castRay(float dx, float dy, float minDist) {
        float x = mob.xWorld;
        float y = mob.yWorld;
        float dist = 0;
        float ds = (float)Math.sqrt(dx*dx+dy*dy);
        float[] result = new float[3];
        
        // skip ray beyond minimum distance
        if(dist<minDist) {
            float dd = minDist-dist;
            int steps = (int)(dd/ds)+1;
            x += steps * dx;
            y += steps * dy;
            dist += steps * ds;
        }
        
        // cast ray
        while(dist<maxVisionDist) {
            // prep next step
            x += dx;
            y += dy;
            dist += ds;
            float colorFactor = colorFactor(dist);
            
            // see wall?
            if(!mob.level.contains(x, y)) {
                result[2] = colorFactor;
//                System.out.println("Seeing level wall with color " + Arrays.toString(result));
                return result;
            }
            else {
                float dxe = 0, dye = 0;
                List<Entity> closeEntities = getEntitiesInVisionRect();
                // check close entities
                for (Entity entity : closeEntities) {
                    if(entity == mob) continue;
                    // see other entity?
//                    if(entity.contains(x, y)) {
//                        for (int i = 0; i < 2; i++) {
//                            result[i] = colorFactor * entity.color[i];
//                        }
////                        System.out.println("Seeing entity " + entity.getEntityTypeName()
////                                         + " with color " + Arrays.toString(result));
//                        return result;
//                    }
                    
                    // inline:
                    dxe = entity.xWorld-x;
                    dye = entity.yWorld-y;
                    if(dxe*dxe + dye*dye < entity.detectRadiusSquared) {
                        for (int i = 0; i < 2; i++) {
                            result[i] = colorFactor * entity.color[i];
                        }
                        return result;
                    }
                    
                }
            }
        }
        return result;
    }
    
//    protected void renderRay(Screen screen, float[] dr) {
//        renderRay(screen, dr[0], dr[1], 0);
//    }
//    
//    protected void renderRay(Screen screen, float[] dr, float minDist) {
//        renderRay(screen, dr[0], dr[1], minDist);
//    }
    
    protected void __renderRay(Screen screen, float dx, float dy, float minDist) {
        float x = mob.xWorld;
        float y = mob.yWorld;
        float dist = 0;
        float ds = (float)Math.sqrt(dx*dx+dy*dy);
        
        // skip ray beyond minimum distance
        if(dist<minDist) {
            float dd = minDist-dist;
            int steps = (int)(dd/ds)+1;
            x += steps * dx;
            y += steps * dy;
            dist += steps * ds;
        }
        
        while(mob.level.contains(x, y) && dist < maxRenderDistance) {
            float colorFactor = dist/maxVisionDist;
            colorFactor = colorFactor*colorFactor;
            Color color = new Color(colorFactor, 1-colorFactor, 0);
            screen.renderPixelWithRGB(Math.round(x), Math.round(y), color.getRGB());
            x+=dx;
            y+=dy;
            dist+=ds;
        }
    }
    
    
}


//    static Point getLineIntersection(Line2D.Float line1, Line2D.Float line2)
//    {
//        Point result = null;
//
//        float
//            s1_x = line1.x2 - line1.x1,
//            s1_y = line1.y2 - line1.y1,
//
//            s2_x = line2.x2 - line2.x1,
//            s2_y = line2.y2 - line2.y1,
//
//            s = (-s1_y * (line1.x1 - line2.x1) + s1_x * (line1.y1 - line2.y1)) / (-s2_x * s1_y + s1_x * s2_y),
//            t = ( s2_x * (line1.y1 - line2.y1) - s2_y * (line1.x1 - line2.x1)) / (-s2_x * s1_y + s1_x * s2_y);
//
//        if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
//        {
//            // Collision detected
//            result = new Point(
//                (int) (line1.x1 + (t * s1_x)),
//                (int) (line1.y1 + (t * s1_y)));
//        }   // end if
//
//        return result;
//    }    
