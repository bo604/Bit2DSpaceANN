/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.entities.controls.vision;

import bit2dspaceann.entities.Entity;
import java.util.Comparator;

/**
 *
 * @author Bo
 */
public class DistanceComparator implements Comparator<Entity> {
    
    private Entity me;

    public DistanceComparator(Entity e) {
        this.me = e;
    }

    public int compare(Entity t1, Entity t2) {
        float d1 = distanceSquared(t1);
        float d2 = distanceSquared(t2);
        if(d1<d2) return -1;
        if(d2<d1) return 1;
        return 0;
    }
    
    private float distanceSquared(Entity other) {
        float dx = me.xWorld - other.xWorld;
        float dy = me.yWorld - other.yWorld;
        return dx*dx + dy*dy;
    }
    
    
}



//class DistanceComparator<Entity> implements Comparator<Entity> {
//    
//    private Entity e;
//
//    public DistanceComparator(Entity myEnt) {
//        this.e = myEnt;
//        myEnt.
//    }
//
//    public int compare(Entity e1, Entity e2) {
//        return 0;
//    }
//
//    private float distance(Entity e1) {
//        e.
//        return 0;
//    }
//    
//}
