package bit2dspaceann.entities.controls.vision;

import bit2dspaceann.entities.ControlledMob;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.util.ArrayMath;

/**
 *
 * @author Bo
 */
public class Eye3_Recurrent_RayVisionControl extends RayVisionControl {
    
    protected float rayRotAngle = .07f; //0.06f;
//    protected float eyeRotAngle = .06f; //-.01f;
    
    protected static final int nSideRays = 3;
    
    // factor for blending recurrent vision between old (0) and new (1) color
    private static final float recurrentVisionMixFactor = 0.3f;

    public Eye3_Recurrent_RayVisionControl(ControlledMob mob) {
        super(mob);
    }
    
    
    @Override
    public void tick() {
        // SUPER!
        super.tick();
        
        // movement direction unit vector
        float[] dr = new float[] {
            rayStepLength * (float)Math.cos(mob.angleMove),
            rayStepLength * (float)Math.sin(mob.angleMove)
        };

        // cast and add rays
        float[] left = new float[3];
        float[] mid = new float[3];
        float[] right = new float[3];
        
        // center ray
        ArrayMath.addInternal(mid, castRay(dr));
        
        //side rays
        float[] rayNeg, rayPos;
        for (int i = 0; i < nSideRays; i++) {
//            rayNeg = castRay(rotate(dr, -eyeRotAngle - i * rayRotAngle));
            rayNeg = castRay(ArrayMath.rotate(dr, -rayRotAngle - i * rayRotAngle));
            ArrayMath.addInternal(left,rayNeg);
//            rayPos = castRay(rotate(dr, eyeRotAngle + i * rayRotAngle));
            rayPos = castRay(ArrayMath.rotate(dr, rayRotAngle + i * rayRotAngle));
            ArrayMath.addInternal(right,rayPos);
            if(i==0) {
                ArrayMath.addInternal(mid,rayNeg);
                ArrayMath.addInternal(mid,rayPos);
            }
        }
        
        // copy vision to mob
        System.arraycopy(left, 0, mob.vision, 0, 3);
        System.arraycopy(mid, 0, mob.vision, 3, 3);
        System.arraycopy(right, 0, mob.vision, 6, 3);
        
        // set lasts
        lastLeft = ArrayMath.mix(lastLeft, left, recurrentVisionMixFactor);
        lastMid = ArrayMath.mix(lastMid, mid, recurrentVisionMixFactor);
        lastRight = ArrayMath.mix(lastRight, right, recurrentVisionMixFactor);
        // copy recurrent vision to mob
        System.arraycopy(lastLeft, 0, mob.vision, 9, 3);
        System.arraycopy(lastMid, 0, mob.vision, 12, 3);
        System.arraycopy(lastRight, 0, mob.vision, 15, 3);
        
    }

    private float[] lastLeft = new float[3];
    private float[] lastMid = new float[3];
    private float[] lastRight = new float[3];
    
    @Override
    public void render(Screen screen) {
        // do render?
        if(!Gfx.RENDER_VISION_RAYS) return;
        
        // move direction unit vector
        float[] dr = new float[] {
            rayStepLength * (float)Math.cos(mob.angleMove),
            rayStepLength * (float)Math.sin(mob.angleMove)
        };
        
        // render mid ray
        renderRay(screen, dr);
        // render side rays
        for (int i = 0; i <= nSideRays; i++) {
//            renderRay(screen, rotate(dr, -eyeRotAngle - i * rayRotAngle));
//            renderRay(screen, rotate(dr, eyeRotAngle + i * rayRotAngle));
            renderRay(screen, ArrayMath.rotate(dr, -rayRotAngle - i * rayRotAngle));
            renderRay(screen, ArrayMath.rotate(dr, rayRotAngle + i * rayRotAngle));
        }
        
    }
    
}