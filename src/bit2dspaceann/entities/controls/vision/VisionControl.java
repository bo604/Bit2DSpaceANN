/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.entities.controls.vision;

import bit2dspaceann.entities.ControlledMob;
import bit2dspaceann.entities.Entity;
import bit2dspaceann.entities.controls.Control;
import bit2dspaceann.util.MiscMath;
import bit2dspaceann.util.Rect2Di;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Bo
 */
public abstract class VisionControl extends Control {
    
//    protected static final float maxVisionDist = 250;
    protected static final int maxVisionDist = 350;
    
    private List<Entity> closeEnts = new ArrayList<Entity>();
    private int lastEntityCheck = 0;
    
    private int tick = 0;

    public VisionControl(ControlledMob mob) {
        super(mob);
    }
    
    
    @Override
    public void tick() {
        tick++;
    }

    
    protected List<Entity> getEntitiesInVisionRect() {
        if(lastEntityCheck<tick) {
            lastEntityCheck=tick;
//            mob.level.getEntitiesInside(constructVisionRect(), closeEnts);
            mob.level.getEntitiesInside(constructVisionRect2Di(), closeEnts);
        }
        return closeEnts;
    }
    
    
    protected Rect2Di constructVisionRect2Di() {
        int w = maxVisionDist;
        int h = maxVisionDist;
        float radiusForCenter = maxVisionDist>>1;
        int cx = (int)(mob.xWorld + radiusForCenter * (float)Math.cos(mob.angleMove));
        int cy = (int)(mob.yWorld + radiusForCenter * (float)Math.sin(mob.angleMove));
        Rect2Di rect = new Rect2Di( cx-(w>>1), cy-(h>>1), w, h);
        return rect;
    }
    
    protected Rectangle2D constructVisionRect() {
        float w = 1.05f * maxVisionDist;
        float h = 1.05f * maxVisionDist;
        float radiusForCenter = .98f * .5f * maxVisionDist;
        float cx = mob.xWorld + radiusForCenter * (float)Math.cos(mob.angleMove);
        float cy = mob.yWorld + radiusForCenter * (float)Math.sin(mob.angleMove);
        Rectangle2D rect = new Rectangle((int)(cx-.5f*w), (int)(cy-.5f*h), (int)w, (int)h);
        return rect;
    }
    
    
    public float colorFactor(float dist) {
//        float colorfactor = dist/maxDist;
//        return 1 - colorfactor;
        return MiscMath.cosDecay(dist, maxVisionDist);
    }
    
    
    
}
