/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.entities.controls.vision;

import bit2dspaceann.util.Vec2f;
import bit2dspaceann.entities.ControlledMob;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.gfx.renderers.GeoRenderer;
import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author Bo
 */
public class Ray {
    
//    public float xOrigin, yOrigin;
//    public float dx, dy;
//    public float nx, ny;
    
    public Vec2f origin;
    public Vec2f direction;
    public Vec2f normal;
    
    private static final int color = Color.magenta.getRGB();
    private static final int renderLength = 2000;
    private static final int normalLength = 30;

    private static final Line2D.Float[] screenBorders;
    
    static {
        int w = Gfx.IMAGE_PIXEL_WIDTH;
        int h = Gfx.IMAGE_PIXEL_HEIGHT;
        screenBorders = new Line2D.Float[4];
        screenBorders[0] = new Line2D.Float(0, 0, w, 0);    // top
        screenBorders[1] = new Line2D.Float(0, h, w, h);    // bot
        screenBorders[2] = new Line2D.Float(0, 0, 0, h);    // left
        screenBorders[3] = new Line2D.Float(w, 0, w, h);    // right
    }
    
    public Ray(float xOrigin, float yOrigin, float angle) {
        origin = new Vec2f(xOrigin, yOrigin);
        direction = new Vec2f();
        normal = new Vec2f();
        setAngle(angle);
    }
    
    public Ray() {
        this(0,0,0);
    }
    
    
    private static final float halfPi = (float)(Math.PI * 0.5);
    
    public final void setAngle(float angle) {
        direction.x = (float)Math.cos(angle);
        direction.y = (float)Math.sin(angle);
        normal.x = -direction.y;
        normal.y = direction.x;
//        normal = Vec2f.rotate(direction, halfPi);
    }
    
    public final void set(ControlledMob rayShooter, float angleOffset) {
        origin.x = rayShooter.xWorld;
        origin.y = rayShooter.yWorld;
//        setAngle(rayShooter.angleMove+angleOffset);
        direction.x = (float)Math.cos(rayShooter.angleMove+angleOffset);
        direction.y = (float)Math.sin(rayShooter.angleMove+angleOffset);
        normal.x = -direction.y;
        normal.y = direction.x;
    }
    
    public boolean intersectsCircle(float xCircle, float yCircle, float radius) {
        Vec2f delta = new Vec2f(xCircle - origin.x, yCircle - origin.y);
        float dist = delta.scalarProduct(normal);
//        System.out.println("dist="+dist);
//        if(dist<0) return false;
        if (Math.abs(dist) < radius) {
            return delta.scalarProduct(direction) > 0;
        }
        else {
            return false;
        }
    }
    
    
    
    public float getNormalProjection(float x, float y) {
        Vec2f delta = new Vec2f(x - origin.x, y - origin.y);
        float dist = delta.scalarProduct(normal);
        return dist;
    }
    
    
    
    public void render(Screen screen) {
        GeoRenderer.drawLine(screen, color, (int)origin.x, (int)origin.y,
                (int)(origin.x+renderLength*direction.x), (int)(origin.y+renderLength*direction.y));
        
        GeoRenderer.drawLine(screen, color, (int)origin.x, (int)origin.y,
                (int)(origin.x+normalLength*normal.x), (int)(origin.y+normalLength*normal.y));
    }
    

    private static final float intersectLineLength = 2000;
    

    public Point getScreenIntersect() {
        Line2D.Float rayLine = getRayLine();
        for (int i = 0; i < screenBorders.length; i++) {
            Line2D.Float screenBorder = screenBorders[i];
            Point p = getLineIntersection(screenBorder, rayLine);
            if(p!=null) {
//                System.out.println("Screen Intersect: " + p.toString());
                return p;
            }
        }
        // may happen? think so..
//        System.out.println("NO INTERSECT BÄÄÄÄM!!!");
        return null;
    }
    
    public Point getLineIntersection(Line2D.Float line) {
        return getLineIntersection(line, getRayLine());
    }

    private Line2D.Float getRayLine() {
        return new Line2D.Float(origin.x, origin.y,
            origin.x + intersectLineLength * direction.x,
            origin.y + intersectLineLength * direction.y);
    }
            
    private static Point getLineIntersection(Line2D.Float line1, Line2D.Float line2) {
        Point result = null;

        float
            s1_x = line1.x2 - line1.x1,
            s1_y = line1.y2 - line1.y1,

            s2_x = line2.x2 - line2.x1,
            s2_y = line2.y2 - line2.y1,

            s = (-s1_y * (line1.x1 - line2.x1) + s1_x * (line1.y1 - line2.y1)) / (-s2_x * s1_y + s1_x * s2_y),
            t = ( s2_x * (line1.y1 - line2.y1) - s2_y * (line1.x1 - line2.x1)) / (-s2_x * s1_y + s1_x * s2_y);

        if (s >= 0 && s <= 1 && t >= 0 && t <= 1) {
            // Collision detected
            result = new Point(
                (int) (line1.x1 + (t * s1_x)),
                (int) (line1.y1 + (t * s1_y)));
        }

        return result;
    }    
    
//    private Line2D.Float[] screenBorders;
    
//    private float getWallDist() {
////        Rectangle r = new Rectangle(0, 0, Gfx.IMAGE_PIXEL_WIDTH, Gfx.IMAGE_PIXEL_HEIGHT);
//        
//        float x2 = mob.xWorld + (float)(10 * Math.cos(mob.angleMove));
//        float y2 = mob.yWorld + (float)(10 * Math.sin(mob.angleMove));
//        Line2D.Float mobLine = new Line2D.Float(mob.xWorld, mob.yWorld, x2, y2);
//        
//        for (int i = 0; i < screenBorders.length; i++) {
//            Line2D.Float line = screenBorders[i];
//            Point p = getLineIntersection(line, mobLine);
//            if(p!=null) {
//                float dx = p.x-mob.xWorld;
//                float dy = p.y-mob.xWorld;
//                return (float)Math.sqrt(dx*dx + dy*dy);
//            }
//        }
//        // should not happen?
//        return -1;
//    }
    
}
