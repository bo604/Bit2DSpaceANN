/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.entities.controls.vision;

import bit2dspaceann.entities.ControlledMob;
import static bit2dspaceann.entities.controls.vision.VisionControl.maxVisionDist;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.util.ArrayMath;

/**
 *
 * @author Bo
 */
public class Eye1_Partial_RayVisionControl extends RayVisionControl {
    
    protected float rayRotAngle = .05f; //0.06f;
//    protected float eyeRotAngle = 0; //-.01f;

    protected static final int nSideRays = 3;
    protected static final float dDist = 1.2f * maxVisionDist / (2<<(nSideRays));

    public Eye1_Partial_RayVisionControl(ControlledMob mob) {
        super(mob);
    }
    
    float getMinDist(int dRay) {
        return ((1<<Math.abs(dRay))-1) * dDist;
    }

    @Override
    public void tick() {
        // SUPER!
        super.tick();
        
        // movement direction unit vector
        float[] dr = new float[] {
            rayStepLength * (float)Math.cos(mob.angleMove),
            rayStepLength * (float)Math.sin(mob.angleMove)
        };

        // cast and add rays
        float[] newVision = new float[3];
        for (int i = -nSideRays; i <= nSideRays; i++) {
            ArrayMath.addInternal(newVision, castRay(ArrayMath.rotate(dr, i * rayRotAngle), getMinDist(i)));
        }
//        // normalize to number of rays
//        div(newVision, nSideRays);
////        div(newVision, 2*nSideRays+1);

        // copy vision to mob
        System.arraycopy(newVision, 0, mob.vision, 0, 3);
                
    }
    
    @Override
    public void render(Screen screen) {
        if(!Gfx.RENDER_VISION_RAYS) return;
        // dir unit vector
        float[] dr = new float[] {
            rayStepLength * (float)Math.cos(mob.angleMove),
            rayStepLength * (float)Math.sin(mob.angleMove)
        };
        // render rays
        for (int i = -nSideRays; i <= nSideRays; i++) {
            renderRay(screen, ArrayMath.rotate(dr, i * rayRotAngle), getMinDist(i));
        }
    }
    
}