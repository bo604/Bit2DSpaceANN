///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package bit2dspaceann.entities.controls.vision.bkp;
//
//import bit2dspaceann.entities.Entity;
//import bit2dspaceann.entities.Fish;
//import bit2dspaceann.entities.controls.vision.VisionControl;
//import static bit2dspaceann.entities.controls.vision.VisionControl.maxDist;
//import bit2dspaceann.gfx.Gfx;
//import bit2dspaceann.gfx.Screen;
//import java.awt.Color;
//import java.awt.geom.Rectangle2D;
//import java.util.List;
//
///**
// *
// * @author Bo
// */
//public class Vision_2_PartialMultiray_Control_bkp01 extends VisionControl {
//    
//    private static final int nSideRays = 3;
//    private static final float rayRotAngle = 0.06f;
//    private float eyeRotAngle = -.01f;
//    
////    private static final float maxDist = (float)Math.sqrt(
////              Gfx.IMAGE_PIXEL_WIDTH * Gfx.IMAGE_PIXEL_WIDTH
////            + Gfx.IMAGE_PIXEL_HEIGHT * Gfx.IMAGE_PIXEL_HEIGHT);
//    
//    // MINIMUM STUFF!
////    private static final float dDist = maxDist / (2*nSideRays+1.0f);
//    private static final float dDist = 1.2f * maxDist / (2<<(nSideRays));
//            
//    private static final float maxRenderDistance = maxDist; //100.0f;
//
////    public Vision_2_PartialMultiray_Control() {
////        super();
////    }
//    
//    
//    
//    float getMinDist(int dRay) {
//        return ((1<<Math.abs(dRay))-1) * dDist;
//    }
//    
//    
//    private float[] castRay(float[] dr, float minDist) {
//        return castRay(dr[0], dr[1], minDist);
//    }
//    
//    
//    private float[] castRay(float dx, float dy, float minDist) {
//        float x = mob.xWorld;
//        float y = mob.yWorld;
//        float dist = 0;
//        float ds = (float)Math.sqrt(dx*dx+dy*dy);
//        float[] result = new float[3];
//        
//        // skip ray beyond minimum distance
//        if(dist<minDist) {
//            float dd = minDist-dist;
//            int steps = (int)(dd/ds)+1;
//            x += steps * dx;
//            y += steps * dy;
//            dist += steps * ds;
//        }
//        
//        // cast ray
//        while(dist<maxDist) {
//            // prep next step
//            x += dx;
//            y += dy;
//            dist += ds;
//            float colorFactor = colorFactor(dist);
//            
//            // see wall?
//            if(!mob.level.contains(x, y)) {
//                result[2] = colorFactor;
////                System.out.println("Seeing level wall with color " + Arrays.toString(result));
//                return result;
//            }
//            else {
//                
//                List<Entity> closeEntities = getCloseEntities();
//                
//                // check all entities.......
////                for (Entity entity : mob.level.entities) {
//                for (Entity entity : closeEntities) {
//                    if(entity == mob) continue;
//                    // see other entity?
//                    if(entity.contains(x, y)) {
//                        for (int i = 0; i < 2; i++) {
//                            result[i] = colorFactor * entity.color[i];
//                        }
////                        System.out.println("Seeing entity " + entity.getEntityTypeName()
////                                         + " with color " + Arrays.toString(result));
//                        return result;
//                    }
//                }
//            }
//        }
//        return result;
//    }
//    
//    
//    
//    @Override
//    public void tick() {
//
//        super.tick();
//        
//        // movement direction unit vector
//        float[] dr = new float[] {
//            visionStepScale * (float)Math.cos(mob.angleMove),
//            visionStepScale * (float)Math.sin(mob.angleMove)
//        };
//
//        // cast and add rays
//        float[] left = new float[3];
//        float[] right = new float[3];
//        for (int i = 0; i <= nSideRays; i++) {
//            add(left, castRay(rotate(dr, -eyeRotAngle - i * rayRotAngle), getMinDist(i)));
//            add(right, castRay(rotate(dr, eyeRotAngle + i * rayRotAngle), getMinDist(i)));
//        }
//
//        // copy vision to mob
//        System.arraycopy(left, 0, ((Fish)mob).vision, 0, 3);
//        System.arraycopy(right, 0, ((Fish)mob).vision, 3, 3);
//                
//    }
//    
//    
//    
//
//    
//    @Override
//    public void render(Screen screen) {
//        
//        if(!Gfx.RENDER_VISION_RAYS) return;
//        
//        float[] dr = new float[] {
//            visionStepScale * (float)Math.cos(mob.angleMove),
//            visionStepScale * (float)Math.sin(mob.angleMove)
//        };
//        
////        for (int i = -nSideRays; i <= nSideRays; i++) {
////            float[] dri = rotate(dr, i * rayRotAngle);
////            float minDist = getMinDist(i);
////            renderRay(screen, dri, minDist);
////        }
//        
//        for (int i = 0; i <= nSideRays; i++) {
////            float[] dri = rotate(dr, i * rayRotAngle);
////            float minDist = getMinDist(i);
//            renderRay(screen, rotate(dr, -eyeRotAngle - i * rayRotAngle), getMinDist(i));
//            renderRay(screen, rotate(dr, eyeRotAngle + i * rayRotAngle), getMinDist(i));
//        }
//        
//        
//        Rectangle2D rect = constructVisionRect();
//        for (int i = 0; i < rect.getWidth(); i++) {
//            screen.renderPixelWithRGB((int)rect.getX()+i, (int)rect.getY(), Color.red.getRGB());
//            screen.renderPixelWithRGB((int)rect.getX()+i, (int)rect.getY()+(int)rect.getHeight(), Color.red.getRGB());
//        }
//        for (int j = 0; j < rect.getHeight(); j++) {
//            screen.renderPixelWithRGB((int)rect.getX(), (int)rect.getY()+j, Color.red.getRGB());
//            screen.renderPixelWithRGB((int)rect.getX()+(int)rect.getWidth(), (int)rect.getY()+j, Color.red.getRGB());
//
//        }
//        
//        
//    }
//    
//    
//    private void renderRay(Screen screen, float[] dr, float minDist) {
//        renderRay(screen, dr[0], dr[1], minDist);
//    }
//    
//    private void renderRay(Screen screen, float dx, float dy, float minDist) {
//        float x = mob.xWorld;
//        float y = mob.yWorld;
//        float dist = 0;
//        float ds = (float)Math.sqrt(dx*dx+dy*dy);
//        
//        // skip ray beyond minimum distance
//        if(dist<minDist) {
//            float dd = minDist-dist;
//            int steps = (int)(dd/ds)+1;
//            x += steps * dx;
//            y += steps * dy;
//            dist += steps * ds;
//        }
//        
//        while(mob.level.contains(x, y) && dist < maxRenderDistance) {
//            float colorFactor = dist/maxDist;
//            colorFactor = colorFactor*colorFactor;
//            Color color = new Color(colorFactor, 1-colorFactor, 0);
//            screen.renderPixelWithRGB(Math.round(x), Math.round(y), color.getRGB());
//            x+=dx;
//            y+=dy;
//            dist+=ds;
//        }
//    }
//    
//    
//}
//
//
////        // left
////        float[] left = new float[3];
////        for (int i = 0; i < nSideRays; i++) {
////            float[] dri = rotate(dr, -eyeRotAngle - i * rayRotAngle);
////            add(left, castRay(dri));
////        }
////        div(left, nSideRays);
//////        System.out.println("Left: " + Arrays.toString(left));
////        
////        // right
////        float[] right = new float[3];
////        for (int i = 0; i < nSideRays; i++) {
////            float[] dri = rotate(dr, eyeRotAngle + i * rayRotAngle);
////            add(right, castRay(dri));
////        }
////        div(right, nSideRays);
//////        System.out.println("Right: " + Arrays.toString(right));
