/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.entities.controls;

import bit2dspaceann.entities.ControlledMob;
import bit2dspaceann.gfx.Screen;

/**
 *
 * @author Bo
 */
public abstract class Control {

    protected ControlledMob mob;

    public Control(ControlledMob mob) {
        if(mob==null) throw new IllegalArgumentException("Control(): mob must not be == null!");
        this.mob = mob;
        mob.addControl(this);
    }
    
    

//    public boolean init(ControlledMob mob) {
//        if(mob==null) return false;
//        mob.addControl(this);
//        this.mob = mob;
//        return true;
//    }

    /**
     * tock
     */
    public abstract void tick();

    /**
     * need?
     */
    public abstract void render(Screen screen);

}
