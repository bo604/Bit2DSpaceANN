/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.entities.controls;

import bit2dspaceann.entities.ControlledMob;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.util.Vec2f;
import java.awt.Color;

/**
 *
 * @author Bo
 */
public class VelocityControl extends Control {
    
//    private static final Random rnd = new Random();
    
    private static final float renderScale = 100f;
    private static final int color = new Color(.2f,.6f,.9f).getRGB();
    
    private static final float dampeningFactor = 0.01f;
    private static final float borderBreakSize = 20;
    
    private static final float dt = 2;
    
    public final Vec2f v = new Vec2f(0,0);

    public VelocityControl(ControlledMob mob) {
        super(mob);
    }
               
    @Override
    public void tick() {
        borderBreak();
        v.x -= dampeningFactor * v.x;
        v.y -= dampeningFactor * v.y;
        mob.xWorld += dt * v.x;
        mob.yWorld += dt * v.y;
    }
    
    private void borderBreak() {
        if(mob.xWorld < borderBreakSize || mob.xWorld > Gfx.IMAGE_PIXEL_WIDTH - borderBreakSize) {
            v.x = 0;
//            v.x = - Math.signum(v.x) * 0.1f;
        }
        if(mob.yWorld < borderBreakSize || mob.yWorld > Gfx.IMAGE_PIXEL_HEIGHT - borderBreakSize) {
            v.y = 0;
        }
    }

    
    @Override
    public void render(Screen screen) {
//        Point from = new Point((int)mob.xWorld, (int)mob.yWorld);
//        Point to = new Point(from);
//        to.translate((int)(renderScale*v.x), (int)(renderScale*v.y));
//        GeoRenderer.drawLine(screen, color, from, to);
    }
    
    
    
}
