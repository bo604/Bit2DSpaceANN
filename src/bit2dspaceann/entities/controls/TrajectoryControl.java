/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.entities.controls;

import bit2dspaceann.entities.ControlledMob;
import bit2dspaceann.gfx.Screen;
import java.awt.Color;
import java.awt.Point;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Bo
 */
public class TrajectoryControl extends Control {
    
    private final List<Point> points = Collections.synchronizedList(new LinkedList<Point>());
    private final float maxPoints = 100.0f;
//    private final float revMaxPoints = 1.0f / maxPoints;
    private final int sampleInterval = 3;
    
    private int ticks = 0;

    public TrajectoryControl(ControlledMob mob) {
        super(mob);
    }
    
    public void clear() {
        points.clear();
    }
    
    @Override
    public void tick() {
        ticks++;
        if(ticks>sampleInterval) {
            ticks=0;
            points.add(new Point(Math.round(mob.xWorld),Math.round(mob.yWorld)));
            if(points.size()>maxPoints)
                points.remove(0);
        }
    }
    
//    private int color = Color.blue.getRGB();

    @Override
    public void render(Screen screen) {
        int i = points.size()-1;
        for (Point point : points) {
//            float blue = 1 - i / maxPoints;
//            screen.renderPixelWithRGB(point.x, point.y, new Color(0,0,blue).getRGB());
            
            float c = 1 - i / maxPoints;
            float r = c < .5f ? 0 : 2f * (c-.5f);
            float g = r;
            float b = c < .5f ? 2f * c : 1;
            
            screen.renderPixelWithRGB(point.x, point.y, new Color(r,g,b).getRGB());
            i--;
        }
    }
    
    
    
}
