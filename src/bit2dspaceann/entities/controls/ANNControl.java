/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bit2dspaceann.entities.controls;

import bit2dspaceann.entities.EvoMob;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.evo.ANN;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.util.MiscMath;
import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author Bo
 */
public class ANNControl extends Control {
    
    private static final Random rnd = new Random();
    
    private final ANN ann;
    
    private final EvoMob emob;

    
    public ANNControl(EvoMob mob, int... nLayers) {
        super(mob);
        emob = mob;
        ann = new ANN(nLayers);
    }
    
    
    public void setGene(float[] gene) {
        ann.setGene(gene);
    }
    
    public float[] getGene() {
        return ann.getGene();
    }
    
    public void randomizeANN() {
        ann.randomize();
    }
    
    float[] lastOutput;
    float[] lastInput;

    @Override
    public void tick() {
        float[] input = constructInput();
        float[] output = ann.calculate(input);
        lastInput = input;
        handleOutput(output);
        lastOutput = output;
    }
    
    
    private void handleOutput(float[] output) {
        // set wheel speed
//        dmc.vl = output[0];
//        dmc.vr = output[1];
        emob.vl = 2 * output[0] - 1;
        emob.vr = 2 * output[1] - 1;
        // set fish color
//        mob.color[0] = output[2];
//        mob.color[1] = output[3];
        
//        // test: slow rot fish
//        mob.vl = 0.01f;
//        mob.vr = 0.008f;
        
    }
    
    
    private float[] constructInput() {

        InputConstructor constructor = new InputConstructor(ann.getNumberInputs());
//        InputConstructor constructor = new InputConstructor(16);
        
        // fuel
//        constructor.add(emob.fuel);
        constructor.add(MiscMath.expDecay(emob.fuel, 1, 0, 1));
        
        // hunger
        constructor.add(hunger(emob.ticksNoSnack));
        
        // vision
        constructor.scopyAll(emob.vision);
        
//        // pressure
//        constructor.add(pressure());
////        constructor.add(lastPressure);
////        lastPressure = pressure();
////        constructor.add(lastPressure);
        
//        // angle
//        constructor.add(directionSense(mob.angleMove));
        
        
//        // recurring vlr
//        constructor.add(lastOutput == null ? .5f : lastOutput[0]);
//        constructor.add(lastOutput == null ? .5f : lastOutput[1]);
        
//        // bias
//        constructor.add(1.0f);
        
        
//        System.out.println(constructor);
        return constructor.input;
    }
    
    private float lastPressure = 0.5f;
    
//    private float[] getLastVision(float[] _lastInput) {
//        float[] lastVision = new float[mob.vision.length];
//        System.arraycopy(_lastInput, 1, lastVision, 0, lastVision.length);
//        return lastVision;
//    }
    
    
    private float piHalf = (float)(0.5f*Math.PI);
    
    private float pressure() {
        float depth = emob.yWorld;
        float maxDepth = Gfx.IMAGE_PIXEL_HEIGHT;
//        float pressure = (float)Math.cos(piHalf*depth/maxDepth);
        float pressure = depth/maxDepth;
//        System.out.println("Pressure: " + pressure);
        return pressure;
    }
    
    private float directionSense(float moveAngle) {
//        return (float)Math.cos(moveAngle);
        return (float)Math.sin(moveAngle);
    }
    
    private class InputConstructor {
        float[] input;
        int idx = 0;
        public InputConstructor(int length) {
            input = new float[length];
        }
        public void add(float f) {
            input[idx] = f;
            idx++;
        }
        public void addAll(float[] f) {
            for (int i = 0; i < f.length; i++) {
                add(f[i]);
            }
        }
        public void scopyAll(float[] f) {
            System.arraycopy(f, 0, input, idx, f.length);
            idx += f.length;
        }

        @Override
        public String toString() {
            return "InputConstructor{" + "idx=" + idx + ", input=" + Arrays.toString(input) +  '}';
        }

        
    }
    
    
    private float hunger(int ticksNoSnack) {
        float hunger = (float)Math.exp(-hungerDecay*ticksNoSnack);
        return hunger;
    }
    
    private float hungerDecay = 0.002f;

    @Override
    public void render(Screen screen) {
    }
    
    
}

