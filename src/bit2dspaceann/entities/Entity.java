/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.entities;

import bit2dspaceann.gfx.Screen;
import bit2dspaceann.level.Level;

/**
 *
 * @author Bo
 */
public abstract class Entity {
    
    public final Class myClass = this.getClass();

    private float detectRadius = 10.0f;
    public float detectRadiusSquared = detectRadius*detectRadius;
    
    public float[] color = new float[] {0,0,0};
    
    protected String entityTypeName;    // type name
    public Level level;
    public float xWorld, yWorld;
    
    public boolean flaggedForRemoval = false;

    public Entity() {
    }
    

    public Entity(String entityTypeName, Level level, float xWorld, float yWorld) {
        this.entityTypeName = entityTypeName;
        this.level = level;
        this.xWorld = xWorld;
        this.yWorld = yWorld;
        level.addEntity(this);
    }
    
    public void setDetectRadius(float r) {
        detectRadius = r;
        detectRadiusSquared = r*r;
    }

    public float getDetectRadius() {
        return detectRadius;
    }
    
    
    
    public void setColor(float r, float g, float b) {
        color[0] = r;
        color[1] = g;
        color[2] = b;
    }
    
    public double[] getKDKey() {
        return new double[]{xWorld,yWorld};
    }
    
//    public boolean contains(float x, float y) {
//        return containsSquaredRadius(x, y);
//    }
    
    public boolean containsSquare(float x, float y) {
        float dx = xWorld-x;
        if(dx<-detectRadius || dx>detectRadius) return false;
        float dy = yWorld-y;
        if(dy<-detectRadius || dy>detectRadius) return false;
        return true;
    }
    
    public boolean contains(float x, float y) {
        float dx = xWorld-x;
        float dy = yWorld-y;
        return dx*dx + dy*dy < detectRadiusSquared;
    }
    

    public String getEntityTypeName() {
        return entityTypeName;
    }

    public abstract void tick();
    public abstract void render(Screen screen);
    

}



//    /**
//     * maybe make this public? and return bool and stuff
//     * @param level
//     */
//    protected final boolean init(String entityTypeName, Level level) {
//        this.entityTypeName = entityTypeName;
//        this.level = level;
//        level.addEntity(this);
//        return true;
//    }
//
//    /**
//     * maybe make this public? and return bool and stuff
//     * @param level
//     */
//    protected final boolean init(String entityTypeName, Level level, float x, float y) {
//        this.entityTypeName = entityTypeName;
//        this.level = level;
//        level.addEntity(this);
//        xWorld = x;
//        yWorld = y;
//        return true;
//    }
    
