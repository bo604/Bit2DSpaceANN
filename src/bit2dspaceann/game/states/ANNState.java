/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.game.states;

import bit2dspaceann.entities.Fish;
import bit2dspaceann.entities.Food;
import bit2dspaceann.entities.Plant;
import bit2dspaceann.entities.Shark;
import bit2dspaceann.evo.EvoChamber;
import bit2dspaceann.game.InputHandler;
import bit2dspaceann.gfx.ColorsLong;
import bit2dspaceann.gfx.Font;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.level.Level;

/**
 *
 * @author Bo
 */
public class ANNState extends State {

//    private static final Random rnd = new Random();
    private EvoChamber evoChamber = null;

    Level level = null;
    
    public ANNState() {
        super();
    }

    public boolean init(InputHandler input) {
        // Super init
        boolean initResult = super.init("ANN", input);

        // Level
        level = new Level();
//        level = new KDTreeLevel();
        
        // EvoChamber
        evoChamber = new EvoChamber(level);

        // Fish
        for (int i = 0; i < Level.nFish; i++) {
            new Fish(level);
        }
        
        // Food
        for (int i = 0; i < Level.nFood; i++) {
            new Food(level);
        }
        
        // Sharks
        for (int i = 0; i < Level.nSharks; i++) {
            new Shark(level);
        }
        
        // Plants Tox
        for (int i = 0; i < Level.nPlantsTox; i++) {
            Plant plant = new Plant(level, true);
        }
        
        // Plants Food
        for (int i = 0; i < Level.nPlantsFood; i++) {
            Plant plant = new Plant(level, false);
        }
        

        return initResult;
    }

    
    

    int nTicksFood = 0;
    int nTicksGeneration = 0;
//    int generationLength = 5000;

    @Override
    public void tick() {
        
//        level.tick();
        level.tickParallelStream();
        
        nTicksFood++;
        if(nTicksFood>30) {
            level.fillFood();
//            level.fillFoodFromTop();
            nTicksFood = 0;
        }
        
        nTicksGeneration++;
        if(nTicksGeneration > Gfx.TICKS_PER_GENERATION) {
            nTicksGeneration = 0;
            evoChamber.makeNewGeneration();
        }
        
    }
    
    
    
    
    
    private long fontColor = ColorsLong.get(-1, -1, -1, -1, -1, -1, -1, 555);

    public void render(Screen screen) {

//        level.renderTiles(screen, Math.round(player.xWorld), Math.round(player.yWorld));

        level.renderEntities(screen);
        
//        int xCenter = screen.pWidth>>1;
//        int yCenter = screen.pHeight>>1;
        
        Font.render("Next Gen. in: " + (Gfx.TICKS_PER_GENERATION - nTicksGeneration), screen, 0, 0, fontColor, 1);
        
        evoChamber.render(screen);

    }


}

