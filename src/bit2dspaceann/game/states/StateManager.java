/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.game.states;

import bit2dspaceann.game.InputHandler;
import bit2dspaceann.gfx.Screen;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Bo
 */
public class StateManager {


    private final List<State> states = new ArrayList<State>();

    private int currentState = 0;

    public StateManager() {

    }

    public boolean init(InputHandler input) {
        
        ANNState as = new ANNState();
        boolean initResult = as.init(input);
        states.add(as);

        return initResult;
    }

    public void addState(State state) {
        states.add(state);
    }


    public void setCurrentState(String stateName) {
        for (int i = 0; i < states.size(); i++) {
            if(states.get(i).name.equals(stateName)) {
                currentState = i;
                return;
            }
        }
        System.out.println("State not found: " + stateName);
    }


    public void tick() {
        states.get(currentState).tick();
    }

    public void render(Screen screen) {
        states.get(currentState).render(screen);
    }




}
