///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//
//package bit2dspaceann.game.states;
//
//import bit2dspaceann.ann.ANNGene;
//import bit2dspaceann.util.EasyChart;
//import bit2dspaceann.entities.Entity;
//import bit2dspaceann.entities.Fish;
//import bit2dspaceann.entities.Food;
//import bit2dspaceann.entities.controls.ANNControl;
//import bit2dspaceann.entities.controls.Control;
//import bit2dspaceann.game.InputHandler;
//import bit2dspaceann.gfx.ColorsLong;
//import bit2dspaceann.gfx.Font;
//import bit2dspaceann.gfx.Gfx;
//import bit2dspaceann.gfx.Screen;
//import bit2dspaceann.level.Level;
//import java.text.DecimalFormat;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Collections;
//import java.util.Iterator;
//import java.util.Random;
//
///**
// *
// * @author Bo
// */
//public class ANNState_bkp01 extends State {
//
//    private static final Random rnd = new Random();
//
//    public ANNState_bkp01() {
//        super();
//    }
//
//    Level level = null;
//    
//    int nFood = 80;
//    int nFish = 25;
//    
//    ANNGene bestGene = null;
//    
//    EasyChart chart = new EasyChart();
//
//    public boolean init(InputHandler input) {
//        // Super init
//        boolean initResult = super.init("ANN", input);
//
//        // Level
////        level = new Level(Gfx.LEVEL_PATHS[3]);
//        level = new Level();
////        int nAst = 10;
////        for (int i = 0; i < nAst; i++) {
////            int x = 3 + rnd.nextInt(20);
////            int y = 3 + rnd.nextInt(20);
////            level.alterTile(x, y, Tile.ASTEROID);
////        }
//
//        // Fish
//        for (int i = 0; i < nFish; i++) {
////            initResult = makeFish(initResult);
//            new Fish(level);
//        }
//        
//        // Food
//        for (int i = 0; i < nFood; i++) {
////            initResult = makeFood(initResult);
//            new Food(level);
//        }
//
//        return initResult;
//    }
//
////    private boolean makeFish(boolean initResult) {
////        ControlledMob mob = new ControlledMob();
////        float x = rnd.nextFloat() * Gfx.IMAGE_PIXEL_WIDTH;
////        float y = rnd.nextFloat() * Gfx.IMAGE_PIXEL_HEIGHT;
////        initResult &= mob.init(level, x, y, 1.4f);
////        mob.color = new float[] {1.0f, 0, 0};
////        
////        DifferentialMoveControl dmc = new DifferentialMoveControl();
////        ANNControl annc = new ANNControl(dmc, 6,6,2);
//////        VisionSingleEyeSingleColorControl vc = new VisionSingleEyeSingleColorControl(annc);
//////        VisionSingleAreaColorControl vc = new VisionSingleAreaColorControl(annc);
//////        VisionDoubleEyeSingleColorControl vc = new VisionDoubleEyeSingleColorControl(annc);
////        VisionDoubleEyeMultirayControl vc = new VisionDoubleEyeMultirayControl(annc);
////        FeedControl fc = new FeedControl(annc);
////        TrajectoryControl tc = new TrajectoryControl();
////        
////        initResult &= dmc.init(mob);
////        initResult &= vc.init(mob);
////        initResult &= annc.init(mob);
////        initResult &= fc.init(mob);
////        initResult &= tc.init(mob);
////        
////        System.out.println(mob);
////        
////        return initResult;
////    }
//    
//    
//    
////    private boolean makeFood(boolean initResult) {
////        Food food = new Food();
////        float x = rnd.nextFloat() * Gfx.IMAGE_PIXEL_WIDTH;
////        float y = rnd.nextFloat() * Gfx.IMAGE_PIXEL_HEIGHT;
////        initResult &= food.init(level, x, y);
//////        System.out.println("Food name: " + food.getEntityTypeName());
////        food.color = new float[] {0, 1.0f, 0};
////        return initResult;
////    }
//    
//    
////    private float[] combineGenesPrimitive(float[] g1, float[] g2) {
////        int nGenes = g1.length;
////        float[] gene = new float[nGenes];
////        for (int i = 0; i < nGenes; i++) {
////            float r = rnd.nextFloat();
////            if(r<.45f) {
////                gene[i] = g1[i];
////            }
////            else if(r<.85f) {
////                gene[i] = g2[i];
////            }
////            else {
////                gene[i] = 2 * rnd.nextFloat() - 1;
////            }
////        }
////        return gene;
////    }
//    
//    private float mutationRate = 0.3f;
//    private float mutationRateDecay = 0.98f;
//    
//    
//    private float[] combineGenesWithFitness(ANNGene g1, ANNGene g2) {
//        int nGenes = g1.gene.length;
//        float[] gene = new float[nGenes];
//        
//        // selection probability based on fitness:
//        float fitTot = g1.fitness + g2.fitness;
//        float p1 = g1.fitness / fitTot;
//        float p2 = g2.fitness / fitTot;
//        
////        System.out.println("g1f=" + g1.fitness + "  p1="+p1);
////        System.out.println("g2f=" + g2.fitness + "  p2="+p2);
//        
//        // include mutation rate:
//        float pNoMutation = 1 - mutationRate;
//        p1 *= pNoMutation;
//        p2 *= pNoMutation;
//        
//        for (int i = 0; i < nGenes; i++) {
//            float r = rnd.nextFloat();
//            if(r<p1) {
//                gene[i] = g1.gene[i];
//            }
//            else if(r<p1+p2) {
//                gene[i] = g2.gene[i];
//            }
//            else {
//                gene[i] = 2 * rnd.nextFloat() - 1;
//            }
//        }
//        return gene;
//    }
//    
//    
//    
//    private ANNGene selectGeneWeighted(ArrayList<ANNGene> sortedGenes) {
//        
//        float totFit = 0;
//        for (ANNGene gene : sortedGenes) {
//            totFit += gene.fitness;
//        }
//        
//        float r = rnd.nextFloat();
//        float relFit = 0;
//        
//        for (ANNGene gene : sortedGenes) {
//            relFit += gene.fitness / totFit;
//            if(r<relFit) {
////                System.out.println("Selected gene fitness: " + gene.fitness);
//                return gene;
//            }
//        }
//        throw new RuntimeException("FUCK UP: NO GENE SELECTED!");
//    }
//    
//    
//    private ArrayList<ANNGene> collectGenes() {
//        
////        PriorityQueue<ANNGene> genes = new PriorityQueue<ANNGene>();
//        ArrayList<ANNGene> genes = new ArrayList<ANNGene>();
//        
//        for (Entity entity : level.entities) {
//            if(entity.getEntityTypeName().equals("Fish")) {
//                Control c = ((Fish)entity).getControl(ANNControl.class);
//                if(c==null) continue;
//                ANNControl annc = (ANNControl)c;
//                genes.add(annc.getANNGene());
//            }
//        }
//        
//        Collections.sort(genes);
//        
////        for (ANNGene gene : genes) {
////            System.out.println("Fitness: " + gene.fitness);
////        }
//        
////        ANNGene[] _genes = genes.toArray(new ANNGene[genes.size()]);
////        return genes.toArray(new ANNGene[genes.size()]);
//        return genes;
//    }
//
//    int nTicksFood = 0;
//    
//    int nTicksGeneration = 0;
//    int generation = 1;
//    
//    int generationLength = 5000;
//
//    public void tick() {
////        System.out.println("Tick!");
//        level.tick();
////        if(input.up.isPressed()) {
////            System.out.println("up pressed");
////        }
//        
//        nTicksFood++;
//        if(nTicksFood>20) {
////            makeFood(true);
//            fillFood();
//            nTicksFood = 0;
//        }
//        
//        nTicksGeneration++;
//        if(nTicksGeneration > generationLength) {
//            nTicksGeneration = 0;
//            generation++;
//            makeNewGeneration();
//        }
//        
//    }
//    
//    DecimalFormat mutaFormat = new DecimalFormat(".##%");
//    
//    private void makeNewGeneration() {
//        System.out.println("=========================");
//        System.out.println("Generation: " + (generation-1));
//        System.out.println("Mutation Rate: " + mutaFormat.format(100.0f*mutationRate));
//        ArrayList<ANNGene> genes = collectGenes();
//        bestGene = genes.get(0);
//        printStats(genes);
//        evolve(genes);
//        System.out.println("=========================");
//        mutationRate *= mutationRateDecay;
//        resetFood();
//        resetPositions();
//    }
//
////    DecimalFormat mutastring = new DecimalFormat("0.###");
//    
//    EasyChart fitDistChart = new EasyChart();
//    
//    private void printStats(ArrayList<ANNGene> genes) {
//        // total fitness
//        float totFit = 0;
//        for (int i = 0; i < genes.size(); i++) {
//            totFit += genes.get(i).fitness;
//        }
//        
//        // fitness distribution
////        System.out.print("Fitness Dist.:");
//        double[] fitDist = new double[genes.size()];
//        for (int i = 0; i < genes.size(); i++) {
//            fitDist[i] = genes.get(i).fitness;
//        }
//        fitDistChart.viewDoubleArray(fitDist);
////        for (Iterator<ANNGene> it = genes.iterator(); it.hasNext();) {
////            ANNGene gene = it.next();
////            System.out.print(gene.fitness);
////            if(it.hasNext()) System.out.print(", ");
////            else System.out.println();
////        }
//        
//        
//        
//        fitnessHistory.add(new Double(totFit));
//        chart.viewDoubleArray(fitnessHistory.toArray(new Double[fitnessHistory.size()]));
//        System.out.println("Total Fitness: " + totFit);
////        System.out.println("Best Gene: " + Arrays.toString(genes.get(0).gene));
//    }
//    
//    private ArrayList<Double> fitnessHistory = new ArrayList<Double>();
//    
//    private void resetFood() {
//        for (Entity entity : level.entities) {
//            if(entity.getEntityTypeName().equals("Food")) {
//                entity.flaggedForRemoval = true;
//            }
//        }
//        for (int i = 0; i < nFood; i++) {
////            makeFood(true);
//            new Food(level);
//        }
//    }
//    
//    private void fillFood() {
//        int nFoodLeft = 0;
//        for (Entity entity : level.entities) {
//            if(entity.getEntityTypeName().equals("Food")) {
//                nFoodLeft++;
//            }
//        }
//        for (int i = 0; i < nFood-nFoodLeft; i++) {
////            makeFood(true);
//            new Food(level);
//        }
//    }
//    
//    private void resetPositions() {
//        for (Entity entity : level.entities) {
//            if(entity.getEntityTypeName().equals("Fish")) {
//                entity.xWorld = rnd.nextFloat() * Gfx.IMAGE_PIXEL_WIDTH;
//                entity.yWorld = rnd.nextFloat() * Gfx.IMAGE_PIXEL_HEIGHT;
//            }
//        }
//    }
//    
//    private void evolve(ArrayList<ANNGene> genes) {
//        
//        // calc stats
//        float totFit = 0;
//        float topFit = 0;
//        for (int i = 0; i < genes.size(); i++) {
//            float fit = genes.get(i).fitness;
//            totFit += fit;
//            if(fit > topFit) {
//                topFit = fit;
//            }
//        }
//        float avrgFit = totFit / genes.size();
//        
//        // thresholds
//        float starveThreshold = 0.05f * avrgFit;
//        float surviveThreshold = 0.75f * topFit;
//        
//        // starve
//        int nStarved = 0;
//        for (Iterator<ANNGene> it = genes.iterator(); it.hasNext();) {
//            ANNGene gene = it.next();
//            if(gene.fitness < starveThreshold) {
//                it.remove();
//                nStarved++;
//            }
//        }
//        System.out.println("Starved (Gene lost): " + nStarved);
//        
//        int nSurvive = 0;
//        for (Entity entity : level.entities) {
//            if(entity.getEntityTypeName().equals("Fish")) {
//                Control c = ((Fish)entity).getControl(ANNControl.class);
//                if(c==null) continue;
//                ANNControl annc = (ANNControl)c;
//                
//                if(annc.fitness < surviveThreshold) {
//                    ANNGene g1 = selectGeneWeighted(genes);
//                    ANNGene g2 = selectGeneWeighted(genes);
//                    float[] newGene = combineGenesWithFitness(g1, g2);
//                    annc.setGene(newGene);
//                    annc.age = 1;
//                }
//                else {
//                    nSurvive++;
//                    annc.age++;
//                }
//                
//                annc.fitness = 1;
//            }
//        }
//        System.out.println("Survived (Gene aged): " + nSurvive);
//    }
//    
//    
//    private long fontColor = ColorsLong.get(-1, -1, -1, -1, -1, -1, -1, 555);
//
//    public void render(Screen screen) {
//
////        level.renderTiles(screen, Math.round(player.xWorld), Math.round(player.yWorld));
//
//        level.renderEntities(screen);
//        
////        int xCenter = screen.pWidth>>1;
////        int yCenter = screen.pHeight>>1;
//        
//        Font.render("Gen="+generation, screen, 0, 12, fontColor, 1);
//        
//        if(bestGene!=null) {
//            bestGene.render(screen);
//        }
//
//    }
//
//
//}
//
