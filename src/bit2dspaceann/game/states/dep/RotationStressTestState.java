/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.game.states.dep;

import bit2dspaceann.game.InputHandler;
import bit2dspaceann.game.states.State;
import bit2dspaceann.gfx.ColorsLong;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.gfx.spriteutil.RotationSpriteMask;
import bit2dspaceann.gfx.spriteutil.SpriteMask;

/**
 *
 * @author Bo
 */
public class RotationStressTestState extends State {


    private final SpriteMask spriteMaskTest = new SpriteMask((byte)0, 0, 27, 1, 1, ColorsLong.TEST_COLOR);
//    private final SpriteMask spriteMaskTest = new SpriteMask(0, 29);

    private final RotationSpriteMask rot1x2
            = new RotationSpriteMask(0, 30, 1, 2);

    private final RotationSpriteMask rot2x2
            = new RotationSpriteMask(2, 30, 2, 2);

    private final RotationSpriteMask rot4x4
            = new RotationSpriteMask((byte)0, 0, 24, 4, 4, ColorsLong.get(-1,002,004,240,445,-1,-1,-1));

    private RotationSpriteMask[][] sprites;

    int angle = 0;


    int n = 26;
    int m = 20;
    int dist = 16;

    public boolean init(InputHandler input) {
        boolean init = super.init("RotationStressTest", input);
        sprites = new RotationSpriteMask[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                RotationSpriteMask rot = new RotationSpriteMask(2, 30, 2, 2);
                sprites[i][j] = rot;
            }
        }
        return init;
    }

    public void tick() {
        /*
         * maybe check for isInit? hm...
         */
        angle = (angle + 1) & 0xff;
    }

    public void render(Screen screen) {

        
//        int xCenter = screen.pWidth>>1;
//        int yCenter = screen.pHeight>>1;

        
////        spriteMaskTest.render(screen, xCenter, yCenter);
//        spriteMaskTest.render(screen, xCenter+40, yCenter, angle);


//        rot1x2.render(screen, xCenter-40, yCenter-40, (angle*3)%360);
//        rot2x2.render(screen, xCenter, yCenter, (angle*2)%360);
//        rot4x4.render(screen, xCenter+40, yCenter+40, angle);

        
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                RotationSpriteMask rot = sprites[i][j];
                rot.render(screen, i * dist, j*dist, angle);
            }
        }


    }


}
