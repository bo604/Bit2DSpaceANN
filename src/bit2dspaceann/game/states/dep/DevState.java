///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//
//package bit2dspaceann.game.states;
//
//import bit2dspaceann.entities.ControlledMob;
//import bit2dspaceann.entities.Player;
//import bit2dspaceann.game.InputHandler;
//import bit2dspaceann.gfx.ColorsLong;
//import bit2dspaceann.gfx.Gfx;
//import bit2dspaceann.gfx.Screen;
//import bit2dspaceann.gfx.spriteutil.RotationSpriteMask;
//import bit2dspaceann.gfx.spriteutil.SpriteMask;
//import bit2dspaceann.level.Level;
//import bit2dspaceann.level.Tile;
//import java.util.Random;
//
///**
// *
// * @author Bo
// */
//public class DevState extends State {
//
//    private static final Random rnd = new Random();
//
//    private final SpriteMask squadA = new SpriteMask(0, 29, ColorsLong.DEF_SHIP);
//    private final SpriteMask squadB = new SpriteMask(0, 29, ColorsLong.DEF_SHIP);
//    private final SpriteMask corvA  = new SpriteMask(1, 30, ColorsLong.DEF_SHIP);
//    private final SpriteMask corvB  = new SpriteMask(1, 31, ColorsLong.DEF_SHIP);
//
//    private final RotationSpriteMask rot1x2
//            = new RotationSpriteMask(0, 30, 1, 2, ColorsLong.DEF_SHIP);
//
//    private final RotationSpriteMask rot2x2
//            = new RotationSpriteMask(2, 30, 2, 2, ColorsLong.DEF_SHIP);
//
//    private final RotationSpriteMask planetTerran
//            = new RotationSpriteMask( 0, 24, 4, 4, ColorsLong.get(-1,002,004,240,445,-1,-1,-1) );
//
//    private final RotationSpriteMask planetTox
//            = new RotationSpriteMask( 0, 24, 4, 4, ColorsLong.get(-1,132,330,333,454,-1,-1,-1) );
//
//    private RotationSpriteMask[][] sprites;
//
//    int angle = 0;
//
//
//
//    private ControlledMob cMobA;
//
//
//    public DevState() {
//        super();
//    }
//
//    int n = 26;
//    int m = 20;
//    int dist = 16;
//
//    Level level = null;
//    Player player = null;
//
//    public boolean init(InputHandler input) {
//        // Super init
//        boolean initResult = super.init("Dev", input);
//
////        sprites = new RotationSpriteMask[n][m];
////        for (int i = 0; i < n; i++) {
////            for (int j = 0; j < m; j++) {
////                RotationSpriteMask rot = new RotationSpriteMask(2, 30, 2, 2);
////                sprites[i][j] = rot;
////            }
////        }
//
//        // Level
//        level = new Level(Gfx.LEVEL_PATHS[3]);
////        level = new Level(null);
//        int nAst = 10;
//        for (int i = 0; i < nAst; i++) {
//            int x = 3 + rnd.nextInt(20);
//            int y = 3 + rnd.nextInt(20);
//            level.alterTile(x, y, Tile.ASTEROID);
//        }
//
//        // Player
////        int xCenter = Gfx.IMAGE_PIXEL_WIDTH >> 1;
////        int yCenter = Gfx.IMAGE_PIXEL_HEIGHT >> 1;
//        int xCenter = (level.levelWidth * Gfx.TILE_SIZE) >> 1;
//        int yCenter = (level.levelHeight * Gfx.TILE_SIZE) >> 1;
//        player = new Player();
//        initResult &= player.init("Soot", input, level, xCenter, yCenter, 2.0f);
//
//
//        makeStarPlanetShips(xCenter, yCenter, initResult);
//        makeStarPlanetShips(xCenter-200, yCenter-300, initResult);
//        makeStarPlanetShips(xCenter+200, yCenter-300, initResult);
//
//        return initResult;
//    }
//
//
//
//    private boolean makeStarPlanetShips(int xCenter, int yCenter, boolean initResult) {
//        // Controlled Mob
//        cMobA = new ControlledMob();
//        initResult &= cMobA.init(level, xCenter-40, yCenter, 1.0f);
//
//        // Entity B to orbit around
//        ControlledMob cMobB = new ControlledMob();
//        initResult &= cMobB.init(level, xCenter-50, yCenter, 1.0f);
//
//        // Entity C to double orbit around
//        ControlledMob cMobC = new ControlledMob();
//        initResult &= cMobC.init(level, xCenter+100, yCenter+50, 1.0f);
//
//        // Moon
//        RotationSpriteMask moonSprite = new RotationSpriteMask(0, 22, 2, 2, ColorsLong.get(-1,000,000,111,222,333,444,555));
//        ControlledMob moon = new ControlledMob();
//        initResult &= moon.init(level, xCenter+100, yCenter+50, 1.0f, moonSprite);
//
//        // Planet
//        RotationSpriteMask planetSprite = new RotationSpriteMask(0, 24, 4, 4, ColorsLong.get(-1,002,004,240,445,-1,-1,-1));
//        planetSprite.renderScale = 2;
//        ControlledMob planet = new ControlledMob();
//        initResult &= planet.init(level, xCenter+100, yCenter+50, 1.0f, planetSprite);
//
//        // Sun
//        RotationSpriteMask sunSprite = new RotationSpriteMask(4, 24, 4, 4, ColorsLong.get(-1,100,110,220,330,541,552,554));
//        sunSprite.renderScale = 3;
//        ControlledMob sun = new ControlledMob();
//        initResult &= sun.init(level, xCenter, yCenter, 0.0f, sunSprite);
//
//        // Controls for Mob A
//        SpinControl spin = new SpinControl();
//        initResult &= spin.init(cMobA);
//        spin.angleSpeed = 2 * rnd.nextFloat();
//        OrbitControl orb = new OrbitControl();
//        initResult &= orb.init(cMobA,planet);
//        orb.frequency *= 2.5f * rnd.nextFloat();
//        orb.setDistance(45);
//
//        // Controls for Mob B
//        spin = new SpinControl();
//        initResult &= spin.init(cMobB);
//        spin.angleSpeed = 2 * rnd.nextFloat();
//        orb = new OrbitControl();
//        initResult &= orb.init(cMobB,planet);
//        orb.frequency *= 1.5f * rnd.nextFloat();
//        orb.setDistance(45);
//
//        // Controls for Mob C
//        spin = new SpinControl();
//        initResult &= spin.init(cMobC);
//        spin.angleSpeed = 2 * rnd.nextFloat();
//        orb = new OrbitControl();
//        initResult &= orb.init(cMobC,planet);
////        orb.frequency *= 2.5f;
//        orb.setDistance(45);
//
//        // Controls for Moon
//        spin = new SpinControl();
//        initResult &= spin.init(moon);
//        spin.angleSpeed = 0.5f * rnd.nextFloat();
//        orb = new OrbitControl();
//        initResult &= orb.init(moon,planet);
//        orb.frequency *= 0.75f * rnd.nextFloat();
//        orb.setDistance(55);
//
//        // Controls for Planet
//        spin = new SpinControl();
//        initResult &= spin.init(planet);
//        spin.angleSpeed = 0.5f * rnd.nextFloat();
//        orb = new OrbitControl();
//        initResult &= orb.init(planet,sun);
//        orb.frequency *= 0.1f + 0.1f * rnd.nextFloat();
//        orb.setDistance(180);
//
//        // Controls for Sun
//        spin = new SpinControl();
//        initResult &= spin.init(sun);
//        spin.angleSpeed = 0.2f * rnd.nextFloat();
//
//        return initResult;
//    }
//
//    public void tick() {
////        angle = (angle + 1) % 0xFF;
//        angle = (angle + 1) & 0xFF;
//        level.tick();
//    }
//
//    public void render(Screen screen) {
//
//        level.renderTiles(screen, Math.round(player.xWorld), Math.round(player.yWorld));
//
//        level.renderEntities(screen);
//
//        
//        int xCenter = screen.pWidth>>1;
//        int yCenter = screen.pHeight>>1;
////
////        spriteMaskTest.render(screen, xCenter, yCenter);
////        squadA.render(screen, xCenter+(int)(40*Math.sin(angle/60.0*Math.PI)), yCenter+(int)(40*Math.cos(angle/60.0*Math.PI)), (2*0xFF-angle*2)$0xFF);
////        squadB.render(screen, xCenter, yCenter, angle);
////
////        corvA.render(screen, player.xWorld, player.yWorld);
////        corvA.render(screen, player.xWorld, player.yWorld, 8, angle);
//
////        corvB.render(screen, xCenter, yCenter-40, angle);
////
////        rot1x2.render(screen, player.xWorld, player.yWorld, 8, angle);
////        rot2x2.render(screen, player.xWorld, player.yWorld, 4, angle);
////
////        planetTerran.render(screen, player.xWorld, player.yWorld, 4, angle);
////        planetTox.render(screen, xCenter-40, yCenter+40, 0xFF-1-angle);
//
////        CenterCrossRenderer.render(screen);
//
//    }
//
//
//}