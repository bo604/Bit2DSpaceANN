/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.game.states;

import bit2dspaceann.game.InputHandler;
import bit2dspaceann.gfx.Screen;

/**
 *
 * @author Bo
 */
public abstract class State {


    public String name = null;
    protected InputHandler input = null;


    public boolean init(String name, InputHandler input) {
        if(name==null || input==null) return false;
        this.name = name;
        this.input = input;
        return true;
    }

    public abstract void tick();
    

    public abstract void render(Screen screen);

    

}
