/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.game;

import bit2dspaceann.gfx.GameCanvas;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Bo
 */
public class InputHandler implements KeyListener {

    public InputHandler() {
        
    }

    public boolean init(GameCanvas canvas) {
        if(canvas == null) return false;
        canvas.addKeyListener(this);
        return true;
    }

    public class Key {
        private boolean pressed = false;
        private int numTimesPressed = 0;
        private boolean isBlocked = false;
        public Key() {
            Key toAdd = this;
            InputHandler.ALL_KEYS.add(toAdd);
        }
        public boolean isPressed() {
            return pressed;
        }
        public void toggle(boolean isPressed) {
            if(!isBlocked) {
                pressed = isPressed;
                System.out.println("Key: " + isPressed);
            }
            else if (!isPressed) {
                isBlocked = false;
                pressed = false;
                System.out.println("Key: " + isPressed);
            }
            if(isPressed) numTimesPressed++;
//            System.out.println("pressed " + this);
        }
        public int getNumTimesPressed() {
            return numTimesPressed;
        }
        public void disableAndBlock() {
            pressed = false;
            isBlocked = true;
        }
//        public void unblock() {
//            isBlocked = false;
//        }
//        public boolean isBlocked() {
//            return isBlocked;
//        }
    }

    public Key esc = new Key();
    public Key enter = new Key();
    public Key space = new Key();

    public Key up = new Key();
    public Key down = new Key();
    public Key left = new Key();
    public Key right = new Key();
    
    public Key c = new Key();
    public Key v = new Key();
    public Key b = new Key();
    
    public Key f = new Key();
    public Key g = new Key();
    
    public Key p = new Key();

    /*
     * DIRTY:
     */
    public static List<Key> ALL_KEYS = new ArrayList<Key>();

    /*
     * Keylistening:
     */
    
    public void keyPressed(KeyEvent e) {
        toggle(e.getKeyCode(), true);
    }

    public void keyReleased(KeyEvent e) {
        toggle(e.getKeyCode(), false);
    }

    public void keyTyped(KeyEvent e) {}

    public void toggle(int keyCode, boolean isPressed) {
        if(keyCode == KeyEvent.VK_ESCAPE) { esc.toggle(isPressed); }
        if(keyCode == KeyEvent.VK_ENTER) { enter.toggle(isPressed); }
        if(keyCode == KeyEvent.VK_SPACE) { space.toggle(isPressed); }
        
        if(keyCode == KeyEvent.VK_W) { up.toggle(isPressed); }
        if(keyCode == KeyEvent.VK_S) { down.toggle(isPressed); }
        if(keyCode == KeyEvent.VK_A) { left.toggle(isPressed); }
        if(keyCode == KeyEvent.VK_D) { right.toggle(isPressed); }
        
        if(keyCode == KeyEvent.VK_C) { c.toggle(isPressed); }
        if(keyCode == KeyEvent.VK_V) { v.toggle(isPressed); }
        if(keyCode == KeyEvent.VK_B) { b.toggle(isPressed); }
        
        if(keyCode == KeyEvent.VK_F) { f.toggle(isPressed); }
        if(keyCode == KeyEvent.VK_G) { g.toggle(isPressed); }
        
        if(keyCode == KeyEvent.VK_P) { p.toggle(isPressed); }
    }

}
