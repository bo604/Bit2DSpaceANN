/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bit2dspaceann.game;

import bit2dspaceann.game.states.StateManager;
import bit2dspaceann.gfx.Font;
import bit2dspaceann.gfx.GameCanvas;
import bit2dspaceann.gfx.GameFrame;
import bit2dspaceann.gfx.Gfx;
import bit2dspaceann.gfx.Screen;
import bit2dspaceann.gfx.spriteutil.SpriteSheet;
import java.awt.BorderLayout;
import java.awt.Color;

/**
 *
 * @author Bo
 */
public class Game implements Runnable {

    protected boolean running = true;

    protected StateManager stateMgr = null;
    protected InputHandler input = null;
//    protected InputHandler guiInput = null;

    protected GameFrame frame = null;
    protected GameCanvas mainCanvas = null;
//    protected GameCanvas guiCanvas = null;

    
//    /*
//     * HOW TO MANAGE SCREEN LAYOUT? (i.e. HUD, MENU, ETC.)
//     */
//    public static final int hudHeight = 3 * Gfx.TILE_SIZE;
//    private Screen hud;
////    private MenuScreen menuScreen;
////    private GameOverScreen ggScreen;

    /*
     * FOR NOW: JUST ONE MAIN SCREEN...
     *          better decide who should render and give them the screen?
     *          player xOffset stuff suckt etwas... mal sehen wie hud z.b. noch gehen könnte.
     *          kommt man da dran (nicht) vorbei? check das nochmal!
     * andererseits: verschiede sprite sheets auf den screens sind toll! hmmmm
     */
    private Screen screen;

//    // pretty star backgrounds
//    private final StarRenderer stars = new StarRenderer();
    

//    public enum CanvasID {
//        Center(240,180,"Center"),
//        Top,
//        Bottom,
//        Left,
//        Right;
//        public boolean enabled;
//        public int width, height;
//        public String borderLayoutPosition;
//        // HAS TO BE SET AFTER SCREEN CREATIONS:
//        public Screen screen;
//        private CanvasID(int width, int height, String borderLayoutPosition) {
//            this.enabled = true;
//            this.width = width;
//            this.height = height;
//            this.borderLayoutPosition = borderLayoutPosition;
//        }
//        private CanvasID() {
//            this.enabled = false;
//        }
//        public void setScreen(Screen screen) {
//            this.screen = screen;
//        }
//    }
//    public EnumMap<CanvasID,GameCanvas> canvi = new EnumMap<CanvasID, GameCanvas>(CanvasID.class);
//    /*
//     * that borderlayout canvas usage has to be thought through further. nice
//     * approach i think. one thing: each of them needs a screen, has to be referenced too, somewhere..
//     * maybe even in the enums? e.g. see above.
//     */
    

    public boolean init() {
        boolean initResult = true;

//        for (CanvasID canvasID : CanvasID.values()) {
//            if(canvasID.enabled) {
//                GameCanvas gc = new GameCanvas();
//                initResult &= gc.init(canvasID);
//                canvi.put(canvasID, gc);
//            }
//        }

        // Canvas
        mainCanvas = new GameCanvas();
        initResult &= mainCanvas.init();

        // Frame
        frame = new GameFrame();
        initResult &= frame.init(mainCanvas);
        frame.addCanvas(mainCanvas, BorderLayout.CENTER);
//        frame.addCanvas(guiCanvas, BorderLayout.EAST);    // hm

        // Input
        input = new InputHandler();
        initResult &= input.init(mainCanvas);

//        // GUI Input?
//        guiInput = new InputHandler();
//        initResult &= guiInput.init(guiCanvas);

        // Level Sprite Sheet
        SpriteSheet defSheet = new SpriteSheet();
        initResult &= defSheet.init(Gfx.DEF_SPRITE_SHEET_PATH);
        // Font Sprite Sheet
        SpriteSheet fontSheet = new SpriteSheet();
        initResult &= fontSheet.init(Gfx.FONT_SPRITE_SHEET_PATH);

        // Screen
        screen = new Screen();
        initResult &= screen.init();
        screen.setSpriteSheet(0, defSheet);
        screen.setSpriteSheet(1, fontSheet);

        // StateManager
        stateMgr = new StateManager();
        initResult &= stateMgr.init(input);
//        stateMgr.setCurrentState("Dev");
        stateMgr.setCurrentState("ANN");
//        stateMgr.setCurrentState("RotationStressTest");

        return initResult;
    }

    public synchronized void start() {
        running = true;
        new Thread(this).start();
    }

    public synchronized void stop() {
        running = false;
    }


    private double nsPerTick = 1000000000D/100D;
    private boolean doAnimate = true;
        
    public void run() {

        boolean init = init();
        System.out.println("Init success: " + init);

        int ticks = 0;
        int frames = 0;
        double delta = 0;

        long lastTime = System.nanoTime();
        long lastFullSecond = System.currentTimeMillis();

                
        while(running) {

            
            if(doAnimate) {
                long now = System.nanoTime();
                delta += (now - lastTime) / nsPerTick;
                lastTime = now;
                // TICK
                while(delta>=1) {
                    ticks++;
                    tick();
                    delta -= 1;
                }
                frames++;
                // RENDER
                render();
                // CHILL
//                chill();
                // UPDATE FPS
                if(System.currentTimeMillis() - lastFullSecond >= 1000) {
                    frameInfo = "FPS=" + frames + "  TPS=" + ticks;
                    System.out.println(frameInfo);
                    frame.setTitle(frameInfo);
                    ticks = 0;
                    frames = 0;
                    lastFullSecond = System.currentTimeMillis();
                }
            }
            else {
                tick();
                ticks++;
                long now = System.nanoTime();
                // kinda dirty render timing...
                if(ticks%200==0) {
                    render();
                    frames++;
                }
                if(now - lastTime > 1000000000) {
//                    render();
                    lastTime = now;
//                    System.out.println("TPS: " + ticks);
                    frameInfo = "FPS=" + frames + "  TPS=" + ticks;
                    System.out.println(frameInfo);
                    frame.setTitle(frameInfo);
                    ticks = 0;
                    frames = 0;
                }
            }
            
        }
        System.exit(0);
    }
    

    String frameInfo = "";

    private void chill() {
        try {
            Thread.sleep(2);
        } catch (Exception e) {
            System.out.println(e);
        }
    }


    public void tick() {
        // tick input
        tickInput();
        // tick state manager
        if(!isPaused)
            stateMgr.tick();
    }
    
    
    private void tickInput() {
        // quit
        if(input.esc.isPressed()) {
            running = false;
        }
        
        // toggle chart rendering
        if(input.c.isPressed()) {
            input.c.disableAndBlock();
            Gfx.RENDER_CHARTS = !Gfx.RENDER_CHARTS;
        }
        // toggle vision ray rendering
        if(input.v.isPressed()) {
            input.v.disableAndBlock();
            Gfx.RENDER_VISION_RAYS = !Gfx.RENDER_VISION_RAYS;
        }
        // toggle fish vision rendering
        if(input.b.isPressed()) {
            input.b.disableAndBlock();
            Gfx.RENDER_FISH_VISION = !Gfx.RENDER_FISH_VISION;
        }
        // toggle genepool rendering
        if(input.g.isPressed()) {
            input.g.disableAndBlock();
            Gfx.RENDER_GENEPOOL = !Gfx.RENDER_GENEPOOL;
        }
        // toggle fitness rendering
        if(input.f.isPressed()) {
            input.f.disableAndBlock();
            Gfx.RENDER_ENTITY_INFO = !Gfx.RENDER_ENTITY_INFO;
        }
        
        // toggle full animation
        if(input.space.isPressed()) {
            input.space.disableAndBlock();
            doAnimate = !doAnimate;
        }
        
        // increase speed
        if(input.up.isPressed()) {
            input.up.disableAndBlock();
            nsPerTick *= 0.9;
            nsPerTick = Math.max(nsPerTick, 1000000000D/300D);
        }
        // decrease speed
        if(input.down.isPressed()) {
            input.down.disableAndBlock();
            nsPerTick *= 1.1;
            nsPerTick = Math.min(nsPerTick, 1000000000D/10D);
        }
        // pause
        if(input.p.isPressed()) {
            input.p.disableAndBlock();
            isPaused = !isPaused;
        }
        
    }
    
    private boolean isPaused = false;

    
    int bgColor = new Color(0.035f, 0, 0.09f).getRGB();


    public void render() {

//        /*
//         * SOME TESTING RENDERERS
//         */
//        randomColorTest.render(screen);
//        BlackRenderer.render(screen);
//        BlackBoxRenderer.render(screen, xCenter-12, xCenter+12, yCenter-12, yCenter+12);
        
//        /*
//         * Background
//         */
//        stars.render(screen);
//        screen.clear(Color.black.getRGB());
        screen.clear(bgColor);

        /*
         * StateManager
         */
        stateMgr.render(screen);

        renderInfos(screen);

        /*
         * Draw all commited renders to the GameFrame
         */
        mainCanvas.commitAndRender(screen, 0);
    }


//    private long fontColor = ColorsLong.get(-1, -1, -1, -1, -1, -1, -1, 555);

    private void renderInfos(Screen screen) {
//        Font.render(frameInfo, screen, screen.xOffset, screen.yOffset, fontColor, 1);
//        Font.render(frameInfo, screen, 0, 0, Font.DEF_COLOR, 1);
        Font.render(frameInfo, screen, Gfx.IMAGE_PIXEL_WIDTH - Font.sizeOf(frameInfo, 1), 0, Font.DEF_COLOR, 1);
    }


}


// <editor-fold defaultstate="collapsed" desc="old">


//        // Screen
//        SpriteSheet standardSS = new SpriteSheet("/sprite_sheet.png");
//        menuScreen = new MenuScreen(width, height, standardSS);
//        hud = new Screen(width, hudHeight, new SpriteSheet("/sprite_sheet.png"));
//        screen = new Screen(width, height, new SpriteSheet("/sprite_sheet.png"));
//        ggScreen = new GameOverScreen(width, height, new SpriteSheet("/sprite_sheet_nyaruko_walk_front.png"));
//        // Level
//        String lvl;
////        lvl = "/levels/test_10x10.png";
////        lvl = "/levels/test_32x32.png";
//        lvl = "/levels/test_64x64.png";
//        level = new Level(lvl);
//        // Input handler
//        input = new InputHandler(this);
//        // Player (Sooty or UserNameDialog?)
//        player = new Player(level, 0, 0, input);
////        player = new Player(level, 0, 0, input, JOptionPane.showInputDialog(this, "Who are you?"));
//        level.addEntity(player);
//        switch(gameState) {
//            case Menu:
//                if(input.space.isPressed()) {
//                    gameState = GameState.Running;
//                }
//                break;
//            case Running:
//                level.tick();
//                if(player.isGameOver()) {
//                    gameState = GameState.GameOver;
//                }
//                else if(input.esc.isPressed()) {
//                    gameState = GameState.Menu;
//                }
//                break;
//            case GameOver:
//                if(input.esc.isPressed() || input.space.isPressed()) {
//                    initialize();
//                    gameState = GameState.Menu;
//                }
//                break;
//            default:
//                break;
//        }
//    }
//        /*
//         * RENDER STUFF
//         */
//        switch(gameState) {
//            case Running:
//                renderLevelViewScreen();
//                break;
//            case GameOver:
//                renderGGScreen();
//                break;
//            case Menu:
//                renderMenuScreen();
//                break;
//        }
//        renderHUD();
//        for (int i = 0; i < mainScreen.pixels.length; i++) {
//            mainScreen.pixels[i] = i % 2000 + tickCount;
//        }
//        for (int j = 0; j < pHeight; j++) {
//            for (int i = 0; i < pWidth; i++) {
////                mainScreen.pixels[i + j * pWidth] = (i + tickCount)%100 + (i%200);
//                mainScreen.randomColorAt(i, j);
//            }
//        }
//        for (int j = 0; j < 8; j++) {
//            for (int i = 0; i < 4; i++) {
//                mainScreen.render(0 + i * Gfx.TILE_SIZE, 0 + j * Gfx.TILE_SIZE, i + j * 32, Colors999.get(-1, 444, 666, 888));
//            }
//        }
//    private void renderHUD() {
//        // Offset in the pixels before drawing the hud.
//        int hudOffset = (pWidth-1) + (pHeight-1) * pWidth + 1;
//        // Render hud
//        level.renderHud(hud);
//        canvas.commitToBufferImage(hud, hudOffset);
//    }
//
//    private void renderLevelViewScreen() {
//        // "Camera position" to player position
//        int xOffset = player.xWorld - (pWidth/2);
//        int yOffset = player.yWorld - (pHeight/2);
//        // Render stuff into view screen
//        level.renderTiles(screen, xOffset, yOffset);
//        level.renderEntities(screen);
//        canvas.commitToBufferImage(screen, 0);
//    }
//
//    private void renderMenuScreen() {
//        menuScreen.render();
//        commitToBufferImage(menuScreen, 0);
//    }
//
//    private void renderGGScreen() {
//        ggScreen.render();
//        commitToBufferImage(ggScreen, 0);
//    }
//        /*
//         * INIT STUFF
//         */
//
//        // Chasers
//        int nChasers = 20;
//        Random rnd = new Random();
//        for (int i = 0; i < nChasers; i++) {
//            Chaser chaser = new Chaser(level,
//                    rnd.nextInt((level.width-1) << 3),
//                    rnd.nextInt((level.height-1) << 3));
//            level.addEntity(chaser);
//            chaser.setPlayerToChase(player);
//        }
//        // Pickups
//        int nPickEach = 10;
//        BasicPickup pick;
//        for (int i = 0; i < nPickEach; i++) {
////            for (Pickup.Factory factory : Pickup.Factory.values()) {
//            for (Pickup pickup : Pickup.values()) {
//                pick = pickup.create(level, level.rndXWorld(), level.rndYWorld());
//                level.addEntity(pick);
//            }
//        }


// </editor-fold>

//            if(mainInput.space.isPressed()) {
//                tick();
//                long now = System.nanoTime();
//                if(now - lastTime > 1000000000) {
//                    render();
//                    lastTime = now;
//                }
//            }
//            else {
//                long now = System.nanoTime();
//                delta += (now - lastTime) / nsPerTick;
//                lastTime = now;
//                // TICK
//                while(delta>=1) {
//                    ticks++;
//                    tick();
//                    delta -= 1;
//                }
//                frames++;
//                // RENDER
//                render();
//                // CHILL
////                chill();
//                // UPDATE FPS
//                if(System.currentTimeMillis() - lastFullSecond >= 1000) {
//                    frameInfo = "FPS=" + frames + "  TPS=" + ticks;
//                    System.out.println(frameInfo);
//                    frame.setTitle(frameInfo);
//                    ticks = 0;
//                    frames = 0;
//                    lastFullSecond = System.currentTimeMillis();
//                }
//            }
