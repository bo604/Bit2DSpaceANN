## Virtual fish learning to eat food: Neural network evolution for "vision-to-motion" controller.

# Warning: Messy code!

### See the video at https://www.youtube.com/watch?v=7R7I0hXv7LI

A spare-time case study of the genetic evolution of a small population of artificial neural nets. Each neural net controls the movement of a "fish" with the objective of food collection. Turn on annotations for some basic info during the video.

In this example, the neural nets have 23 input units, 8 hidden units in a single hidden layer, and 2 output units. 21 of the inputs are assigned to the vision of the fish, and one input each to "hunger" and "fuel". The outputs control the movement of the fish, which is modeled as two wheels with an axle, and each output controls the rotation speed of one of the wheels. 

The weights of the connections between the layers are initially randomized and remain constant during each generation. After a given time, the fitness of each fish is evaluated as the number of food collected. Then a new generation is created, using the weights of the neural nets as a genome. This is done by first selecting two fish at random with a probability proportional to their fitness. The genome of their offspring is a randomized gene-by-gene selection of the genomes of the parents; again with the selection probability weighted by their fitness. This is repeated until a new population is created, and the food collection for the next generation begins.

At the beginning, the fish are moving in a random fashion as expected for randomized weights of the neural nets: Some are simply rotating in place, some are moving backwards, and some are moving forwards, which is a big deal at this stage! Those fish who have a somewhat decent movement control will reproduce more often, and the offspring will become more efficient at collecting food.

Over the course of many generations, they will learn to:
- Move forward when there is food in front of them
- Move in spirals to find food otherwise
- Avoid the open border
- Move slowly when there is no food in sight (save fuel)
- Move quickly to collect food in sight before somebody else does

Motion:
Movement stops when fuel is empty. Fuel consumption is proportional to the square of the velocity, and fuel is replenished each time food is collected. When two fish come close to each other in this example, their total fuel is averaged/shared between them.

Vision:
Each fish has 7 vision cells. For each cell, a ray is cast towards the front, and the vision of the cell is set to the first obstacle the ray hits. The further away the obstacle, the darker it gets. Each cell sees RGB-color, hence 21 input units in total for the neural nets.

Vision Colors:
Red: Other fish
Green: Food
Blue: Border

Environment Variations:
Food-dispensing plants, Fish-eating plants, Predators, ...

Coded in Java. Visualization recycled from sprite-based games coded earlier. No 3rd party libraries were used.